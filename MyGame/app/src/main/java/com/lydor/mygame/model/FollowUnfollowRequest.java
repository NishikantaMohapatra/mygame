package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FollowUnfollowRequest implements Parcelable {

    private String following_id;
    private int follow;

    public FollowUnfollowRequest() {
    }

    protected FollowUnfollowRequest(Parcel in) {
        following_id = in.readString();
        follow = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(following_id);
        dest.writeInt(follow);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FollowUnfollowRequest> CREATOR = new Creator<FollowUnfollowRequest>() {
        @Override
        public FollowUnfollowRequest createFromParcel(Parcel in) {
            return new FollowUnfollowRequest(in);
        }

        @Override
        public FollowUnfollowRequest[] newArray(int size) {
            return new FollowUnfollowRequest[size];
        }
    };

    public String getFollowing_id() {
        return following_id;
    }

    public void setFollowing_id(String following_id) {
        this.following_id = following_id;
    }

    public int getFollow() {
        return follow;
    }

    public void setFollow(int follow) {
        this.follow = follow;
    }
}

package com.lydor.mygame.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activity.ChallengesDetailsActivity;
import com.lydor.mygame.activity.LoginActivity;
import com.lydor.mygame.adapter.MyProfileMyFeedAdapter;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.MyFeedResponse;
import com.lydor.mygame.model.MyProfileMyFeedListResponse;
import com.lydor.mygame.model.MyProfileMyFeedVideoListData;
import com.lydor.mygame.model.UserMyFeedRequest;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyProfileMyFeedVideoList.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyProfileMyFeedVideoList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProfileMyFeedVideoList extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView rcvList;
    private ArrayList<MyProfileMyFeedVideoListData> mList;
    private ProgressDialog mProgressDialog;
    private SharedPref pref;
    private MyProfileMyFeedAdapter mAdapter;

    private OnFragmentInteractionListener mListener;
    private TextView errTv;

    int currentPageNum,lastPageNum;

    public MyProfileMyFeedVideoList() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyProfileMyFeedVideoList.
     */
    // TODO: Rename and change types and number of parameters
    public static MyProfileMyFeedVideoList newInstance(String param1, String param2) {
        MyProfileMyFeedVideoList fragment = new MyProfileMyFeedVideoList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile_my_feed_video_list, container, false);

        mList = new ArrayList<>();
        mAdapter = new MyProfileMyFeedAdapter(mList,getActivity());
        rcvList = view.findViewById(R.id.recycler_view1);
        errTv = view.findViewById(R.id.errTv);
        LinearLayoutManager linearLayoutManager=new GridLayoutManager(getActivity(),2);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvList.setLayoutManager(linearLayoutManager);
        rcvList.setHasFixedSize(true);
        rcvList.setAdapter(mAdapter);
        pref = new SharedPref(getActivity());

        mList.clear();
        getMyProfileMyFeed(1);


        rcvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    //Toast.makeText(getActivity(), "Last", Toast.LENGTH_LONG).show();

                    if(currentPageNum!=lastPageNum){
                        Log.v("currentPage:",currentPageNum+"");
                        getMyProfileMyFeed(currentPageNum+1);
                    }else{
                        Toast.makeText(getActivity(), "No More Videos", Toast.LENGTH_SHORT).show();
                    }



                }
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void getMyProfileMyFeed(int pageNum){
        if (AppUtil.isInternetConnected(getActivity())) {
            mProgressDialog= ProgressDialog.show(getActivity(),null, Constants.PLEASE_WAIT);


            UserMyFeedRequest request = new UserMyFeedRequest();
            request.setUser_id(pref.getInt(Constants.USER_ID));

            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);

            Call<MyProfileMyFeedListResponse> call = apiService.getProfileMoments(pageNum,request
                    );
            call.enqueue(new Callback<MyProfileMyFeedListResponse>() {
                @Override
                public void onResponse(Call<MyProfileMyFeedListResponse> call, Response<MyProfileMyFeedListResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        MyProfileMyFeedListResponse myProfileMyFeedListResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();
                        currentPageNum = myProfileMyFeedListResponse.getCurrent_page();
                        lastPageNum = myProfileMyFeedListResponse.getLast_page();
                        if(myProfileMyFeedListResponse.getData().size()>0){
                            //mList.clear();
                            mList.addAll(myProfileMyFeedListResponse.getData());
                            mAdapter.notifyDataSetChanged();
                            errTv.setVisibility(View.GONE);
                            rcvList.setVisibility(View.VISIBLE);


                        }else{
                            mList.clear();
                            mAdapter.notifyDataSetChanged();
                            errTv.setVisibility(View.VISIBLE);
                            rcvList.setVisibility(View.GONE);
                            errTv.setText("You have not posted any video...");
                        }
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<MyProfileMyFeedListResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(getActivity(), "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }
}

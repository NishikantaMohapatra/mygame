package com.lydor.mygame.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.lydor.mygame.R;
import com.lydor.mygame.activity.ChallengesDetailsActivity;
import com.lydor.mygame.model.MyChallengesList;
import com.lydor.mygame.model.MyFeedList;
import com.lydor.mygame.models.Model_Video;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Date;
import java.util.List;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import wseemann.media.FFmpegMediaMetadataRetriever;

public class ChallengesListingAdapter extends RecyclerView.Adapter<ChallengesListingAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private Context context;
    private List<MyChallengesList> testLists;
    DateFormat formatter = null;
    Date convertedDate = null;


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public ChallengesListingAdapter(List<MyChallengesList> testLists, Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.testLists = testLists;
    }


    // inflates the row layout from xml when needed
    @Override
    public ChallengesListingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.challenges_listing_item,
                parent, false);
        ChallengesListingAdapter.ViewHolder viewHolder = new ChallengesListingAdapter.ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the view and textview in each row
    @Override
    public void onBindViewHolder(ChallengesListingAdapter.ViewHolder holder, final int position) {
        LinearLayout lnrLayContainer = holder.lnrLayContainer;
        final ImageView imageView = holder.imageView;
        TextView tvName = holder.tvName;
        TextView tvLikes = holder.tvLikes;
        TextView tvDate = holder.tvDate;
        View view = holder.viewId;
        TextView shareTv = holder.shareTv;

        final MyChallengesList myChallengesList = testLists.get(position);

        tvName.setText(testLists.get(position).getTitle());
        tvLikes.setText(testLists.get(position).getTotal_likes()+" Likes");

        String str = testLists.get(position).getCreated_at();
        String[] splitStr = str.split("\\s+");

        formatter =new SimpleDateFormat("dd-MM-yyyy");
        try {
            convertedDate =(Date) formatter.parse(splitStr[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //System.out.println("Date from dd-MM-yyyy String in Java : " + convertedDate);

        String date = null;
        try {
            String inputTimeStamp = testLists.get(position).getCreated_at();

            final String inputFormat = "yyyy-MM-dd";
            final String outputFormat = "dd MMM yyyy";

            System.out.println(TimeStampConverter(inputFormat, inputTimeStamp,
                    outputFormat));

            date = TimeStampConverter(inputFormat,inputTimeStamp,outputFormat);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        tvDate.setText(date);
        /*Uri uri = Uri.parse("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+testLists.get(position).getFile_name());
        FFmpegMediaMetadataRetriever retriever = new  FFmpegMediaMetadataRetriever();
        Bitmap bmp = null;
        try {

            retriever.setDataSource("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+testLists.get(position).getFile_name());
            bmp = retriever.getFrameAtTime();
            //int videoHeight = (int) (bmp.getHeight()*((float)getItemCount()/bmp.getWidth()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        retriever.release();*/
        if(testLists.get(position).getVideo_thumbnail()!=null) {
            //holder.imv.setTag(liveStreamLists.get(position).getImage());
            /*final Bitmap finalBmp = bmp;*/
            Picasso.with(context).load("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+testLists.get(position).getVideo_thumbnail())
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap,
                                                   Picasso.LoadedFrom from) {

                            imageView.setImageBitmap(bitmap);

                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            imageView.setBackground(
                                    context.getResources().getDrawable(R.mipmap.photo)
                            );

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        }
        else
        {
            imageView.setBackground(
                    context.getResources().getDrawable(R.mipmap.photo)
            );

        }

        if(position==testLists.size()-1){


            view.setVisibility(View.INVISIBLE);
        }

        lnrLayContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(context,ChallengesDetailsActivity.class);
                in.putExtra("challengeID",testLists.get(position).getId());
                in.putExtra("videoUrl",testLists.get(position).getFile_name());
                in.putExtra("obj",myChallengesList);
                context.startActivity(in);
            }
        });


        shareTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareVid("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+testLists.get(position).getFile_name());
            }
        });




    }


    // total number of rows
    @Override
    public int getItemCount() {
        return testLists.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public LinearLayout lnrLayContainer;
        public ImageView imageView;
        public TextView tvName,tvDate,tvLikes,shareTv;
        public View viewId;

        public ViewHolder(View itemView) {
            super(itemView);
            lnrLayContainer = itemView.findViewById(R.id.lnrLayContainer);
            imageView = itemView.findViewById(R.id.imv);
            tvName = itemView.findViewById(R.id.tvName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvLikes = itemView.findViewById(R.id.tvLikes);
            viewId = itemView.findViewById(R.id.viewId);
            shareTv = itemView.findViewById(R.id.shareTv);

        }
    }

    // allows clicks events to be caught

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);


    }

    public Bitmap createVideoThumbNail(String path){
        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
    }


    private static String TimeStampConverter(final String inputFormat,
                                             String inputTimeStamp, final String outputFormat)
            throws ParseException {
        return new SimpleDateFormat(outputFormat).format(new SimpleDateFormat(
                inputFormat).parse(inputTimeStamp));
    }


    public void shareVid(String videoUrl){
        String path = videoUrl; //should be local path of downloaded video
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        share.putExtra(Intent.EXTRA_TEXT, path);
        context.startActivity(Intent.createChooser(share, "Share link!"));


    }
}
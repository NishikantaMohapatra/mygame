package com.lydor.mygame.utils;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 5/1/2017.
 */

public class DateTimeUtil {

    private static final String TAG=DateTimeUtil.class.getSimpleName();
    public static String getSatelliteDateTimeAsString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        return sdf.format(new Date(time));
    }
    public static String get(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        sdf.setTimeZone(TimeZone.getDefault());

        return sdf.format(new Date(time));

    }
    public  static Date getStartDateGuest(long time)
    {
        Calendar calendar= Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.set(Calendar.HOUR,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);

        Log.v(TAG,convertMillisAsDateString(calendar.getTimeInMillis()));

        return calendar.getTime();
    }


    public  static int compareCurrentDateWithOtherDate(long anotherDate)
    {
        Calendar currentCalendar= Calendar.getInstance();
        currentCalendar.setTimeInMillis(System.currentTimeMillis());


        Calendar anotherDateCalender= Calendar.getInstance();
        anotherDateCalender.setTimeInMillis(anotherDate);

        Date currentDate=new Date(
                currentCalendar.get(Calendar.YEAR),
                currentCalendar.get(Calendar.MONTH),
                currentCalendar.get(Calendar.DAY_OF_MONTH)
                );

        Date otherDate=new Date(
                anotherDateCalender.get(Calendar.YEAR),
                anotherDateCalender.get(Calendar.MONTH),
                anotherDateCalender.get(Calendar.DAY_OF_MONTH)
        );

        return currentDate.compareTo(otherDate);
    }

    public  static Date getStartDateTwo(long time)
    {
        Calendar calendar= Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);

        Calendar calendar1= Calendar.getInstance();
        calendar1.set(Calendar.MONTH,calendar.get(Calendar.MONTH));
        calendar1.set(Calendar.YEAR,calendar.get(Calendar.YEAR));
        calendar1.set(Calendar.DAY_OF_MONTH,calendar.get(Calendar.DAY_OF_MONTH));
        return calendar1.getTime();
    }
    public  static Date getCurrentDate()
    {
//        Calendar calendar= Calendar.getInstance();
//        calendar.setTimeInMillis(System.currentTimeMillis());
//        calendar.set(Calendar.HOUR_OF_DAY,calendar.get(Calendar.HOUR_OF_DAY));

        return new Date();

    }

    public static String getDateAsString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date(time));
    }

    public static String getDateTime(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh:mm a");
        return sdf.format(new Date(time));
    }
    public static String getAudiRecordTimeFormat(long time) {
        return   String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(time),
                TimeUnit.MILLISECONDS.toSeconds(time) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))
        );
    }
    public static String convertMillisAsDateString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
        return sdf.format(new Date(time));
    }
    public static String convertMillisAsChatDateString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(new Date(time));
    }

    public static String convertMillisAsString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date(time));
    }

    public static String convertMillisAsTimeString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(new Date(time));
    }
    public static String convertMillisAsDateTimeString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh:mm a");
        return sdf.format(new Date(time));
    }
    public static String getDeviceDateTimeAsString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(new Date(time));
    }
    public static String getChatFileFormat(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_hhmmss");
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(new Date(time));
    }
    public static String getAddedOnDate(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(new Date(time));
    }
    static final String DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static Date GetUTCdatetimeAsDate()
    {
        //note: doesn't check for null
        return StringDateStrToDate(GetUTCdatetimeAsString());
    }
    public static Date getCreateDate()
    {
        return new Date();
    }



    public static String GetUTCdatetimeAsString()
    {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());

        return utcTime;
    }

    public static Date StringDateStrToDate(String StrDate)
    {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);

        try
        {
            dateToReturn = (Date)dateFormat.parse(StrDate);
        }
        catch (ParseException e)
        {
            ////////Log.v(TAG,e.getMessage());
        }

        return dateToReturn;
    }
    public static Date StringFcmDateStrToDate(String StrDate)
    {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try
        {
            dateToReturn = (Date)dateFormat.parse(StrDate);
        }
        catch (ParseException e)
        {
            Log.v(TAG,e.getMessage());
        }

        return dateToReturn;
    }
    public static Date StringDoubtDateStrToDate(String StrDate)
    {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");

        try
        {
            dateToReturn = (Date)dateFormat.parse(StrDate);
        }
        catch (ParseException e)
        {
            Log.v(TAG,e.getMessage());
        }

        return dateToReturn;
    }
    public static long StringDateStrToTimeMillis(String StrDate)
    {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");

        try
        {
            dateToReturn = (Date)dateFormat.parse(StrDate);
        }
        catch (ParseException e)
        {
            ////////Log.v(TAG,e.getMessage());
        }

        return dateToReturn.getTime();
    }

    public static String getDateAsTodayEtc(long millis)
    {
           if(DateUtils.isToday(millis))
           {
               return  "Today";

           }
           else

               if(isYesterday(millis))
               {
                   return  "Yesterday";

               }
           else
               return convertMillisAsDateString(millis);
    }
    public static String getChatReadDeliveryFormat(long millis)
    {
        if(DateUtils.isToday(millis))
        {
            return  "today"+" at "+convertMillisAsTimeString(millis);

        }
        else

        if(isYesterday(millis))
        {
            return  "yesterday"+" at "+convertMillisAsTimeString(millis);

        }
        else
            return convertMillisAsChatDateString(millis)+" at "+convertMillisAsTimeString(millis);
    }
    public static boolean isYesterday(long date) {
        Calendar now = Calendar.getInstance();
        Calendar cdate = Calendar.getInstance();
        cdate.setTimeInMillis(date);

        now.add(Calendar.DATE,-1);

        return now.get(Calendar.YEAR) == cdate.get(Calendar.YEAR)
                && now.get(Calendar.MONTH) == cdate.get(Calendar.MONTH)
                && now.get(Calendar.DATE) == cdate.get(Calendar.DATE);
    }
    public static String getSurfResultFormat(long time)
    {
        long difference = System.currentTimeMillis()-time;
        int days=(int)(difference/(1000*60*60*24));
        if(days==0)
        {
            String format=  DateUtils.getRelativeTimeSpanString(
                    time, System.currentTimeMillis(),
                    0L, DateUtils.FORMAT_ABBREV_ALL).toString();
            if(format.contains("sec"))
                return "Just Now";
            else
                return convertMillisAsTimeString(time);

        }
        else

        if(DateUtils.isToday(time))
        {
            return  "Today";

        }
        else
            return convertMillisAsDateString(time);
    }

    public static String getTimeAsNowEtc(long time)
    {
      String format=  DateUtils.getRelativeTimeSpanString(
                time, System.currentTimeMillis(),
                0L, DateUtils.FORMAT_ABBREV_ALL).toString();
        if(format.contains("sec"))
            return "Just Now";
        else
            return convertMillisAsTimeString(time);
    }

    public static String formatDateFromString(Context context, String date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);

        } catch (ParseException e) {
           // //////Log.v(TAG,e.getMessage());
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM yyyy");
        String finalDate = timeFormat.format(myDate);

        return  finalDate;
    }



    public static String getSatelliteDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        return sdf.format(new Date(System.currentTimeMillis()));
    }

    public  static Date getEndDate(long time)
    {
        Calendar calendar= Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,50);

        return calendar.getTime();
    }

    
}

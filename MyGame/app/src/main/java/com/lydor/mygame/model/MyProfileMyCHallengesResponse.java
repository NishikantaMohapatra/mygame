package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class MyProfileMyCHallengesResponse implements Parcelable {

    private int current_page;
    private ArrayList<MyProfileMyChallengesListData> data;
    private String first_page_url;
    private String from;
    private int last_page;
    private String last_page_url;
    private String next_page_url;
    private String path;
    private int per_page;
    private String prev_page_url;
    private int to;
    private int total;

    public MyProfileMyCHallengesResponse() {
    }

    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public ArrayList<MyProfileMyChallengesListData> getData() {
        return data;
    }

    public void setData(ArrayList<MyProfileMyChallengesListData> data) {
        this.data = data;
    }

    public String getFirst_page_url() {
        return first_page_url;
    }

    public void setFirst_page_url(String first_page_url) {
        this.first_page_url = first_page_url;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public int getLast_page() {
        return last_page;
    }

    public void setLast_page(int last_page) {
        this.last_page = last_page;
    }

    public String getLast_page_url() {
        return last_page_url;
    }

    public void setLast_page_url(String last_page_url) {
        this.last_page_url = last_page_url;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public void setPrev_page_url(String prev_page_url) {
        this.prev_page_url = prev_page_url;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public static Creator<MyProfileMyCHallengesResponse> getCREATOR() {
        return CREATOR;
    }

    protected MyProfileMyCHallengesResponse(Parcel in) {
        current_page = in.readInt();
        first_page_url = in.readString();
        from = in.readString();
        last_page = in.readInt();
        last_page_url = in.readString();
        next_page_url = in.readString();
        path = in.readString();
        per_page = in.readInt();
        prev_page_url = in.readString();
        to = in.readInt();
        total = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(current_page);
        dest.writeString(first_page_url);
        dest.writeString(from);
        dest.writeInt(last_page);
        dest.writeString(last_page_url);
        dest.writeString(next_page_url);
        dest.writeString(path);
        dest.writeInt(per_page);
        dest.writeString(prev_page_url);
        dest.writeInt(to);
        dest.writeInt(total);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MyProfileMyCHallengesResponse> CREATOR = new Creator<MyProfileMyCHallengesResponse>() {
        @Override
        public MyProfileMyCHallengesResponse createFromParcel(Parcel in) {
            return new MyProfileMyCHallengesResponse(in);
        }

        @Override
        public MyProfileMyCHallengesResponse[] newArray(int size) {
            return new MyProfileMyCHallengesResponse[size];
        }
    };
}

package com.lydor.mygame.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.media.ExifInterface;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by HP on 6/10/2017.
 */

public class BitmapUtil {


    private static final String TAG=BitmapUtil.class.getSimpleName();
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            //Log.v(TAG,heightRatio+" : "+heightRatio+":i"+inSampleSize);

            inSampleSize++;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        //Log.v(TAG,totalPixels+" : "+totalReqPixelsCap);
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            //Log.v(TAG,inSampleSize+"");
            inSampleSize++;
        }

        //Log.v(TAG,inSampleSize+" : inSampleSize");
        return inSampleSize;
    }
    public static String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(),
               FileUtil.IMAGE_SENT_DIR);
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath()  +"/"+ System.currentTimeMillis() + ".jpg");
        return uriSting;

    }


    public static File compressImage(Context context, File filePath, int mh, int mw) {



     /*   Cursor cursor =context. getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            filePath= uri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            filePath= cursor.getString(index);
            cursor.close();
        }*/
      //  if(filePath.length()<= ChatConstants.MEDIA_COMPRESS_MAX_SIZE)
        //    return filePath;

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath.getAbsolutePath(), options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = mh;//816.0f;
        float maxWidth =mw;// 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            }
            else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath.getAbsolutePath(), options);
        } catch (OutOfMemoryError exception) {
            //Log.v(TAG, exception.getMessage() + "");

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            //Log.v(TAG, exception.getMessage() + "");
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath.getAbsolutePath());

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            //Log.v(TAG, "EXIF" + "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                //Log.v(TAG, "EXIF" + "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                //Log.v(TAG, "EXIF" + "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                //Log.v(TAG, "EXIF" + "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            //Log.v(TAG, e.getMessage() + "");
        }

        FileOutputStream out = null;
       // String fileName=getFilename();
        //Log.v(TAG,"File name: "+filePath);
        try {
            out = new FileOutputStream(filePath);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            out.close();

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }

      /*  long totalBytes=new File(filePath).length();

        MediaUpload upload=new MediaUpload();
        upload.setLocalAbsPath(filePath);
        upload.setFileSize(totalBytes);
        if(totalBytes<=(ChatConstants.MEDIA_CHUNK_SIZE_KB))
        {
            upload.setChunkSizeInBytes(totalBytes);
            upload.setTotalChunks(1);

        }
        else
        {


            upload.setChunkSizeInBytes(ChatConstants.MEDIA_CHUNK_SIZE_KB);
            upload.setTotalChunks(totalBytes/ChatConstants.MEDIA_CHUNK_SIZE_KB);
        }
        upload.setUploadedChunks(0);
        upload.setUploadedOffset(0);*/

        return filePath;

    }


    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static Bitmap decodeSampledBitmapFromFile(String path,
                                                     int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path,options);


        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return createRoundedBitmap(BitmapFactory.decodeFile(path,options), reqWidth, reqHeight);
    }

    public static Bitmap createRoundedBitmap(Bitmap mbitmap, int w, int h)
    {
        Bitmap imageRounded = Bitmap.createBitmap(w,h, mbitmap.getConfig());
        Canvas canvas = new Canvas(imageRounded);
        Paint mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        canvas.drawRoundRect((new RectF(0, 0,w, h)), 8, 8, mpaint);

        return imageRounded;
    }
    public  static  boolean isFileExist(String imageAbsPath)
    {
        boolean status=false;
        final File f = new File(imageAbsPath);
        if (f.isFile()&&f.exists()&&f.length()>0) {
            status=true;
        }

        return  status;

    }

    public static  boolean isVideoFile(String fileName)
    {
        String[] file=fileName.split("/");
        fileName=file[file.length-1];
        String ext= fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
        if(ext!=null&&ext.equalsIgnoreCase(".mp4"))
        {
            return  true;
        }

        return false;

    }

}

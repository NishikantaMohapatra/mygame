package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.adapter.Adapter_VideoFolder;
import com.lydor.mygame.adapter.ChallengesDetailsAdapter;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.fragment.CameraViewNewActivity;
import com.lydor.mygame.model.ChallengesDetailsAcceptedList;
import com.lydor.mygame.model.ChallengesDetailsResponse;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.MyCHallengesAcceptedRequest;
import com.lydor.mygame.model.MyChallengesList;
import com.lydor.mygame.model.MyChallengesResponse;
import com.lydor.mygame.model.MyFeedList;
import com.lydor.mygame.models.Model_Video;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChallengesDetailsActivity extends AppCompatActivity {


    private VideoView videoView;
    ImageView imageView;

    boolean isPlay = false;
    ArrayList<ChallengesDetailsAcceptedList> mList;

    ChallengesDetailsAdapter obj_adapter;
    ArrayList<Model_Video> al_video = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerView.LayoutManager recyclerViewLayoutManager;
    private static final int REQUEST_PERMISSIONS = 100;
    ImageView imvBack;
    MyChallengesList myChallengesList;
    TextView tvDescription,tvLikes,acceptChallenge;
    private ProgressDialog mProgressDialog;
    SharedPref pref;
    TextView tvDateEnds,tvShare;
    int currentPageNum,lastPageNum;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;
    String strVideoUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenges_details);

        videoView = findViewById(R.id.videoView);
        imageView = findViewById(R.id.imageView);
        tvDescription = findViewById(R.id.tvDescription);
        tvLikes = findViewById(R.id.tvLikes);
        acceptChallenge = findViewById(R.id.acceptChallenge);
        mProgressDialog = new ProgressDialog(this);
        tvDateEnds = findViewById(R.id.tvDateEnds);
        tvShare = findViewById(R.id.tvShare);

        recyclerView = (RecyclerView) findViewById(R.id.rcvList);

        mList = new ArrayList<>();

        LinearLayoutManager linearLayoutManager=new GridLayoutManager(ChallengesDetailsActivity.this,2);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setOnClickListener(null);
        obj_adapter = new ChallengesDetailsAdapter(mList,this);
        recyclerView.setAdapter(obj_adapter);
        al_video = new ArrayList<>();
        pref = new SharedPref(this);
        //fn_checkpermission();

        if(pref.getBoolean("isActive")){
            acceptChallenge.setVisibility(View.VISIBLE);
        }else{
            acceptChallenge.setVisibility(View.GONE);
        }

        imvBack = findViewById(R.id.imvBack);
        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(videoView.isPlaying()){
                    videoView.stopPlayback();
                    finish();
                }else{
                    finish();
                }

            }
        });


        //String strVideoUrl = "https://androidwave.com/media/androidwave-video-exo-player.mp4";

        if(getIntent().getStringExtra("videoUrl")!=null){
            strVideoUrl = getIntent().getStringExtra("videoUrl");
            myChallengesList = getIntent().getExtras().getParcelable("obj");
        }

        tvDescription.setText(myChallengesList.getDescription());
        tvLikes.setText(myChallengesList.getTotal_likes()+" Likes");

        String str = myChallengesList.getCreated_at();
        String[] splitStr = str.split("\\s+");
        String date = null;
        try {
            String inputTimeStamp = myChallengesList.getCreated_at();

            final String inputFormat = "yyyy-MM-dd";
            final String outputFormat = "dd MMM yyyy";

            System.out.println(TimeStampConverter(inputFormat, inputTimeStamp,
                    outputFormat));

            date = TimeStampConverter(inputFormat,inputTimeStamp,outputFormat);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        tvDateEnds.setText(date);


        //update package name
        /*Uri uri = Uri.parse("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+strVideoUrl);
        //videoView.setLayoutParams(params);
        videoView.setVideoURI(uri);

        videoView.requestFocus();
        videoView.start();
        isPlay = true;*/

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isPlay){

                    videoView.pause();
                    isPlay = false;
                    imageView.setImageResource(R.drawable.ic_playsign);
                    //startTimer();
                }else if(!isPlay){

                    videoView.start();
                    isPlay = true;
                    //updateSeekBar();

                    imageView.setImageResource(R.drawable.ic_pausesign);
                }
            }
        });


        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {

                imageView.setImageResource(R.drawable.ic_playsign);
                isPlay = false;
            }
        });



        acceptChallenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //checkPermission();
                Intent in = new Intent(ChallengesDetailsActivity.this, CameraViewNewActivity.class);
                in.putExtra("From","challenges");
                in.putExtra("id",String.valueOf(myChallengesList.getId()));
                startActivity(in);
            }
        });
        //customDialog("20191127/cce29870-110e-11ea-8117-b1e187889d21.mp4");
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    //Toast.makeText(getActivity(), "Last", Toast.LENGTH_LONG).show();

                        if(currentPageNum!=lastPageNum){
                            Log.v("currentPage:",currentPageNum+"");
                            getChallengesListing(currentPageNum+1);
                        }else{
                            Toast.makeText(ChallengesDetailsActivity.this, "No More Videos", Toast.LENGTH_SHORT).show();
                        }



                }
            }
        });

        final String finalStrVideoUrl = strVideoUrl;
        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                shareVid("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+ finalStrVideoUrl);

            }
        });


    }

    private void fn_checkpermission(){
        /*RUN TIME PERMISSIONS*/

        if ((ContextCompat.checkSelfPermission(ChallengesDetailsActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(ChallengesDetailsActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(ChallengesDetailsActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(ChallengesDetailsActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE))) {

            } else {
                ActivityCompat.requestPermissions(ChallengesDetailsActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }
        }else {
            Log.e("Else","Else");
            //fn_video();
        }
    }

    public void stopVideoPlayBack(){
        if(videoView.isPlaying()){
            videoView.stopPlayback();
        }
    }




    public void fn_video() {

        int int_position = 0;
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name,column_id,thum;

        String absolutePathOfImage = null;
        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Video.Media.BUCKET_DISPLAY_NAME, MediaStore.Video.Media._ID, MediaStore.Video.Thumbnails.DATA};

        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        cursor = getContentResolver().query(uri, projection, null, null, orderBy + " DESC");

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.BUCKET_DISPLAY_NAME);
        column_id = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
        thum = cursor.getColumnIndexOrThrow(MediaStore.Video.Thumbnails.DATA);
        al_video = new ArrayList<>();
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            Log.e("Column", absolutePathOfImage);
            Log.e("Folder", cursor.getString(column_index_folder_name));
            Log.e("column_id", cursor.getString(column_id));
            Log.e("thum", cursor.getString(thum));

            Model_Video obj_model = new Model_Video();
            obj_model.setBoolean_selected(false);
            obj_model.setStr_path(absolutePathOfImage);
            obj_model.setStr_thumb(cursor.getString(thum));

            //al_video.add(obj_model);

        }


        /*obj_adapter = new ChallengesDetailsAdapter(al_video,ChallengesDetailsActivity.this);
        recyclerView.setAdapter(obj_adapter);
        obj_adapter.notifyDataSetChanged();*/

    }





    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(videoView.isPlaying()){
            videoView.stopPlayback();

        }
        finish();
    }

    public void getVideoPathandPlay(String path){



        if(videoView.isPlaying()){

            videoView.pause();
            videoView.stopPlayback();
            videoView.setVideoPath(path);

            videoView.requestFocus();
            videoView.start();
            isPlay = true;
            //updateSeekBar();

            imageView.setImageResource(R.drawable.ic_pausesign);




        }else{
            videoView.setVideoPath(path);

            videoView.requestFocus();
            videoView.start();
            isPlay = true;
            //updateSeekBar();

            imageView.setImageResource(R.drawable.ic_pausesign);
        }





    }


    public void getChallengesListing(int pageNum){
        if (AppUtil.isInternetConnected(this)) {
            mProgressDialog= ProgressDialog.show(this,null, Constants.PLEASE_WAIT);


            MyCHallengesAcceptedRequest request = new MyCHallengesAcceptedRequest();
            request.setChallenge_id(myChallengesList.getId());

            RestService apiService =
                    ApiClient.getClient(this).create(RestService.class);

            Call<ChallengesDetailsResponse> call = apiService.getAcceptedChallengesList(pageNum,request);
            call.enqueue(new Callback<ChallengesDetailsResponse>() {
                @Override
                public void onResponse(Call<ChallengesDetailsResponse> call, Response<ChallengesDetailsResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }

                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(ChallengesDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        ChallengesDetailsResponse challengesDetailsResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();
                        currentPageNum = challengesDetailsResponse.getCurrent_page();
                        lastPageNum = challengesDetailsResponse.getLast_page();
                        if(challengesDetailsResponse.getData().size()>0){
                            //mList.clear();
                            mList.addAll(challengesDetailsResponse.getData());
                            obj_adapter.notifyDataSetChanged();


                        }else{
                            Toast.makeText(ChallengesDetailsActivity.this, "No Accepted video available...", Toast.LENGTH_SHORT).show();
                        }
                    }else if(response.code()==401){
                        Toast.makeText(ChallengesDetailsActivity.this, "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(ChallengesDetailsActivity.this, LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        finish();
                    }


                }

                @Override
                public void onFailure(Call<ChallengesDetailsResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(ChallengesDetailsActivity.this, ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(ChallengesDetailsActivity.this, "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }


    public void customDialog(String videoUrl){
        final Dialog dialog = new Dialog(this);

        View view = getLayoutInflater().inflate(R.layout.video_dialog, null);


        final VideoView videoView = (VideoView) view.findViewById(R.id.videoView);
        ImageView cross = (ImageView)view.findViewById(R.id.cross);

        // Change MyActivity.this and myListOfItems to your own values

        Uri uri = Uri.parse("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+videoUrl);
        //videoView.setLayoutParams(params);
        videoView.setVideoURI(uri);

        videoView.requestFocus();
        videoView.start();
        isPlay = true;

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(videoView.isPlaying()){
                    videoView.stopPlayback();
                }
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Uri uri = Uri.parse("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+strVideoUrl);
        //videoView.setLayoutParams(params);
        videoView.setVideoURI(uri);

        videoView.requestFocus();
        videoView.start();
        isPlay = true;
        mList.clear();
        getChallengesListing(1);
    }


    private static String TimeStampConverter(final String inputFormat,
                                             String inputTimeStamp, final String outputFormat)
            throws ParseException {
        return new SimpleDateFormat(outputFormat).format(new SimpleDateFormat(
                inputFormat).parse(inputTimeStamp));
    }


    protected void checkPermission(){
        if(ContextCompat.checkSelfPermission(ChallengesDetailsActivity.this,Manifest.permission.CAMERA)
                + ContextCompat.checkSelfPermission(ChallengesDetailsActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(ChallengesDetailsActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(ChallengesDetailsActivity.this,Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            // Do something, when permissions not granted
            if(ActivityCompat.shouldShowRequestPermissionRationale(
                    ChallengesDetailsActivity.this,Manifest.permission.CAMERA)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    ChallengesDetailsActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    ChallengesDetailsActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    ChallengesDetailsActivity.this,Manifest.permission.RECORD_AUDIO) ){
                // If we should give explanation of requested permissions

                // Show an alert dialog here with request explanation
                AlertDialog.Builder builder = new AlertDialog.Builder(ChallengesDetailsActivity.this);
                builder.setMessage("Camera, Read Contacts, Write External and Audio Record" +
                        " Storage permissions are required to do the task.");
                builder.setTitle("Please grant those permissions");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(
                                ChallengesDetailsActivity.this,
                                new String[]{
                                        Manifest.permission.CAMERA,
                                        Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        Manifest.permission.RECORD_AUDIO
                                },
                                MY_PERMISSIONS_REQUEST_CODE
                        );
                    }
                });
                builder.setNeutralButton("Cancel",null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }else{
                // Directly request for required permissions, without explanation
                ActivityCompat.requestPermissions(
                        ChallengesDetailsActivity.this,
                        new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.RECORD_AUDIO
                        },
                        MY_PERMISSIONS_REQUEST_CODE
                );
            }
        }else {
            // Do something, when permissions are already granted
            Intent in = new Intent(ChallengesDetailsActivity.this,CustomCameraActivity.class);
            in.putExtra("From","challenges");
            in.putExtra("id",String.valueOf(myChallengesList.getId()));
            startActivity(in);
            //Toast.makeText(ChallengesDetailsActivity.this,"Permissions already granted",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CODE:{
                // When request is cancelled, the results array are empty
                if(
                        (grantResults.length >0) &&
                                (grantResults[0]
                                        + grantResults[1]
                                        + grantResults[2]
                                        + grantResults[3]
                                        == PackageManager.PERMISSION_GRANTED
                                )
                ){
                    // Permissions are granted
                    Intent in = new Intent(ChallengesDetailsActivity.this,CustomCameraActivity.class);
                    in.putExtra("From","challenges");
                    in.putExtra("id",String.valueOf(myChallengesList.getId()));
                    startActivity(in);
                    Toast.makeText(ChallengesDetailsActivity.this,"Permissions granted.",Toast.LENGTH_SHORT).show();
                }else {
                    // Permissions are denied
                    Toast.makeText(ChallengesDetailsActivity.this,"Permissions denied.",Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }


    public void shareVid(String videoUrl){
        String path = videoUrl; //should be local path of downloaded video
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        share.putExtra(Intent.EXTRA_TEXT, path);
        startActivity(Intent.createChooser(share, "Share link!"));


    }
}

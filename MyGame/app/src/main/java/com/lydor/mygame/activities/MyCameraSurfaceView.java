package com.lydor.mygame.activities;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.lydor.mygame.activity.CustomCameraActivity;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MyCameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback{

    private SurfaceHolder mHolder;
    public Camera mCamera;
    public List<Camera.Size> mSupportedPreviewSizes;
    private Camera.Size mPreviewSize;

    public MyCameraSurfaceView(Context context, Camera camera) {
        super(context);
        mCamera = camera;
        mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int weight,
                               int height) {
        Log.e(TAG, "surfaceChanged => w=" + weight + ", h=" + height);
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.
        refreshCamera(mCamera);
        if (mHolder.getSurface() == null){
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or reformatting changes here
        // start preview with new settings
        try {
            Camera.Parameters parameters = mCamera.getParameters();

            List<String> focusModes = parameters.getSupportedFocusModes();
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            }
            parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            /*parameters.setZoom(100);
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
            parameters.setVideoStabilization(false);*/

            //parameters.setZoom(0);
            mCamera.setParameters(parameters);
            mCamera.setDisplayOrientation(90);
            mCamera.stopSmoothZoom();



            /*mCamera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean b, Camera camera) {
                    if(b){
                        synchronized (camera){
                            new Thread(){
                                public void run() {
                                    try {
                                        mCamera.setPreviewDisplay(mHolder);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    super.run();
                                }
                            }.start();
                        }
                    }
                }
            });*/



            //setFocus(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);


            mCamera.startPreview();

        } catch (Exception e){
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub

        try {
            //mCamera.open(1);
            mCamera.setDisplayOrientation(90);
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d("MyGame", "Error setting camera preview: " + e.getMessage());
        }


        // The Surface has been created, now tell the camera where to draw the preview.
        /*Camera.Parameters params = mCamera.getParameters();

        try {


           *//* if (Integer.parseInt(Build.VERSION.SDK) >= 8)
                setDisplayOrientation(mCamera, 90);
            else
            {
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                {
                    params.set("orientation", "portrait");
                    params.set("rotation", 90);

                }
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                {
                    params.set("orientation", "landscape");
                    params.set("rotation", 90);
                }
            }*//*
            if(this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT){

                params.set("orientation","portrait");
                //mCamera.setPreviewDisplay(holder);
                mCamera.setDisplayOrientation(180);
                params.setRotation(180);
            }else{
                params.set("orientation","landscape");
                //mCamera.setPreviewDisplay(holder);
                mCamera.setDisplayOrientation(90);
                params.setRotation(90);
            }

            mCamera.setParameters(params);


            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();

            //mCamera.startPreview();
        } catch (IOException e) {
        }*/
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        if(mCamera!=null){
            //mCamera.stopPreview();
            mCamera.release();
            if(mCamera!=null){

                mCamera.release();
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
            }else{
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
            }



            mCamera = null;
        }


    }


    protected void setDisplayOrientation(Camera camera, int angle){
        Method downPolymorphic;
        try
        {
            downPolymorphic = camera.getClass().getMethod("setDisplayOrientation", new Class[] { int.class });
            if (downPolymorphic != null)
                downPolymorphic.invoke(camera, new Object[] { angle });
        }
        catch (Exception e1)
        {
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getOptimalPreviewSize(
                    mSupportedPreviewSizes, width, height);
        }

        if (mPreviewSize!=null) {
            float ratio;
            if(mPreviewSize.height >= mPreviewSize.width)
                ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
            else
                ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;

            // One of these methods should be used, second method squishes preview slightly
            setMeasuredDimension(width, (int) (width * ratio));
            //        setMeasuredDimension((int) (width * ratio), height);
        }
    }

    public Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        if (sizes==null) return null;

        Camera.Size optimalSize = null;
        double ratio = (double)h/w;
        double minDiff = Double.MAX_VALUE;
        double newDiff;
        for (Camera.Size size : sizes) {
            newDiff = Math.abs((double)size.width/size.height - ratio);
            if (newDiff < minDiff) {
                optimalSize = size;
                minDiff = newDiff;
            }
        }
        return optimalSize;
    }


    public void refreshCamera(Camera camera) {
        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }
        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }
        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        mCamera = camera;
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        } catch (Exception e) {
            Log.d(VIEW_LOG_TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    private void setFocus(String mParameter) {
        Camera.Parameters mParameters = mCamera.getParameters();
        mParameters.setFocusMode(mParameter);
        mCamera.setParameters(mParameters);
    }

}

package com.lydor.mygame.networkcalling;


import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.model.AboutUsResponse;
import com.lydor.mygame.model.ChallengesDetailsResponse;
import com.lydor.mygame.model.FollowUnfollowRequest;
import com.lydor.mygame.model.FollowUnfollowResponse;
import com.lydor.mygame.model.FollowersResponse;
import com.lydor.mygame.model.FollowingResponse;
import com.lydor.mygame.model.ForgetPasswordRequest;
import com.lydor.mygame.model.ForgetPasswordResponse;
import com.lydor.mygame.model.LoginRequest;
import com.lydor.mygame.model.LoginResponse;
import com.lydor.mygame.model.LoginResponseNew;
import com.lydor.mygame.model.MyCHallengesAcceptedRequest;
import com.lydor.mygame.model.MyChallengesResponse;
import com.lydor.mygame.model.MyFeedResponse;
import com.lydor.mygame.model.MyProfileMyCHallengesResponse;
import com.lydor.mygame.model.MyProfileMyFeedListResponse;
import com.lydor.mygame.model.OtherUserDetailsRequest;
import com.lydor.mygame.model.OtherUsersResponse;
import com.lydor.mygame.model.PostVideoMyFeedResponse;
import com.lydor.mygame.model.PrivacyPolicyResponse;
import com.lydor.mygame.model.RegistrationRequest;
import com.lydor.mygame.model.RegistrationResponse;
import com.lydor.mygame.model.RegistrationResponseNew;
import com.lydor.mygame.model.ReportAProblemRequest;
import com.lydor.mygame.model.ReportAProblemResponse;
import com.lydor.mygame.model.SearchUserRequest;
import com.lydor.mygame.model.SearchUserResponse;
import com.lydor.mygame.model.UpdateUserDetails;
import com.lydor.mygame.model.UserMyFeedRequest;
import com.lydor.mygame.model.UserMyProfileRequest;
import com.lydor.mygame.model.UserMyProfileResponse;
import com.lydor.mygame.model.VideoLikeUnLikeRequest;
import com.lydor.mygame.model.VideoLikeUnlikeResponse;
import com.lydor.mygame.utils.Constants;


import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by HP on 8/19/2017.
 */


public interface RestService {




    @POST(Constants.URL_REGISTERATION)
    Call<RegistrationResponse> postRegistration(@Body() RegistrationRequest registrationRequest);

    @POST(Constants.URL_LOGIN)
    Call<LoginResponseNew> postLogin(@Body() LoginRequest loginRequest);


    @GET(Constants.URL_GET_MYFEED)
    Call<MyFeedResponse> getMyFeedList(@Query("page") int page);


    @GET(Constants.URL_GET_MYFEED_FOLLOWERES)
    Call<MyFeedResponse> getMyFeedListFollowing(@Query("page") int page);


    @Multipart
    @POST(Constants.URL_POST_VIDEO_FEED)
    Call<PostVideoMyFeedResponse> postVideoFeed(@Part MultipartBody.Part video,
                                                @Part("title") RequestBody title,
                                                @Part("description") RequestBody description,
                                                @Part("video_type") RequestBody video_type);


    @GET(Constants.URL_MYACTIVE_CHALLENGES)
    Call<MyChallengesResponse> getChallengesListing(@Query("page") int page);

    @GET(Constants.URL_MYPAST_CHALLENGES)
    Call<MyChallengesResponse> getPastChallengeListing(@Query("page") int page);


    @POST(Constants.URL_ACCEPTED_CHALLENGES_LIST)
    Call<ChallengesDetailsResponse> getAcceptedChallengesList(@Query("page") int page, @Body MyCHallengesAcceptedRequest myCHallengesAcceptedRequest);

    @Multipart
    @POST(Constants.URL_POST_VIDEO_FEED)
    Call<PostVideoMyFeedResponse> postChallengesAcceptedVideo(@Part MultipartBody.Part video,
                                                @Part("title") RequestBody title,
                                                @Part("description") RequestBody description,
                                                              @Part("challenge_id") RequestBody challengeId);

    @POST(Constants.URL_VIDEO_LIKE_UNLIKE)
    Call<VideoLikeUnlikeResponse> postVideoLikeUnlike(@Body VideoLikeUnLikeRequest videoLikeUnLikeRequest);

    @POST(Constants.URL_GET_USERS)
    Call<ArrayList<UserMyProfileResponse>> postGettingUserDetails();


    @POST(Constants.URL_GET_USERS)
    Call<ArrayList<OtherUsersResponse>> postGetOtherUserDetails(@Body OtherUserDetailsRequest request);

    @GET(Constants.URL_GET_FOLLOWERS_LIST)
    Call<ArrayList<FollowersResponse>> getFollowerList();

    @GET(Constants.URL_GET_FOLLOWERS_LIST)
    Call<ArrayList<FollowersResponse>> searchFollowersList(@Query("name") String name);


    @GET(Constants.URL_GET_FOLLOWING_LIST)
    Call<ArrayList<FollowingResponse>> getFollowingList();


    @GET(Constants.URL_GET_FOLLOWING_LIST)
    Call<ArrayList<FollowingResponse>> searchFollowingList(@Query("name") String name);


    @POST(Constants.URL_MYPROFILE_MYVIDEO)
    Call<MyProfileMyFeedListResponse> getPostMyProfileMyVideo();


    @POST(Constants.URL_MYPROFILE_MYVIDEO)
    Call<MyProfileMyFeedListResponse> getPostUserProfileMyFeed(@Body UserMyFeedRequest userMyFeedRequest);


    @POST(Constants.URL_MYPROFILE_MYCHALLENGES)
    Call<MyProfileMyFeedListResponse> getMyProfileMyChallenges(@Query("page") int page);


    @POST(Constants.URL_GET_USER_PROFILE_MOMENTS)
    Call<MyProfileMyFeedListResponse> getProfileMoments(@Query("page") int page,@Body UserMyFeedRequest request);


    @POST(Constants.URL_GET_USER_PROFILE_PERFORMANCE)
    Call<MyProfileMyFeedListResponse> getProfilePerformance(@Query("page") int page,@Body UserMyFeedRequest request);



    @POST(Constants.URL_MYPROFILE_MYCHALLENGES)
    Call<MyProfileMyFeedListResponse> getPostUserMyAcceptedChallenges(@Query("page") int page,@Body UserMyFeedRequest userMyFeedRequest);


    @Multipart
    @POST(Constants.URL_UPDATE_MYPROFILE)
    Call<UpdateUserDetails> updateUserProfile(@Part MultipartBody.Part picture,
                                              @Part("name") RequestBody name,
                                              @Part("user_tag") RequestBody user_tag,
                                              @Part("user_bio") RequestBody user_bio);


    @Multipart
    @POST(Constants.URL_UPDATE_MYPROFILE)
    Call<UpdateUserDetails> updateUserProfileWithoutUserName(@Part MultipartBody.Part picture,
                                              @Part("name") RequestBody name,
                                              @Part("user_bio") RequestBody user_bio);


    @POST(Constants.URL_FOLLOW_UNFOLLOW)
    Call<FollowUnfollowResponse> getFollowUnfollow(@Body FollowUnfollowRequest followUnfollowRequest);


    @POST(Constants.URL_REPORT_A_PROBLEM)
    Call<ReportAProblemResponse> postReportAProblem(@Body ReportAProblemRequest reportAProblemRequest);


    @POST(Constants.URL_SEARCH_USER)
    Call<ArrayList<SearchUserResponse>> postSearchUser(@Body SearchUserRequest request);


    @GET(Constants.URL_ABOUT_US)
    Call<AboutUsResponse> getAboutUsDetails();

    @GET(Constants.URL_PRIVACY_POLICY)
    Call<PrivacyPolicyResponse> getPrivacyPolicyResponse();


    @POST(Constants.URL_FORGET_PASSWORD)
    Call<ForgetPasswordResponse> postForgetPassword(@Body ForgetPasswordRequest request);

}






package com.lydor.mygame.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;

import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activity.BaseActivity;
import com.lydor.mygame.activity.FollowersActivity;
import com.lydor.mygame.activity.LoginActivity;
import com.lydor.mygame.activity.PrivacyPolicyActivity;
import com.lydor.mygame.activity.ReportUsActivity;
import com.lydor.mygame.activity.SearchUserActivity;
import com.lydor.mygame.fragmentss.HomeFragment;
import com.lydor.mygame.fragmentss.MoviesFragment;
import com.lydor.mygame.model.LoginResponse;
import com.lydor.mygame.model.LoginResponseNew;
import com.lydor.mygame.utils.Constants;
import com.suke.widget.SwitchButton;

import java.io.File;


public class MainActivity extends BaseActivity {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    //private ImageView imgNavHeaderBg, imgProfile;
   // private TextView txtWebsite;//txtName,
    private Toolbar toolbar;
    private FloatingActionButton fab;

    // urls to load navigation header background image
    // and profile image
    private static final String urlNavHeaderBg = "http://api.androidhive.info/images/nav-menu-header-bg.jpg";
    private static final String urlProfileImg = "https://lh3.googleusercontent.com/eCtE_G34M9ygdkmOpYvCag1vBARCmZwnVS6rS5t4JLzJ6QgQSBquM0nuTsCpLhYbKljoyS-txg";

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_PHOTOS = "photos";
    private static final String TAG_MOVIES = "movies";
    private static final String TAG_NOTIFICATIONS = "notifications";
    private static final String TAG_SETTINGS = "settings";
    public static String CURRENT_TAG = TAG_HOME;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;

    private ImageView crossImg;
    private TextView tvFeed,tvMyProfile,notificationTv,tvAboutUs,tvPrivacyPolicy,tvReporProblem,tvFollowing,tvClearCache,tvLogout;
    /*SwitchButton switchon;
    TextView on,off;*/
    ImageView drawerIcon, imgOnOff,searchUser;

    boolean isNotification = false;
    SharedPref preferences;
    GridLayout gridLay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerIcon = (ImageView) findViewById(R.id.drawerIcon);
        gridLay = (GridLayout) findViewById(R.id.gridLay);
        searchUser = (ImageView) findViewById(R.id.searchUser);

        //setSupportActionBar(toolbar);

        /*if(getIntent().getExtras().getParcelable("obj")!=null){

            LoginResponseNew response = getIntent().getExtras().getParcelable("obj");
            Log.v("Login Response: ","Login Response: "+response.getName());
        }*/


        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        imgOnOff = (ImageView) findViewById(R.id.imgOnOff);
        /*switchon = findViewById(R.id.no_switch);
        off = findViewById(R.id.no_off);
        on = findViewById(R.id.no_on);*/



        crossImg = navigationView.findViewById(R.id.crossImage);
        tvFeed = navigationView.findViewById(R.id.tvFeed);
        tvMyProfile = navigationView.findViewById(R.id.tvMyProfile);
        notificationTv = navigationView.findViewById(R.id.notificationTv);
        tvAboutUs = navigationView.findViewById(R.id.tvAboutUs);
        tvPrivacyPolicy = navigationView.findViewById(R.id.tvPrivacyPolicy);
        tvReporProblem = navigationView.findViewById(R.id.tvReportProblem);
        tvFollowing = navigationView.findViewById(R.id.tvFollowing);
        tvClearCache = navigationView.findViewById(R.id.tvClearCache);
        tvLogout = navigationView.findViewById(R.id.tvLogout);
        preferences = new SharedPref(this);

        tvClearCache.setVisibility(View.GONE);

//        setOnOff(View.GONE,View.VISIBLE);


        if(preferences.getBoolean(Constants.FROM_MENU)){
            preferences.putBoolean(Constants.FROM_MENU,false);
            drawer.openDrawer(Gravity.LEFT);
        }else{
            drawer.closeDrawer(Gravity.LEFT);
        }


        imgOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNotification){
                    isNotification = false;
                    imgOnOff.setImageDrawable(getResources().getDrawable(R.drawable.notification_off));
                }else{
                    isNotification = true;
                    imgOnOff.setImageDrawable(getResources().getDrawable(R.drawable.notification_on));
                }
            }
        });

        gridLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this,NotifiActivty.class);
                startActivity(in);
            }
        });


        drawerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeFragment) getSupportFragmentManager().findFragmentById(R.id.frame)).callChildFragmentMethod();
                /*HomeFragment fragment = (HomeFragment) getFragmentManager().findFragmentByTag(“FragTagName”);
                fragment.specific_function_name();*/
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        tvFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.putBoolean(Constants.FROM_MENU,true);
                Intent in = new Intent(MainActivity.this, FollowersActivity.class);
                startActivity(in);
            }
        });


        tvAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.putBoolean(Constants.FROM_MENU,true);
                Intent in = new Intent(MainActivity.this, com.lydor.mygame.activity.AboutUsActivity.class);
                startActivity(in);
            }
        });

        tvPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.putBoolean(Constants.FROM_MENU,true);
                Intent in = new Intent(MainActivity.this,PrivacyPolicyActivity.class);
                startActivity(in);
            }
        });

        tvReporProblem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.putBoolean(Constants.FROM_MENU,true);
                Intent in = new Intent(MainActivity.this, ReportUsActivity.class);
                startActivity(in);
            }
        });

        tvClearCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.putBoolean(Constants.FROM_MENU,true);
                deleteCache(MainActivity.this);
            }
        });


        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showToast("Successfully logged out");
                Intent in = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(in);
                preferences.logout();
                finish();
            }
        });

        //crossImg = findViewById(R.id.crossImage);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        //txtName = (TextView) navHeader.findViewById(R.id.name);
        //txtWebsite = (TextView) navHeader.findViewById(R.id.website);
        //imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        //imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);



        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }


        crossImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeFragment) getSupportFragmentManager().findFragmentById(R.id.frame)).callStartPlayer();
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        tvFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadHomeFragment();
            }
        });

        tvMyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.putBoolean(Constants.FROM_MENU,true);
                Intent in = new Intent(MainActivity.this,AboutUsActivity.class);
                startActivity(in);

            }
        });

        notificationTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.putBoolean(Constants.FROM_MENU,true);
                Intent in = new Intent(MainActivity.this,NotifiActivty.class);
                startActivity(in);


            }
        });


        /*switchon.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {

                if(isChecked){
                    setOnOff(View.VISIBLE, View.GONE);
                }else{
                    setOnOff(View.GONE, View.VISIBLE);
                }

            }
        });*/


        searchUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, SearchUserActivity.class);
                startActivity(in);
            }
        });
    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {
        // name, website
        //txtName.setText("Ravi Tamada");
        //txtWebsite.setText("www.androidhive.info");

        // loading header background image
        /*Glide.with(this).load(urlNavHeaderBg)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg);*/

        // Loading profile image
        /*Glide.with(this).load(urlProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);*/

        // showing dot next to notifications label
        //navigationView.getMenu().getItem(1).setActionView(R.layout.menu_dot);
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        //selectNavMenu();

        // set toolbar title
        //setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button
            toggleFab();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();

            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            default:
                return new HomeFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        //navigationView.getMenu().getItem(navItemIndex).setChecked(true);
        //navigationView.inflateMenu(R.layout.custom_item);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;

                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, null, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        //drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        //actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        preferences.putBoolean(Constants.FROM_MENU,false);
        finishAffinity();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.main, menu);
        }

        // when fragment is notifications, load the menu created for notifications
        if (navItemIndex == 3) {
            getMenuInflater().inflate(R.menu.notifications, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            Toast.makeText(getApplicationContext(), "Logout user!", Toast.LENGTH_LONG).show();
            return true;
        }

        // user is in notifications fragment
        // and selected 'Mark all as Read'
        if (id == R.id.action_mark_all_read) {
            Toast.makeText(getApplicationContext(), "All notifications marked as read!", Toast.LENGTH_LONG).show();
        }

        // user is in notifications fragment
        // and selected 'Clear All'
        if (id == R.id.action_clear_notifications) {
            Toast.makeText(getApplicationContext(), "Clear all notifications!", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

    // show or hide the fab
    private void toggleFab() {
        if (navItemIndex == 0)
            fab.hide();
        else
            fab.hide();
    }


    /*void setOnOff(int foron, int foroff) {
        on.setVisibility(foron);
        off.setVisibility(foroff);
    }*/


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


}

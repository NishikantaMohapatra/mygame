package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MyCHallengesAcceptedRequest implements Parcelable {

    private int challenge_id;

    public MyCHallengesAcceptedRequest() {
    }

    public int getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(int challenge_id) {
        this.challenge_id = challenge_id;
    }

    public static Creator<MyCHallengesAcceptedRequest> getCREATOR() {
        return CREATOR;
    }

    protected MyCHallengesAcceptedRequest(Parcel in) {
        challenge_id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(challenge_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MyCHallengesAcceptedRequest> CREATOR = new Creator<MyCHallengesAcceptedRequest>() {
        @Override
        public MyCHallengesAcceptedRequest createFromParcel(Parcel in) {
            return new MyCHallengesAcceptedRequest(in);
        }

        @Override
        public MyCHallengesAcceptedRequest[] newArray(int size) {
            return new MyCHallengesAcceptedRequest[size];
        }
    };
}

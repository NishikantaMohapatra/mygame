package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ReportAProblemRequest implements Parcelable {

    private String log;

    public ReportAProblemRequest() {
    }

    protected ReportAProblemRequest(Parcel in) {
        log = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(log);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReportAProblemRequest> CREATOR = new Creator<ReportAProblemRequest>() {
        @Override
        public ReportAProblemRequest createFromParcel(Parcel in) {
            return new ReportAProblemRequest(in);
        }

        @Override
        public ReportAProblemRequest[] newArray(int size) {
            return new ReportAProblemRequest[size];
        }
    };

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}

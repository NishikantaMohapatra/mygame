package com.lydor.mygame.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.lydor.mygame.R;
import com.lydor.mygame.reciever.ServiceReceiver;


/**
 * Created by NISHIKANT on 11/30/2018.
 */

public class BaseActivity extends AppCompatActivity
{
    private ProgressDialog mProgressDialog;
    protected ServiceReceiver mServiceReceiver;
    protected Handler mHandler;
    protected String TITLE=BaseActivity.class.getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showToast(String msg)
    {
        Toast.makeText(this,msg, Toast.LENGTH_SHORT).show();
    }

    public void enableDialog(String msg)
    {
        mProgressDialog= ProgressDialog.show(this,null,msg);
    }

    public void disableDialog()
    {
        if(mProgressDialog!=null)
        {
            mProgressDialog.dismiss();
            mProgressDialog=null;
        }
    }

    public void createReciever()
    {
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);


    }

    @Override
    protected void onStart() {
        super.onStart();
        //RegPrefManager.getInstance(this).setOpened(1);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //RegPrefManager.getInstance(this).setOpened(0);
    }

    /*public void showNoInternetConnection()
    {
        showCloseDialog(TITLE,
                "No Internet Connection");
    }
    public void showSomethingWentWrong()
    {
        showCloseDialog(TITLE,
               "Some thing went wrong");
    }*/
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        disableDialog();
        super.onSaveInstanceState(outState);
    }


   /* public void showCloseDialog(String title, String message)
    {
        View view= getLayoutInflater().inflate(R.layout.dialog_textview_one_button,
                null,false);

        TextView tvTitle=view.findViewById(R.id.tvTitle);
        TextView tvMessage=view.findViewById(R.id.tvMessage);
        TextView tvCancel=view.findViewById(R.id.tvNo);

        tvTitle.setText(title);
        tvMessage.setText(message);

        tvCancel.setText("OK");
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(null);
        builder1.setCancelable(false);

        builder1.setView(view);
        final AlertDialog alert11 = builder1.create();

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert11.dismiss();

            }
        });

        alert11.show();
    }
    public void showPopupDialog(String title,
                                String message, final boolean finish)
    {
        View view= getLayoutInflater().inflate(R.layout.dialog_textview_one_button,
                null,false);

        TextView tvTitle=view.findViewById(R.id.tvTitle);
        TextView tvMessage=view.findViewById(R.id.tvMessage);
        TextView tvCancel=view.findViewById(R.id.tvNo);

        tvTitle.setText(title);
        tvMessage.setText(message);

        tvCancel.setText("OK");
        android.support.v7.app.AlertDialog.Builder builder1 = new android.support.v7.app.AlertDialog.Builder(this);
        builder1.setMessage(null);
        builder1.setCancelable(false);

        builder1.setView(view);
        final android.support.v7.app.AlertDialog alert11 = builder1.create();

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert11.dismiss();
                if(finish)
                    finish();
                else
                    onYesClick(finish);
            }
        });
        alert11.setCancelable(false);
        alert11.show();
    }

     public void onYesClick(boolean yes)
     {

     }

    public void noTitleNoInternetDialog(
            String message)
    {
        View view= getLayoutInflater().inflate(R.layout.dialog_textview_one_button_no_title,
                null,false);

        TextView tvMessage=view.findViewById(R.id.tvMessage);
        TextView tvCancel=view.findViewById(R.id.tvNo);
        tvMessage.setText(message);

        tvCancel.setText(getResources().getString(R.string.text_ok));
        android.support.v7.app.AlertDialog.Builder builder1 = new android.support.v7.app.AlertDialog.Builder(this);
        builder1.setMessage(null);
        builder1.setCancelable(false);

        builder1.setView(view);
        final android.support.v7.app.AlertDialog alert11 = builder1.create();

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert11.dismiss();

            }
        });
        alert11.show();
    }

    public void deleteGuesta(int p)
    {

    }*/
}


package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ForgetPasswordResponse implements Parcelable {

    private String message;

    public ForgetPasswordResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static Creator<ForgetPasswordResponse> getCREATOR() {
        return CREATOR;
    }

    protected ForgetPasswordResponse(Parcel in) {
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ForgetPasswordResponse> CREATOR = new Creator<ForgetPasswordResponse>() {
        @Override
        public ForgetPasswordResponse createFromParcel(Parcel in) {
            return new ForgetPasswordResponse(in);
        }

        @Override
        public ForgetPasswordResponse[] newArray(int size) {
            return new ForgetPasswordResponse[size];
        }
    };
}

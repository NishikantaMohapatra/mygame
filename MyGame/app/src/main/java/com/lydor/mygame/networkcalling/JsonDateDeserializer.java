package com.lydor.mygame.networkcalling;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.lydor.mygame.utils.DateTimeUtil;


import java.lang.reflect.Type;
import java.util.Date;

public class JsonDateDeserializer
    implements JsonDeserializer<Date> {

    public static final String TAG=JsonDateSerializer.class.getSimpleName();
    public Date deserialize(JsonElement json,
                            Type date,
                            JsonDeserializationContext context)
        throws JsonParseException {


        return DateTimeUtil.StringFcmDateStrToDate(json.getAsJsonPrimitive().getAsString());
    }
}
package com.lydor.mygame.activity;


import androidx.annotation.RequiresApi;

import android.content.Intent;

import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;

import com.lydor.mygame.callingnetwork.ApiClient;

import com.lydor.mygame.model.PostVideoMyFeedResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;


import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostTypeActivity extends BaseActivity {

    TextView tvPost;
    String videoPath;
    EditText etDesciprtion;
    String challengeId,fromActivity;
    ImageView imvBack;
    SharedPref preferences;
    String momOrPerf;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_type);


        imvBack = findViewById(R.id.imvBack);
        tvPost = findViewById(R.id.tvOtp);
        etDesciprtion = findViewById(R.id.etDesciprtion);
        preferences = new SharedPref(this);

        if(getIntent().getStringExtra("videoPath")!=null){

            videoPath = getIntent().getStringExtra("videoPath");
        }

        if(getIntent().getStringExtra("fromActivity")!=null){

            fromActivity = getIntent().getStringExtra("fromActivity");
            if(getIntent().getStringExtra("fromActivity").equalsIgnoreCase("challenges")){

                challengeId = getIntent().getStringExtra("id");

            }else{
                momOrPerf = getIntent().getStringExtra("mOrp");
                Log.v("momOrPerf",momOrPerf);
            }
        }



        tvPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(etDesciprtion.getText().toString().isEmpty()){

                    showToast("Please enter video details");
                }else{
                    if(fromActivity.equalsIgnoreCase("challenges")){


                        postChallengesAcceptedVideo();



                    }else{


                        postMyFeedVideo();


                    }
                }



            }
        });

        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(PostTypeActivity.this,CustomCameraActivity.class);

                if(getIntent().getStringExtra("fromActivity").equalsIgnoreCase("challenges")){

                    in.putExtra("From","challenges");
                    in.putExtra("id",getIntent().getStringExtra("id"));
                    // challengeId = getIntent().getStringExtra("id");

                }else{
                    in.putExtra("From","Normal");
                }
                startActivity(in);
                finish();
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(PostTypeActivity.this,CustomCameraActivity.class);

        if(getIntent().getStringExtra("fromActivity").equalsIgnoreCase("challenges")){

            in.putExtra("From","challenges");
            in.putExtra("id",getIntent().getStringExtra("id"));
           // challengeId = getIntent().getStringExtra("id");

        }else{
            in.putExtra("From","Normal");
        }
        startActivity(in);
        finish();
    }


    private void postMyFeedVideo(){
        if (AppUtil.isInternetConnected(PostTypeActivity.this)) {
            enableDialog("Please wait...");

            File videoFile = new File(videoPath);
            RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), videoFile);
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("video", videoFile.getName(), videoBody);

            RequestBody title =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf("video test"));


            RequestBody description =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(etDesciprtion.getText().toString()));
            RequestBody videoType;
            if(momOrPerf.equalsIgnoreCase("fromMoments")){
                videoType = RequestBody.create(MediaType.parse("multipart/form-data"),"1");
            }else{
                videoType = RequestBody.create(MediaType.parse("multipart/form-data"),"2");
            }



            RestService apiService =
                    ApiClient.getClient(this).create(RestService.class);

            Call<PostVideoMyFeedResponse> call = apiService.postVideoFeed(vFile,title,description,videoType);
            call.enqueue(new Callback<PostVideoMyFeedResponse>() {
                @Override
                public void onResponse(Call<PostVideoMyFeedResponse> call, Response<PostVideoMyFeedResponse> response) {


                    disableDialog();



                    if(response.code()==201){
                        PostVideoMyFeedResponse postVideoMyFeedResponse = response.body();
                        if(postVideoMyFeedResponse.getStatus().equalsIgnoreCase("ok")){
                            Toast.makeText(PostTypeActivity.this,
                                    "Video posted successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Toast.makeText(PostTypeActivity.this, "" + postVideoMyFeedResponse.getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    }else {

                        if(response.code()==401){
                            Toast.makeText(PostTypeActivity.this, "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                            Intent in = new Intent(PostTypeActivity.this, LoginActivity.class);
                            startActivity(in);
                            preferences.logout();
                            finish();
                            return;
                        }

                        else  {

                            Toast.makeText(PostTypeActivity.this, response.message()+"", Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }

                }

                @Override
                public void onFailure(Call<PostVideoMyFeedResponse> call, Throwable t) {

                    disableDialog();
                    showToast(t.toString());
                }
            });




        } else {
            disableDialog();
            Toast.makeText(PostTypeActivity.this,
                    "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }

    private void postChallengesAcceptedVideo(){
        if (AppUtil.isInternetConnected(PostTypeActivity.this)) {
            enableDialog("Please wait...");

            File videoFile = new File(videoPath);
            RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), videoFile);
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("video", videoFile.getName(), videoBody);

            RequestBody title =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf("video test"));


            RequestBody description =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(etDesciprtion.getText().toString()));

            RequestBody challengesId =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(challengeId));


            RestService apiService =
                    ApiClient.getClient(this).create(RestService.class);

            Call<PostVideoMyFeedResponse> call = apiService.postChallengesAcceptedVideo(vFile,title,description,challengesId);
            call.enqueue(new Callback<PostVideoMyFeedResponse>() {
                @Override
                public void onResponse(Call<PostVideoMyFeedResponse> call, Response<PostVideoMyFeedResponse> response) {


                    disableDialog();



                    if(response.code()==201){
                        PostVideoMyFeedResponse postVideoMyFeedResponse = response.body();
                        if(postVideoMyFeedResponse.getStatus().equalsIgnoreCase("ok")){
                            Toast.makeText(PostTypeActivity.this, "Challenge posted successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Toast.makeText(PostTypeActivity.this, "" + postVideoMyFeedResponse.getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    }else {


                        if (response != null && response.errorBody() != null) {
                            Toast.makeText(PostTypeActivity.this, "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                            Intent in = new Intent(PostTypeActivity.this, LoginActivity.class);
                            startActivity(in);
                            preferences.logout();
                            finish();
                            return;
                        }

                    }


                }

                @Override
                public void onFailure(Call<PostVideoMyFeedResponse> call, Throwable t) {

                    disableDialog();
                    showToast(t.toString());
                }
            });




        } else {
            disableDialog();
            Toast.makeText(PostTypeActivity.this,
                    "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }

}

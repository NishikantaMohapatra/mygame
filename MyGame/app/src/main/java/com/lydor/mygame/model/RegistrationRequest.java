package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RegistrationRequest implements Parcelable {

    private String name;
    private String email;
    private String mobile;
    private String password;
    private String password_confirm;

    public RegistrationRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_confirm() {
        return password_confirm;
    }

    public void setPassword_confirm(String password_confirm) {
        this.password_confirm = password_confirm;
    }

    public static Creator<RegistrationRequest> getCREATOR() {
        return CREATOR;
    }

    protected RegistrationRequest(Parcel in) {
        name = in.readString();
        email = in.readString();
        mobile = in.readString();
        password = in.readString();
        password_confirm = in.readString();
    }

    public static final Creator<RegistrationRequest> CREATOR = new Creator<RegistrationRequest>() {
        @Override
        public RegistrationRequest createFromParcel(Parcel in) {
            return new RegistrationRequest(in);
        }

        @Override
        public RegistrationRequest[] newArray(int size) {
            return new RegistrationRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(email);
        parcel.writeString(mobile);
        parcel.writeString(password);
        parcel.writeString(password_confirm);
    }
}

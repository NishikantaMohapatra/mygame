package com.lydor.mygame.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Annotation;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activities.MainActivity;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.LoginRequest;
import com.lydor.mygame.model.LoginResponse;
import com.lydor.mygame.model.LoginResponseNew;
import com.lydor.mygame.model.RegistrationRequest;
import com.lydor.mygame.model.RegistrationResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    private static final String TAG = "Login";
    private TextView tvFrgtPass,tvRegister,tvLogin;
    private ImageView imvBack;
    private EditText etPassword,etEmail;
    private static final int REQUEST_PERMISSIONS = 100;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    public static final int REQUEST_CAMERA_CODE = 100;


    SharedPref preferences;
    CallbackManager callbackManager;
    LoginButton loginButton;
    RelativeLayout fbLogin;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        printHashKey(LoginActivity.this);
        callbackManager = CallbackManager.Factory.create();
        tvFrgtPass = findViewById(R.id.tvFrgtPass);
        tvRegister = findViewById(R.id.tvRegister);
        imvBack = findViewById(R.id.imvBack);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        tvLogin = findViewById(R.id.tvLogin);
        loginButton = findViewById(R.id.login_button);
        fbLogin = findViewById(R.id.fbLogin);

        preferences = new SharedPref(this);

        //checkPermission(this);

        final String fcmId = FirebaseInstanceId.getInstance().getToken();
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        token = task.getResult().getToken();

                        // Log and toast
                        // msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, token);
                        Log.d(TAG + "Fcm id is :- ", fcmId);
                        //Toast.makeText(LoginActivity.this, token, Toast.LENGTH_SHORT).show();
                    }
                });

        Typeface type = Typeface.createFromAsset(getAssets(),"font/titilliumregular.otf");
        etPassword.setTypeface(type);
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });

        tvFrgtPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(LoginActivity.this,ForgetPasswordActivity.class);
                startActivity(in);
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(LoginActivity.this,RegistrationActivity.class);
                in.putExtra("from","registration");
                startActivity(in);
            }
        });

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (etEmail.getText().toString().isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();
                } else if (!etEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(LoginActivity.this, "Please enter a valid email", Toast.LENGTH_SHORT).show();
                } else if (etPassword.getText().toString().isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Please enter a password", Toast.LENGTH_SHORT).show();
                }else{

                    prepareLogin();
                    /*Intent in = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(in);*/
                }

            }
        });


        List< String > permissionNeeds = Arrays.asList("user_photos", "email",
                "user_birthday", "public_profile", "AccessToken");

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

            ProgressDialog progress = new ProgressDialog(LoginActivity.this);
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("Facebook","Facebook response::"+response.toString());
                                        try {
                                            progress.dismiss();
                                            Intent in = new Intent(getApplicationContext(), RegistrationActivity.class);
                                            in.putExtra("userName", object.getString("name"));
                                            in.putExtra("userEmail", object.getString("email"));
                                            in.putExtra("from","fbLogin");
                                            startActivity(in);
                                            String name = object.getString("name");
                                            String email = object.getString("email");

                                        } catch (JSONException e) {
                                            progress.dismiss();
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "name,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                        progress.dismiss();
                    }

                    @Override
                    public void onCancel() {
                        Log.d("mydata", "Login Cancelled");
                        progress.dismiss();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d("mydata", exception.toString());
                        progress.dismiss();
                        if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        }
                    }
                });

        fbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //loginButton.performClick();
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile","email"));
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public  boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.
                    WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.
                            READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.
                            CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.
                            RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                        context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                //Do Nothing
                return true;
            }
        } else {
            //Do Nothing
            return true;
        }
    }

    private void prepareLogin(){
        if (AppUtil.isInternetConnected(LoginActivity.this)) {
            enableDialog("Please wait...");
            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setEmail(etEmail.getText().toString());
            loginRequest.setPassword(etPassword.getText().toString());
            loginRequest.setDevice_id(token);


            RestService apiService =
                    ApiClient.getClient(this).create(RestService.class);

            Call<LoginResponseNew> call = apiService.postLogin(loginRequest);
            call.enqueue(new Callback<LoginResponseNew>() {
                @Override
                public void onResponse(Call<LoginResponseNew> call, Response<LoginResponseNew> response) {


                    disableDialog();



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            //LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(LoginActivity.this, Constants.SOME_THING_WRONG, Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                    LoginResponseNew successresponse = response.body();
                    if(successresponse.getStatus().equalsIgnoreCase("ok")){
//                        Toast.makeText(LoginActivity.this, "" + successresponse.getStatus(), Toast.LENGTH_SHORT).show();

                        Intent in = new Intent(LoginActivity.this,MainActivity.class);
                        in.putExtra("obj",successresponse);
                        startActivity(in);

                        preferences.putBoolean(Constants.LOGIN_STATUS,true);
                        preferences.putString(Constants.USER_TOKEN,successresponse.getToken());
                        preferences.putString(Constants.USER_NAME,successresponse.getName());
                        preferences.putInt(Constants.USER_ID,successresponse.getId());
                        finish();
                    }else{
                        Toast.makeText(LoginActivity.this, "" + successresponse.getStatus(), Toast.LENGTH_SHORT).show();
                    }
                    }else{
                        showToast("Something went wrong!!!");
                    }


                }

                @Override
                public void onFailure(Call<LoginResponseNew> call, Throwable t) {

                    disableDialog();
                    showToast(t.toString());
                }
            });




        } else {
            disableDialog();
            Toast.makeText(LoginActivity.this, "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }


    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }
}

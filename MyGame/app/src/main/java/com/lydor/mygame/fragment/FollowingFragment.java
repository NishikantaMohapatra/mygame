package com.lydor.mygame.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activity.LoginActivity;
import com.lydor.mygame.adapter.FollowersListAdapter;
import com.lydor.mygame.adapter.FollowingListAdapter;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.FollowUnfollowRequest;
import com.lydor.mygame.model.FollowUnfollowResponse;
import com.lydor.mygame.model.FollowersResponse;
import com.lydor.mygame.model.FollowingResponse;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;
import com.lydor.mygame.utils.SimpleDividerItemDecoration;

import org.w3c.dom.Text;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FollowingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FollowingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FollowingFragment extends Fragment implements FollowingListAdapter.EventListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    RecyclerView rcvList;
    ArrayList<FollowingResponse> mList;
    FollowingListAdapter mAdapter;
    private ProgressDialog mProgressDialog;
    private SharedPref pref;
    private TextView errTv;
    private EditText etSearch;

    private FollowingFragment.OnFragmentInteractionListener mListener;

    public FollowingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FollowingFragment newInstance(String param1, String param2) {
        FollowingFragment fragment = new FollowingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_following, container, false);
        errTv = view.findViewById(R.id.errTv);
        rcvList = view.findViewById(R.id.rcvList);
        etSearch = view.findViewById(R.id.etSearch);
        mProgressDialog = new ProgressDialog(getActivity());
        pref = new SharedPref(getActivity());
        mList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rcvList.setLayoutManager(linearLayoutManager);
        mAdapter = new FollowingListAdapter(mList,getActivity(),this);
        rcvList.addItemDecoration(new SimpleDividerItemDecoration(getActivity(),10,true));
        rcvList.setAdapter(mAdapter);

        getFollowingList();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(etSearch.getText().toString().length()>=3){
                    searchFollowing(etSearch.getText().toString());
                }else{
                    searchFollowing("");
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void postFollowUnfollow(String followersId, int status) {
        postFollowUnfollowRequest(followersId,status);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void getFollowingList(){
        if (AppUtil.isInternetConnected(getActivity())) {
            mProgressDialog= ProgressDialog.show(getActivity(),null, Constants.PLEASE_WAIT);



            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);

            Call<ArrayList<FollowingResponse>> call = apiService.getFollowingList(
            );
            call.enqueue(new Callback<ArrayList<FollowingResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<FollowingResponse>> call, Response<ArrayList<FollowingResponse>> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        ArrayList<FollowingResponse> followingResponses = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        if(followingResponses.size()>0){
                            mList.clear();
                            mList.addAll(followingResponses);
                            rcvList.scrollToPosition(0);
                            mAdapter.notifyDataSetChanged();

                            rcvList.setVisibility(View.VISIBLE);
                            errTv.setVisibility(View.GONE);
                            etSearch.setVisibility(View.VISIBLE);
                        }else{
                            rcvList.setVisibility(View.GONE);
                            errTv.setVisibility(View.VISIBLE);
                            errTv.setText("You are not following anybody...");
                            mList.clear();
                            mAdapter.notifyDataSetChanged();
                            etSearch.setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), "Yet to follow somebody", Toast.LENGTH_SHORT).show();
                        }
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<ArrayList<FollowingResponse>> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(getActivity(), "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }


    public void postFollowUnfollowRequest(String followId,int status){
        if (AppUtil.isInternetConnected(getActivity())) {
            //mProgressDialog= ProgressDialog.show(getActivity(),null, Constants.PLEASE_WAIT);

            FollowUnfollowRequest request = new FollowUnfollowRequest();
            request.setFollowing_id(followId);
            request.setFollow(status);

            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);

            Call<FollowUnfollowResponse> call = apiService.getFollowUnfollow(request
            );
            call.enqueue(new Callback<FollowUnfollowResponse>() {
                @Override
                public void onResponse(Call<FollowUnfollowResponse> call, Response<FollowUnfollowResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        FollowUnfollowResponse followUnfollowResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        getFollowingList();
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<FollowUnfollowResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(getActivity(), "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){

            getFollowingList();
        }
    }


    public void searchFollowing(String name){
        if (AppUtil.isInternetConnected(getActivity())) {
            //mProgressDialog= ProgressDialog.show(getActivity(),null, Constants.PLEASE_WAIT);



            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);

            Call<ArrayList<FollowingResponse>> call = apiService.searchFollowingList(name
            );
            call.enqueue(new Callback<ArrayList<FollowingResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<FollowingResponse>> call, Response<ArrayList<FollowingResponse>> response) {





                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        ArrayList<FollowingResponse> followingResponses = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        if(followingResponses.size()>0){
                            mList.clear();
                            mList.addAll(followingResponses);
                            mAdapter.notifyDataSetChanged();
                            rcvList.setVisibility(View.VISIBLE);
                            errTv.setVisibility(View.GONE);
                        }else{
                            rcvList.setVisibility(View.GONE);
                            errTv.setVisibility(View.VISIBLE);
                            errTv.setText("You are not following anybody...");
                            mList.clear();
                            mAdapter.notifyDataSetChanged();
                            //Toast.makeText(getActivity(), "Yet to follow somebody", Toast.LENGTH_SHORT).show();
                        }
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<ArrayList<FollowingResponse>> call, Throwable t) {

                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {

            Toast.makeText(getActivity(), "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }
}

package com.lydor.mygame.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lydor.mygame.R;
import com.lydor.mygame.activity.VideoPlayerEditProfileActivity;
import com.lydor.mygame.activity.VideoPreviewActivityMyProfile;
import com.lydor.mygame.model.MyProfileMyFeedVideoListData;
import com.lydor.mygame.models.Model_Video;

import java.util.List;

public class MyProfileMyFeedAdapter extends RecyclerView.Adapter<MyProfileMyFeedAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private Context context;
    private List<MyProfileMyFeedVideoListData> testLists;
    private Context mcontext;
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public MyProfileMyFeedAdapter(List<MyProfileMyFeedVideoListData> testLists, Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.testLists = testLists;
    }


    // inflates the row layout from xml when needed
    @Override
    public MyProfileMyFeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_videos,
                parent, false);
        MyProfileMyFeedAdapter.ViewHolder viewHolder = new MyProfileMyFeedAdapter.ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the view and textview in each row
    @Override
    public void onBindViewHolder(MyProfileMyFeedAdapter.ViewHolder holder, final int position) {
        RelativeLayout container = holder.container;
        ImageView imgView = holder.imageView;
        ImageView imagViewNew = holder.iv_image_new;

        //File imgFile = new  File("file" + testLists.get(position).getStr_thumb());

        /*if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            imgView.setImageBitmap(myBitmap);

        }*/

        /*Bitmap thumbnails = ThumbnailUtils.createVideoThumbnail("file" +
                testLists.get(position).getStr_path(),MediaStore.Video.Thumbnails.MICRO_KIND);*/
        //imgView.setImageBitmap(testLists.get(position).getStr_thumb());
        //imgView.setImageBitmap(createVideoThumbNail(testLists.get(position).getStr_path()));

        if(testLists.get(position).getVideo_thumbnail()!=null){
            imagViewNew.setVisibility(View.VISIBLE);
            imgView.setVisibility(View.GONE);
            Glide.with(context).load("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+testLists.get(position).getVideo_thumbnail())
                    .skipMemoryCache(false)
                    .placeholder(R.drawable.photo)
                    .into(imagViewNew);
        }else{
            imagViewNew.setVisibility(View.GONE);
            imgView.setVisibility(View.VISIBLE);
            imgView.setBackground(context.getResources().getDrawable(R.drawable.photo));
        }





        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_gallery = new Intent(context, VideoPreviewActivityMyProfile.class);
                intent_gallery.putExtra("video","http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+testLists.get(position).getFile_name());
                intent_gallery.putExtra("obj",testLists.get(position));
                context.startActivity(intent_gallery);


            }
        });



    }


    // total number of rows
    @Override
    public int getItemCount() {
        return testLists.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public RelativeLayout container;
        public ImageView imageView,iv_image_new;

        public ViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.rl_select);
            imageView = itemView.findViewById(R.id.iv_image);
            iv_image_new = itemView.findViewById(R.id.iv_image_new);
        }
    }

    // allows clicks events to be caught

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);


    }

    public Bitmap createVideoThumbNail(String path){
        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
    }
}

package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.lydor.mygame.R;
import com.lydor.mygame.model.ChallengesDetailsAcceptedList;

public class VideoPreviewActiviyNew extends AppCompatActivity {

    VideoView videoView;
    ImageView cross;
    String str_video;
    ImageView imageView;
    Uri uri;
    boolean isPlay = false;
    ChallengesDetailsAcceptedList obj;
    TextView tvDescription,tvLikes,tvShare;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_preview_activiy_new);


        videoView = findViewById(R.id.videoView);
        cross = findViewById(R.id.cross);
        imageView = findViewById(R.id.imageView);
        tvDescription = findViewById(R.id.tvDescription);
        tvLikes = findViewById(R.id.tvLikes);
        tvShare = findViewById(R.id.tvShare);


        if(getIntent().getStringExtra("video")!=null){
            str_video = getIntent().getStringExtra("video");
            obj = getIntent().getParcelableExtra("obj");
        }




        if(obj.getDescription()!=null){
            tvDescription.setText(obj.getDescription());
        }else{
            tvDescription.setText("No description for this video");
        }

        tvLikes.setText(obj.getTotal_likes()+" Likes");

        videoView.setFitsSystemWindows(true);
        //videoView.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        uri = Uri.parse(str_video);
        videoView.setVideoURI(uri);
        videoView.start();

        if(videoView.isPlaying()){
            imageView.setImageResource(R.drawable.ic_pausesign);

        }
        isPlay=true;

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(videoView.isPlaying()){
                    videoView.stopPlayback();
                }
                finish();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isPlay){

                    videoView.pause();
                    isPlay = false;
                    imageView.setImageResource(R.drawable.ic_playsign);
                    //onPauseTimer();
                    //startTimer();
                }else if(!isPlay){
                    //t.start();
                    videoView.start();
                    isPlay = true;
                    //updateSeekBar();
                    //resumeTimer();

                    imageView.setImageResource(R.drawable.ic_pausesign);
                }
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                isPlay = false;
                imageView.setImageResource(R.drawable.ic_playsign);

            }
        });


        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareVid(str_video);
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(videoView.isPlaying()){
            videoView.stopPlayback();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(videoView.isPlaying()){
            videoView.stopPlayback();
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(videoView.isPlaying()){
            videoView.stopPlayback();
        }
    }

    public void shareVid(String videoUrl){
        String path = videoUrl; //should be local path of downloaded video
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        share.putExtra(Intent.EXTRA_TEXT, path);
        startActivity(Intent.createChooser(share, "Share link!"));


    }
}

package com.lydor.mygame.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lydor.mygame.R;
import com.lydor.mygame.activities.Activity_galleryview;
import com.lydor.mygame.activity.GalleryActivity;
import com.lydor.mygame.activity.VideoPlayerEditProfileActivity;
import com.lydor.mygame.activity.VideoPreviewActivity;
import com.lydor.mygame.models.Model_Video;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;



public class Adapter_VideoFolder extends RecyclerView.Adapter<Adapter_VideoFolder.ViewHolder> {

    private LayoutInflater mInflater;
    private Context context;
    private List<Model_Video> testLists;
    private Context mcontext;
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public Adapter_VideoFolder(List<Model_Video> testLists, Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.testLists = testLists;
    }


    // inflates the row layout from xml when needed
    @Override
    public Adapter_VideoFolder.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_videos,
                parent, false);
        Adapter_VideoFolder.ViewHolder viewHolder = new Adapter_VideoFolder.ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the view and textview in each row
    @Override
    public void onBindViewHolder(Adapter_VideoFolder.ViewHolder holder, final int position) {
        final RelativeLayout container = holder.container;
        final ImageView imgView = holder.imageView;
        final ImageView iv_image_new = holder.iv_image_new;

        //File imgFile = new  File("file" + testLists.get(position).getStr_thumb());

        /*if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            imgView.setImageBitmap(myBitmap);

        }*/

        /*Bitmap thumbnails = ThumbnailUtils.createVideoThumbnail("file" +
                testLists.get(position).getStr_path(),MediaStore.Video.Thumbnails.MICRO_KIND);*/
        //imgView.setImageBitmap(testLists.get(position).getStr_thumb());
        //imgView.setImageBitmap(createVideoThumbNail(testLists.get(position).getStr_path()));

        /*if(testLists.get(position).getStr_thumb()!=null) {
            //holder.imv.setTag(liveStreamLists.get(position).getImage());
            imgView.setVisibility(View.GONE);
            iv_image_new.setVisibility(View.VISIBLE);
            Picasso.with(context).load(testLists.get(position).getStr_thumb())
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap,
                                                   Picasso.LoadedFrom from) {

                            //mediaCoverImage.setImageBitmap(bitmap);

                            imgView.setVisibility(View.GONE);
                            iv_image_new.setVisibility(View.VISIBLE);
                            iv_image_new.setImageBitmap(bitmap);

                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            imgView.setVisibility(View.VISIBLE);
                            iv_image_new.setVisibility(View.GONE);
                            iv_image_new.setImageDrawable(
                                    context.getResources().
                                            getDrawable(R.drawable.photo)
                            );

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            imgView.setVisibility(View.VISIBLE);
                            iv_image_new.setVisibility(View.GONE);
                            iv_image_new.setImageDrawable(
                                    context.getResources().
                                            getDrawable(R.drawable.photo)
                            );
                        }
                    });
        }
        else
        {
            imgView.setVisibility(View.VISIBLE);
            iv_image_new.setVisibility(View.GONE);
            iv_image_new.setImageDrawable(
                    context.getResources().
                            getDrawable(R.drawable.photo)
            );

        }
*/


        Glide.with(context).load(testLists.get(position).getStr_thumb())
                .skipMemoryCache(false)
                .placeholder(R.drawable.thumbnailforgallery)
                .into(iv_image_new);




       container.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

                       Intent intent_gallery = new Intent(context, VideoPreviewActivity.class);
                       intent_gallery.putExtra("video",testLists.get(position).getStr_path());
                       intent_gallery.putExtra("from","adapter");
                       //intent_gallery.putExtra("video",testLists.get(position).getStr_path());
                       intent_gallery.putExtra("fromActivity","adapter");
                       context.startActivity(intent_gallery);
                       ((GalleryActivity)context).onFinishActivity();


           }
       });



    }


    // total number of rows
    @Override
    public int getItemCount() {
        return testLists.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public RelativeLayout container;
        public ImageView imageView,iv_image_new;

        public ViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.rl_select);
            imageView = itemView.findViewById(R.id.iv_image);
            iv_image_new = itemView.findViewById(R.id.iv_image_new);
        }
    }

    // allows clicks events to be caught

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);


    }

    public Bitmap createVideoThumbNail(String path){
        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
    }
}



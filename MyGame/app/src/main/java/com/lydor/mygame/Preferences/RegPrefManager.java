package com.lydor.mygame.Preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Nishikanta on 10/18/2016.
 */
public class RegPrefManager {

    private SharedPreferences mSharedPreferences;
    private static RegPrefManager mPrefManager;

    public Context mContext;

    private RegPrefManager(Context context) {
        this.mContext=context;
        mSharedPreferences = context.
                getSharedPreferences("com.lydor.mygame.pref", Context.MODE_PRIVATE);
    }

    public static RegPrefManager getInstance(Context context) {
        if (mPrefManager == null) {
            mPrefManager = new RegPrefManager(context);
        }
        return mPrefManager;
    }
    public void setDashboard(long v) {

        mSharedPreferences.edit().putLong("Dashboard", v).apply();
    }
    public int getOpened() {
        return mSharedPreferences.
                getInt("opened", 0);
    }

    public void setOpened(int value) {

        mSharedPreferences.edit().putInt("opened", value).apply();

    }
    public long getDashboard() {

        return mSharedPreferences.getLong("Dashboard", 0);
    }

    public String getUsername() {
        return mSharedPreferences.getString("Username",
                "developer");
    }

    public String getPassword() {
        return mSharedPreferences.getString("Password",
                "dQwertyOdm@MaaSai@1234#2019ss");
    }

    public long getAssociationId() {

        return mSharedPreferences.getLong("Association", 0);
    }
    public void setFcmId(String value) {
        mSharedPreferences.edit().putString("Fcm", value).apply();

    }
    public String getHostName()
    {
        return mSharedPreferences.getString("HostName",
                "http://40.114.87.117");
    }

    public String getTomcatPort()
    {
        return mSharedPreferences.getString("TomcatPort",
                ":8080");
    }


    public void clearPrefs()
    {
        mSharedPreferences.edit().clear().apply();
    }

    public void setEmergencyNos(int value) {
        mSharedPreferences.edit().putInt("setEmergencyNos", value).apply();

    }

    public void setAlertTypes(int value) {
        mSharedPreferences.edit().putInt("AlertType", value).apply();

    }
    public int getAlertTypes() {

        return mSharedPreferences.getInt("AlertType", 0);
    }

    public long getRoleId() {

        return mSharedPreferences.getLong("Role", 0);
    }
    public void setRoleId(long value) {
        mSharedPreferences.edit().putLong("Role", value).apply();
    }

    public String getFcmId() {

        return mSharedPreferences.getString("Fcm", null);
    }

    public void setFcmIdChanged(String value) {
        mSharedPreferences.edit().putString("FcmChanged", value).apply();

    }

    public String getFcmIdChanged() {

        return mSharedPreferences.getString("FcmChanged", null);
    }

    public void setFlatId(String value) {
        mSharedPreferences.edit().putString("Flat", value).apply();
    }

    public long getFlatId() {

        return Long.parseLong(mSharedPreferences.getString("Flat", null));
    }

    public void setAssociationId(long value) {
        mSharedPreferences.edit().putLong("Association", value).apply();

    }

    public void setAssociationName(String value) {
        mSharedPreferences.edit().putString("AssociationName", value).apply();

    }
    public String getAssociationName() {

        return mSharedPreferences.getString("AssociationName", null);
    }


    public String getFcmRegId() {
        return mSharedPreferences.getString("fcm_id", null);
    }

    public long getLoginId() {
        return mSharedPreferences.getLong("UserId",
                0);
    }

    public void setLoginId(long value) {
        mSharedPreferences.edit().putLong("UserId", value).apply();
    }

    public long getAccountLoginId() {
        return mSharedPreferences.getLong("LoginId",
                1);
    }

    public void setAccountLoginId(long value) {
        mSharedPreferences.edit().putLong("LoginId", value).apply();
    }

    public long getActive() {
        return mSharedPreferences.getLong("Active",
                0);
    }

    public void setActive(long value) {
        mSharedPreferences.edit().putLong("Active", value).apply();
    }

    public String getLoginName() {
        return mSharedPreferences.getString("LoginName",
                null);
    }
    public void setLoginName(String value) {
        mSharedPreferences.edit().putString("LoginName", value).apply();
    }

    public void setAssociationLoginId(long value) {
        mSharedPreferences.edit().putLong("AssociationLoginId", value).apply();
    }
    public long getAssociationLoginId() {
        return mSharedPreferences.getLong("AssociationLoginId",
                0);
    }

    public long getAssociationAccessId() {
        return mSharedPreferences.getLong("AssociationAccessId",
                0);
    }

    public void setAssociationAccessId(long value) {
        mSharedPreferences.edit().putLong("AssociationAccessId", value).apply();

    }

    public long getAccessId() {
        return mSharedPreferences.getLong("AccessId",
                0);
    }

    public void setAccessId(long value) {
        mSharedPreferences.edit().putLong("AccessId", value).apply();

    }

    public int getSmsTestRecievedCode() {
        return mSharedPreferences.
                getInt("sms_test_received_code", 0);
    }

    public void setSmsTestRecievedCode(int value) {

        mSharedPreferences.edit().putInt("sms_test_received_code", value).apply();
    }

    public int getSmsTestSavedCode() {
        return mSharedPreferences.
                getInt("sms_test_saved_code", 0);
    }

    public void setSmsTestSavedCode(int value) {
        mSharedPreferences.
                edit().putInt("sms_test_saved_code", value).apply();

    }

    public void removeSmsTestSavedCode() {
        mSharedPreferences.
                edit().remove("sms_test_saved_code").apply();

    }

    public void removeSmsTestRecievedCode() {
        mSharedPreferences.
                edit().remove("sms_test_received_code").apply();
    }

    public void logout() {

        mSharedPreferences.edit().clear().apply();
    }

    public String getTP()
    {
        return mSharedPreferences.getString("TomcatPort",
                ":8080");
    }
    public String getHN()
    {
        return mSharedPreferences.getString("HostName",
                "http://40.114.87.117");
    }
    public String getWebServicePath()
    {
        return mSharedPreferences.getString("Path",
                getHN()+getTP()+"/superguard-rest-api-sql/");
    }

    public String getCV()
    {
        return mSharedPreferences.getString("VC",
                getWebServicePath()+"accesses/verifyCredentials");
    }
    public String flatList()
    {
        return mSharedPreferences.getString("flatList",
                getWebServicePath()+"flats/findByBlockId");
    }
    public String companyByType()
    {
        return mSharedPreferences.getString("companyByType",
                getWebServicePath()+"company/getByType");
    }

    public String getSendOtp()
    {
        return mSharedPreferences.getString("SOTP",
                getWebServicePath()+"users/sendOtp");
    }

    public String getLogin() {
        return mSharedPreferences.getString("GetLogin",
                getWebServicePath()+"logins/accessAppLogin");
    }

    public String getAddSchool()
    {
        return mSharedPreferences.getString("addSchoolAndBus",
                getWebServicePath()+"association/schools/addSchoolAndSchoolBusAndMapToAssociation");
    }
    public String getPostLogout()
    {
        return mSharedPreferences.getString("logout",
                getWebServicePath()+"logins/logout");
    }
    public String getUpdateStaff()
    {
        return mSharedPreferences.getString("updateStaffAcc",
                getWebServicePath()+"users/updateStaffAccount");
    }

    public String getGuardsByAssociationId()
    {
        return mSharedPreferences.getString("getGuards",
                getWebServicePath()+"association/staff/getGuardsByAssociationId");
    }
//
    public String getForgotPassword()
    {
        return mSharedPreferences.getString("forgotPassword",
                getWebServicePath()+"accesses/forgotPassword");
    }
//
    public String getGuardLogin()
    {
        return mSharedPreferences.getString("guardLogin",
                getWebServicePath()+"logins/guardLogin");
    }
//
    public String getLstList()
    {
        return mSharedPreferences.getString("lstList",
                getWebServicePath()+"lst/findByAssociationId");
    }
    public String getAlertsCreate()
    {
        return mSharedPreferences.getString("alertsCreate",
                getWebServicePath()+"alerts/create");
    }
//
    public String getCitiesActive()
    {
        return mSharedPreferences.getString("citiesActive",
                getWebServicePath()+"cities/getByActive");
    }


    public String familyNeedApprovalKidsUserFlatId()
    {
        return mSharedPreferences.getString("familyNeedApprovalKidsUserFlatId",
                getWebServicePath()+"family/findNeedApprovalKidsByUserFlatId");
    }

    public String getAdmAssFindById()
    {
        return mSharedPreferences.getString("AdmAssFindById",
                getWebServicePath()+"admin/associations/findByAdminIdAndCityId");
    }
//
//    public String getfindAlertsByAssId()
//    {
//        return mSharedPreferences.getString("findAlertsByAssId",
//                getWebServicePath()+"alerts/findAlertsByAssociationIdAndDate"
//        );
//    }
//
//    public String getPrimaryAssociation()
//    {
//        return mSharedPreferences.getString("setPrimaryAssociation",
//                getWebServicePath()+"admin/associations/setPrimaryAssociation"
//        );
//
//    }
    public String getFindBlocks()
    {
        return mSharedPreferences.getString("findBlocks",
                getWebServicePath()+"blocks/findByAssociationId");

    }

    public String getCreateNotices() {
        return mSharedPreferences.getString("createNotices",
                getWebServicePath() + "notices/create");
    }

    public String getNoticesByAssId()
    {
        return mSharedPreferences.getString("NoticesByAssId",
                getWebServicePath()+"notices/findByAssociationId");
    }

    public String getUpdateApproval()
    {
        return mSharedPreferences.getString("UPAPPR",
                getWebServicePath()+ "user/flats/updateApproval");
    }

    public String getAPRList()
    {
        return mSharedPreferences.getString("APRList",
                getWebServicePath()+"admin/associations/findApprovalsByAssociationIdAndApprovalStatus");
    }


    public String getNoticesListByAssId()
    {
        return mSharedPreferences.getString("NoticesListByAssId",
                getWebServicePath()+"notices/getListByAssociationId");

    }
//
//    public String getAssociations()
//    {
//        return mSharedPreferences.getString("getAss",
//                getWebServicePath()+"associations/findByAdminId"
//        );
//
//    }
    public String getVehicles()
    {
        return mSharedPreferences.getString("getVeh",
                getWebServicePath()+"attendance/getVehiclesWithAttendance");

    }
//
    public String getCreateBlock()
    {
        return mSharedPreferences.getString("createBlock",
                getWebServicePath()+"blocks/create");

    }
    public String getCreateFlat()
    {
        return mSharedPreferences.getString("createFlat",
                getWebServicePath()+"flats/create");

    }
    public String getCreateAttendances()
    {
        return mSharedPreferences.getString("createAttendances",
                getWebServicePath()+"attendance/createList");

    }
    public String getEditBlock()
    {
        return mSharedPreferences.getString("editBlock",
                getWebServicePath()+"blocks/edit"
        );

    }
//    public String getDeleteBlock()
//    {
//        return mSharedPreferences.getString("deleteBlock",
//                getWebServicePath()+"blocks/delete"
//        );
//
//    }
//
//    public String getCreateAssociations()
//    {
//        return mSharedPreferences.getString("createAssociations",
//                getWebServicePath()+"associations/create"
//        );
//
//    }
//    public String getFindFlots()
//    {
//        return mSharedPreferences.getString("findFlots",
//                getWebServicePath()+"flats/findByBlockId"
//        );
//
//    }
    public String getFindBlockFloor()
    {
        return mSharedPreferences.getString("findBlockFloor",
                getWebServicePath()+"flats/findByBlockIdAndFloorId");

    }
    public String getFindActiveFloors()
    {
        return mSharedPreferences.getString("findActiveFloors",
                getWebServicePath()+"floors/getByBlockId");

    }

    public String getFindResidents()
    {
        return mSharedPreferences.getString("findResidents",
                getWebServicePath()+"user/flats/findResidentsByFlatId");

    }

    public String getAddStaff()
    {
        return mSharedPreferences.getString("addStaff",
                getWebServicePath()+"users/addStaff");

    }

    public String getGuard()
    {
        return mSharedPreferences.getString("getGuard",
                getWebServicePath()+"users/findGuardsByAssociationId");

    }
    public String getCreateEmergency()
    {
        return mSharedPreferences.getString("createEmergency",
                getWebServicePath()+"emergency/numbers/create");

    }
    public String getEmergencyList()
    {
        return mSharedPreferences.getString("emergencyList",
                getWebServicePath()+"emergency/numbers/findByAssociationId");

    }
    public String getComplaintTypeAndCategories()
    {
        return mSharedPreferences.getString("complaintTypeCat",
                getWebServicePath()+"complaint/categories/getTypesAndCategories");

    }
    public String getCreateComplaints()
    {
        return mSharedPreferences.getString("createComplaints",
                getWebServicePath()+"complaints/create");

    }
    public String getSearchKidEntry()
    {
        return mSharedPreferences.getString("searchKidEntry",
                getWebServicePath()+"entry/kid/searchKidEntriesByAssociationIdAndName");

    }
    public String getLsuProfileDetails()
    {
        return mSharedPreferences.getString("lsuProfiledetails",
                getWebServicePath()+"lsu/flats/getLsuProfileDetailsByAndAssociationIdAndName");

    }

    public String getComplaintsList()
        {
        return mSharedPreferences.getString("complaintsList",
                getWebServicePath()+"complaints/findByAssociationId");

    }
    public String getInsideLocalServicesList()
    {
        return mSharedPreferences.getString("insideLocalServicesList",
                getWebServicePath()+"lsu/admin/findByAssociationIdAndLocalServiceTypeId");

    }

    public String getCreateLSU()
    {
        return mSharedPreferences.getString("createLSU",
                getWebServicePath()+"lsu/create");
    }
    public String getCreateUsers()
    {
        return mSharedPreferences.getString("createUsers",
                getWebServicePath()+"users/create");

    }
    public String getUserChangePassword()
    {
        return mSharedPreferences.getString("userChangePassword",
                getWebServicePath()+"users/changePassword");

    }
    public String getAdminDetails()
    {
        return mSharedPreferences.getString("adminDetails",
                getWebServicePath()+"users/findAccountById");

    }
    public String getAccessChangePassword()
    {
        return mSharedPreferences.getString("accessChangePassword",
                getWebServicePath()+"association/accesses/changePassword");

    }
    public String getGuardLogout()
    {
        return mSharedPreferences.getString("logout",
                getWebServicePath()+"logins/logout");

    }
//
//    public String getEntryExitType()
//    {
//        return mSharedPreferences.getString("entryExitType",
//                getWebServicePath()+"entry/exit/types/getByActive"
//        );
//
//    }
    public String getSearchVehicleNumbers()
    {
        return mSharedPreferences.getString("searchVehNo",
                getWebServicePath()+"vehicles/searchVehicleByNumber");

    }
    public String getCreateDelivery()
    {
        return mSharedPreferences.getString("createDelivery",
                getWebServicePath()+"entry/delivery/create");

    }
    public String getCreateEntryExitLog()
    {
        return mSharedPreferences.getString("createEntryExitLog",
                getWebServicePath()+"entry/exit/log/create");

    }

    public String getCall()
    {
        return mSharedPreferences.getString("createCall",
                getWebServicePath()+"api/v1/eintercom/call/create");

    }

//    public String getCreateVehicle()
//    {
//        return mSharedPreferences.getString("createVehicle",
//                getWebServicePath()+"vehicles/create"
//        );
//
//    }

    public String getManualVehicleList()
    {
        return mSharedPreferences.getString("manualVehicleList",
                getWebServicePath()+"entry/manual/vehicle/findByAssociationId"
        );

    }
public String getManualKidList()
{
    return mSharedPreferences.getString("manualKidList",
            getWebServicePath()+"entry/manual/kid/findByAssociationId"
    );

}
    public String getManualList()
    {
        return mSharedPreferences.getString("manualList",
                getWebServicePath()+"entry/manual/findByAssociationIdAndType"
        );

    }
    public String getSearchCabNumber()
    {
        return mSharedPreferences.getString("searchCabNumber",
                getWebServicePath()+"entry/cab/searchByNumber");

    }
    public String getDeliveryListByNumber()
    {
        return mSharedPreferences.getString("searchDeliveryListByNumber",
                getWebServicePath()+"entry/delivery/searchByNumber");

    }
    public String getGatePackageList()
    {
        return mSharedPreferences.getString("getGatePackageList",
                getWebServicePath()+"entry/delivery/getGatePackageList");

    }

    public String getStaffList()
    {
        return mSharedPreferences.getString("staffList",
                getWebServicePath()+"association/staff/getDetailsWithAttendanceByAssociationIdAndDate");

    }
//    public String getFindAssociationById()
//    {
//        return mSharedPreferences.getString("associationById",
//                getWebServicePath()+"associations/findById"
//        );
//
//    }
    public String getSchoolBusList()
    {
        return mSharedPreferences.getString("schoolBusList",
                getWebServicePath()+"family/getSchoolBusListByAssociationId");

    }
//    public String getSearchLSUName()
//    {
//        return mSharedPreferences.getString("searchLSUName",
//                getWebServicePath()+"lsu/searchByName"
//        );
//
//    }
    public String getComplaintResolve()
    {
        return mSharedPreferences.getString("complaintResolve",
                getWebServicePath()+"complaints/updateAsprogress");

    }
//    public String getAlertTypesList()
//    {
//        return mSharedPreferences.getString("alertTypesList",
//                getWebServicePath()+"alert/types/getByActive"
//        );
//
//    }


    public String getStaffListByAssociationId()
    {
        return mSharedPreferences.getString("StaffListByAssociationId",
                getWebServicePath()+"association/staff/getDetailsWithAttendanceByAssociationIdAndDate");

    }

    public String getStaffListByAssociation()
    {
        return mSharedPreferences.getString("StaffListByAssociation",
                getWebServicePath()+"association/staff/getStaffListByAssociationId");

    }
//
    public String getAdminLogin()
    {
        return mSharedPreferences.getString("adminLogin",
                getWebServicePath()+"accesses/admin/login");

    }
    public String getByRoleName()
    {
        return mSharedPreferences.getString("roleNameAndMobile",
                getWebServicePath()+"users/getByRoleNameAndMobile");

    }
//
    public String getDesignations()
    {
        return mSharedPreferences.getString("designations",
                getWebServicePath()+"designations/getAll");

    }
//
    public String getCreateStaff()
    {
        return mSharedPreferences.getString("createStaff",
                getWebServicePath()+"association/staff/create");
    }
//
    public String getLSUList()
    {
        return mSharedPreferences.getString("localServiceUserList",
                getWebServicePath()+"lsu/type/getByAssociationIdAndLocalServiceTypeId");
    }
//
    public String getAddLSU()
    {
        return mSharedPreferences.getString("addLSU",
                getWebServicePath()+"users/addLocalServiceUser");
    }
    public String getCreateItem()
    {
        return mSharedPreferences.getString("createItem",
                getWebServicePath()+"given/items/getByAssociationIdAndLocalServiceUserTypeId");
    }
    public String getCreateLSUTypes()
    {
        return mSharedPreferences.getString("createLSU",
                getWebServicePath()+"lsu/type/create");

    }
    public String getCreateManualEntry()
    {
        return mSharedPreferences.getString("createManualEntry",
                getWebServicePath()+"entry/manual/create");

    }
    public String getSearchGuestEntry()
    {
        return mSharedPreferences.getString("guestSearch",
                getWebServicePath()+"entry/guest/searchGuest");

    }
    public String getAlerts()
    {
        return mSharedPreferences.getString("getAlerts",
                getWebServicePath()+"alerts/getAlertsByAssociationId");

    }
    public String getComplaintsByAssociationId()
    {
        return mSharedPreferences.getString("complaints",
                getWebServicePath()+"complaints/getByAssociationId");

    }
    public String getReviewsRating()
    {
        return mSharedPreferences.getString("reviewsRating",
                getWebServicePath()+"lsu/type/getRatingAndReviewByLocalServiceUserTypeId");

    }
    public String getLSUProfileDetails()
    {
        return mSharedPreferences.getString("LSUProfileDetails",
                getWebServicePath()+"lsu/type/findById");

    }
    public String getMonthAttendance()
    {
        return mSharedPreferences.getString("monthAttendance",
                getWebServicePath()+"entry/exit/log/getMonthlyAttendanceByTypeAndId");

    }
    public String getDateWiseEntry()
    {
        return mSharedPreferences.getString("dateWiseEntry",
                getWebServicePath()+"entry/exit/log/getLogByEntryExitIdAndEntryExitTypeAndDate");

    }
//
    public String getLSUEntryLog()
    {
        return mSharedPreferences.getString("LSUEntryLog",
                getWebServicePath()+"entry/exit/log/getDayAttendanceByLocalServiceUserTypeIdAndYear");
    }
//    public String getDeleteFromAssociation()
//    {
//        return mSharedPreferences.getString("removeFromAss",
//                getWebServicePath()+"association/staff/deleteFromAssociationById");
//
//    }
//
    public String getStaffDetails()
    {
        return mSharedPreferences.getString("staffDetails",
                getWebServicePath()+"association/staff/findById");

    }
    public String getEditLSUProfileDetails()
    {
        return mSharedPreferences.getString("editLSUProfileDetails",
                getWebServicePath()+"users/updateLsuAccount");

    }
    public String getUpdateSlotEmpty()
    {
        return mSharedPreferences.getString("updateSlotEmpty",
                getWebServicePath()+"lsu/type/updateSlotEmpty");

    }
    public String getSchools()
    {
        return mSharedPreferences.getString("schoolsList",
                getWebServicePath()+"schools/getAll");

    }
    public String getOTP()
    {
        return mSharedPreferences.getString("SOTP",
                getWebServicePath()+"users/sendOtp");
    }

    public String getCommentList(){

        return mSharedPreferences.getString("getCommentList",
                getWebServicePath()+"help/desk/comment/findByComplaintId");
    }


    public String postComments(){


        return mSharedPreferences.getString("postComments",
                getWebServicePath()+"help/desk/comment/create");
    }

    public String userGetRoleNameMobile()
    {
        return mSharedPreferences.getString("userGetRoleNameMobile",
                getWebServicePath()+"users/getByRoleNameAndMobile");
    }

    public String accessForgotPassword()
    {
        return mSharedPreferences.getString("accessForgotPassword",
                getWebServicePath()+"accesses/forgotPassword");
    }

    public String getAddKid()
    {
        return mSharedPreferences.getString("addKid",
                getWebServicePath()+"entry/manual/kid/create");
    }

    public String getAddVehicle()
    {
        return mSharedPreferences.getString("addVehicle",
                getWebServicePath()+"entry/manual/vehicle/create");
    }


    public String entryCabCreate()
    {
        return mSharedPreferences.getString("entryCabCreate",
                getWebServicePath()+"entry/cab/create");
    }


    public String entryDeliveryCreate()
    {
        return mSharedPreferences.getString("entryDeliveryCreate",
                getWebServicePath()+"entry/delivery/create");
    }

    public String entryGuestCreate()
    {
        return mSharedPreferences.getString("entryGuestCreate",
                getWebServicePath()+"entry/guest/create");
    }


    public String emergencyCategory(){

        return mSharedPreferences.getString("emergencyCategory",
        getWebServicePath()+"emergency/categories/getAll");
    }

    public String givenItems(){

        return mSharedPreferences.getString("givenItems",
                getWebServicePath()+"given/items/getByLocalServiceUserTypeIdAndDate");
    }


    public String mngmntDesignationList(){

        return mSharedPreferences.getString("mngmntDesignationList",
                getWebServicePath()+"management/type/findByAssociationId");
    }

    public String createMangmnt(){

        return mSharedPreferences.getString("createMangmnt",
                getWebServicePath()+"management/create");
    }



}


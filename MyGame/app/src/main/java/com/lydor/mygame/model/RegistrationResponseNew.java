package com.lydor.mygame.model;

import com.google.gson.annotations.SerializedName;

public class RegistrationResponseNew {

    @SerializedName("meassage")
    private String messsage;


    public String getMesssage() {
        return messsage;
    }

    public void setMesssage(String messsage) {
        this.messsage = messsage;
    }

    public RegistrationResponseNew() {
    }

    public RegistrationResponseNew(String messsage) {
        this.messsage = messsage;
    }
}

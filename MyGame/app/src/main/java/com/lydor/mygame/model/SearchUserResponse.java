package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SearchUserResponse implements Parcelable {

    private int user_id;
    private String name;
    private int total_followers;
    private int total_following;
    private String user_tag;
    private String user_image;
    private int ifollow;

    public SearchUserResponse() {
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal_followers() {
        return total_followers;
    }

    public void setTotal_followers(int total_followers) {
        this.total_followers = total_followers;
    }

    public int getTotal_following() {
        return total_following;
    }

    public void setTotal_following(int total_following) {
        this.total_following = total_following;
    }

    public String getUser_tag() {
        return user_tag;
    }

    public void setUser_tag(String user_tag) {
        this.user_tag = user_tag;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public int getIfollow() {
        return ifollow;
    }

    public void setIfollow(int ifollow) {
        this.ifollow = ifollow;
    }

    public static Creator<SearchUserResponse> getCREATOR() {
        return CREATOR;
    }

    protected SearchUserResponse(Parcel in) {
        user_id = in.readInt();
        name = in.readString();
        total_followers = in.readInt();
        total_following = in.readInt();
        user_tag = in.readString();
        user_image = in.readString();
        ifollow = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(user_id);
        dest.writeString(name);
        dest.writeInt(total_followers);
        dest.writeInt(total_following);
        dest.writeString(user_tag);
        dest.writeString(user_image);
        dest.writeInt(ifollow);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchUserResponse> CREATOR = new Creator<SearchUserResponse>() {
        @Override
        public SearchUserResponse createFromParcel(Parcel in) {
            return new SearchUserResponse(in);
        }

        @Override
        public SearchUserResponse[] newArray(int size) {
            return new SearchUserResponse[size];
        }
    };
}

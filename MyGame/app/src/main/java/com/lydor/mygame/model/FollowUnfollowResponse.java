package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FollowUnfollowResponse implements Parcelable {
    private String status;
    private String message;

    public FollowUnfollowResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static Creator<FollowUnfollowResponse> getCREATOR() {
        return CREATOR;
    }

    protected FollowUnfollowResponse(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FollowUnfollowResponse> CREATOR = new Creator<FollowUnfollowResponse>() {
        @Override
        public FollowUnfollowResponse createFromParcel(Parcel in) {
            return new FollowUnfollowResponse(in);
        }

        @Override
        public FollowUnfollowResponse[] newArray(int size) {
            return new FollowUnfollowResponse[size];
        }
    };
}

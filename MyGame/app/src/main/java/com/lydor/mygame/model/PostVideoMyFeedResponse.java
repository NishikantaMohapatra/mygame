package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PostVideoMyFeedResponse implements Parcelable {

    private String status;

    public PostVideoMyFeedResponse() {
    }

    protected PostVideoMyFeedResponse(Parcel in) {
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PostVideoMyFeedResponse> CREATOR = new Creator<PostVideoMyFeedResponse>() {
        @Override
        public PostVideoMyFeedResponse createFromParcel(Parcel in) {
            return new PostVideoMyFeedResponse(in);
        }

        @Override
        public PostVideoMyFeedResponse[] newArray(int size) {
            return new PostVideoMyFeedResponse[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

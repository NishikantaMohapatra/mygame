package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activities.AboutUsActivity;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.PostVideoMyFeedResponse;
import com.lydor.mygame.model.UpdateUserDetails;
import com.lydor.mygame.model.UserMyProfileResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.BitmapUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.PickerBuilder;
import com.lydor.mygame.utils.Profile_utility;
import com.lydor.mygame.utils.StringUtils;
import com.lydor.mygame.utils.Utility;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.lydor.mygame.utils.Profile_utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

public class EditProfileActivity extends BaseActivity {

    ImageView imvBack,imv;
    ArrayList<UserMyProfileResponse> userObj;
    EditText etName,etUserName,etMobile,etBio,etEmail;
    TextView tvSaveBtn;

    private String path;
    private Uri newPath;

    private File photofile;
    private File compressedImageFile = null;

    private int height;
    private int width;

    private String userChoosenTask;

    String userName = null;
    String userTag= null;
    String mobile = null;
    String userBio = null;
    String email = null;
    String userImage = null;
    int iFollow = 0;
    SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        imvBack = findViewById(R.id.imvBack);
        etName = findViewById(R.id.etName);
        etUserName = findViewById(R.id.etUserName);
        etMobile = findViewById(R.id.etMobile);
        etBio = findViewById(R.id.etBio);
        etEmail = findViewById(R.id.etEmail);
        tvSaveBtn = findViewById(R.id.tvSaveBtn);
        imv = findViewById(R.id.cimv);

        pref = new SharedPref(this);

        etMobile.setEnabled(false);
        etEmail.setEnabled(false);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        EditProfileActivity.this.getWindowManager().
                getDefaultDisplay().
                getMetrics(displayMetrics);
        height = displayMetrics.heightPixels/2;
        width = displayMetrics.widthPixels/2;

        if(getIntent().getParcelableArrayListExtra("obj")!=null){
            userObj = getIntent().getParcelableArrayListExtra("obj");
        }

        if(userObj.size()>0){

            for (int i = 0;i<userObj.size();i++){
                userName = userObj.get(i).getName();
                userTag = userObj.get(i).getUser_tag();
                mobile = userObj.get(i).getMobile();
                userBio = userObj.get(i).getUser_bio();
                email = userObj.get(i).getEmail();
                userImage = userObj.get(i).getUser_image();
            }
        }

        if(userName!=null){
            etName.setText(userName);
        }else{
            etName.setText("");
        }

        if(userTag!=null){
            etUserName.setText(userTag);
        }else{
            etUserName.setText("");
        }

        if(mobile!=null){
            etMobile.setText(mobile);
        }else{
            etMobile.setText("");
        }

        if(userBio!=null){
            etBio.setText(userBio);
        }else{
            etBio.setText("");
        }

        if(email!=null){
            etEmail.setText(email);
        }else{
            etEmail.setText("");
        }


        if(userImage!=null) {
            //holder.imv.setTag(liveStreamLists.get(position).getImage());
            Picasso.with(EditProfileActivity.this).load("http://mygame-app.s3.ap-south-1.amazonaws.com/"+userImage)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap,
                                                   Picasso.LoadedFrom from) {

                            imv.setImageBitmap(bitmap);

                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            imv.setImageDrawable(
                                    getResources().
                                            getDrawable(R.drawable.ic_user));

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        }
        else
        {
            imv.setImageDrawable(
                    getResources().
                            getDrawable(R.drawable.ic_user)
            );

        }


        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission(EditProfileActivity.this);
            }
        });

        tvSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etName.getText().toString().isEmpty()){
                    Toast.makeText(EditProfileActivity.this, ""+ Constants.ENTER_YOUR_NAME, Toast.LENGTH_SHORT).show();
                }else if(userTag!=null){
                    if(etUserName.getText().toString().isEmpty()){
                        updateUserDetails();
                    }else if(etUserName.getText().toString().length()<8 || etUserName.getText().toString().length()>25){
                            showToast("User name must contain at least eight to twenty five characters");
                    }
                    else if(etUserName.getText().toString().length()>=8){
                        if(etUserName.getText().toString().equalsIgnoreCase(userTag)){
                            updateUserDetailsWithoutUserName();
                        }else{

                            updateUserDetails();
                        }
                    } else{
                        updateUserDetails();
                    }

                }else{
                    if(etUserName.getText().toString().isEmpty()){
                        updateUserDetails();
                    }else if(etUserName.getText().toString().length()<8){
                        showToast("User name must contain at least eight characters");
                    }else{
                        updateUserDetails();
                    }

                }
            }
        });
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public  boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.
                    WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.
                            CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                        context, Manifest.permission.WRITE_EXTERNAL_STORAGE )
                        || ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                        context, Manifest.permission.CAMERA )) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");

                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(
                                    (Activity) context, new String[]{
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                selectImage();
                return true;
            }
        } else {
            selectImage();
            return true;
        }
    }


    private String base64;

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library"};

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setTitle(Html.fromHtml("<font color='#05ab9a'>Add Photo</font>"));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Profile_utility.checkPermission(EditProfileActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result) {
                        new PickerBuilder(EditProfileActivity.this, PickerBuilder.SELECT_FROM_CAMERA)
                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                    @Override
                                    public void onImageReceived(Uri imageUri) {
                                        try {
                                            path = imageUri.getPath();
                                            newPath = null;
                                            newPath = imageUri;
                                            photofile = null;
                                            photofile = new File(new URI(newPath.toString()));
                                            compressedImageFile = null;
                                            compressedImageFile = BitmapUtil.compressImage(
                                                    EditProfileActivity.this,
                                                    photofile,
                                                    height,
                                                    width);

                                            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(EditProfileActivity.this,
                                                    imageUri);
                                            base64= StringUtils.bitmapToBase64(bitmap);
                                            imv.setBackgroundResource(0);
                                            imv.setImageBitmap(bitmap);
                                            //tvImageName.setText(base64);
                                            //psnlChangeI.setText("EDIT");


                                        } catch (Exception e) {
                                            Toast.makeText(EditProfileActivity.this, "Please insert photo again",
                                                    Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                })
                                .withTimeStamp(false)
                                .setCropScreenColor(Color.GREEN)
                                .start();
                    }
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {
                        new PickerBuilder(EditProfileActivity.this, PickerBuilder.SELECT_FROM_GALLERY)
                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                    @Override
                                    public void onImageReceived(Uri imageUri) {
                                        try {

                                            path = imageUri.getPath();
                                            newPath = null;
                                            newPath = imageUri;
                                            photofile = null;
                                            photofile = new File(new URI(newPath.toString()));
                                            compressedImageFile = null;
                                            compressedImageFile = BitmapUtil.compressImage(
                                                    EditProfileActivity.this,
                                                    photofile,
                                                    height,
                                                    width);
                                            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(EditProfileActivity.this, imageUri);
                                            base64= StringUtils.bitmapToBase64(bitmap);
                                            imv.setBackgroundResource(0);
                                            imv.setImageBitmap(bitmap);
                                            //tvImageName.setText(base64);
                                            //binding.psnlChangeI.setText("EDIT");
                                        } catch (Exception e) {
                                            Toast.makeText(EditProfileActivity.this, "Please insert photo again", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setCropScreenColor(Color.GREEN)
                                .setOnPermissionRefusedListener(new PickerBuilder.onPermissionRefusedListener() {
                                    @Override
                                    public void onPermissionRefused() {
                                        Toast.makeText(EditProfileActivity.this, "This permision is necessary", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .start();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }


    private void updateUserDetails(){
        if (AppUtil.isInternetConnected(EditProfileActivity.this)) {
            enableDialog("Please wait...");



            MultipartBody.Part vFile;
            if(compressedImageFile!=null){
                RequestBody photoBody = RequestBody.create(MediaType.parse("image/*"), compressedImageFile);
                vFile = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), photoBody);
            }else{
                RequestBody photoBody = RequestBody.create(MediaType.parse("image/*"), "");
                vFile = MultipartBody.Part.createFormData("image", "");
            }


            RequestBody name =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(etName.getText().toString()));


            RequestBody userName =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(etUserName.getText().toString()));

            RequestBody userBio =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(etBio.getText().toString()));


            RestService apiService =
                    ApiClient.getClient(this).create(RestService.class);

            Call<UpdateUserDetails> call = apiService.updateUserProfile(vFile,name,userName,userBio);
            call.enqueue(new Callback<UpdateUserDetails>() {
                @Override
                public void onResponse(Call<UpdateUserDetails> call, Response<UpdateUserDetails> response) {


                    disableDialog();



                    if(response.code()==201){
                        UpdateUserDetails updateUserDetails = response.body();
                        if(updateUserDetails.getStatus().equalsIgnoreCase("ok")){
                            Toast.makeText(EditProfileActivity.this, "User details updated successfully", Toast.LENGTH_SHORT).show();
                            pref.putString(Constants.USER_NAME,etName.getText().toString());
                            finish();
                        }else{
                            Toast.makeText(EditProfileActivity.this, "" + updateUserDetails.getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    }else {


                        if (response != null && response.errorBody() != null) {
                            //LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(EditProfileActivity.this, Constants.SOME_THING_WRONG, Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }


                }

                @Override
                public void onFailure(Call<UpdateUserDetails> call, Throwable t) {

                    disableDialog();
                    showToast(t.toString());
                }
            });




        } else {
            disableDialog();
            Toast.makeText(EditProfileActivity.this,
                    "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }


    private void updateUserDetailsWithoutUserName(){
        if (AppUtil.isInternetConnected(EditProfileActivity.this)) {
            enableDialog("Please wait...");



            MultipartBody.Part vFile;
            if(compressedImageFile!=null){
                RequestBody photoBody = RequestBody.create(MediaType.parse("image/*"), compressedImageFile);
                vFile = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), photoBody);
            }else{
                RequestBody photoBody = RequestBody.create(MediaType.parse("image/*"), "");
                vFile = MultipartBody.Part.createFormData("image", "");
            }


            RequestBody name =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(etName.getText().toString()));




            RequestBody userBio =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(etBio.getText().toString()));


            RestService apiService =
                    ApiClient.getClient(this).create(RestService.class);

            Call<UpdateUserDetails> call = apiService.updateUserProfileWithoutUserName(vFile,name,userBio);
            call.enqueue(new Callback<UpdateUserDetails>() {
                @Override
                public void onResponse(Call<UpdateUserDetails> call, Response<UpdateUserDetails> response) {


                    disableDialog();



                    if(response.code()==201){
                        UpdateUserDetails updateUserDetails = response.body();
                        if(updateUserDetails.getStatus().equalsIgnoreCase("ok")){
                            Toast.makeText(EditProfileActivity.this, "User details updated successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Toast.makeText(EditProfileActivity.this, "" + updateUserDetails.getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    }else {


                        if (response != null && response.errorBody() != null) {
                            //LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(EditProfileActivity.this, Constants.SOME_THING_WRONG, Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }


                }

                @Override
                public void onFailure(Call<UpdateUserDetails> call, Throwable t) {

                    disableDialog();
                    showToast(t.toString());
                }
            });




        } else {
            disableDialog();
            Toast.makeText(EditProfileActivity.this,
                    "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }
}

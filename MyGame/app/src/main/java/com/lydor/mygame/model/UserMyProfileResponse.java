package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserMyProfileResponse implements Parcelable {

    private int id;
    private String name;
    private String email;
    private String mobile;
    private String user_tag;
    private String user_image;
    private String location;
    private int notification_status;
    private String user_bio;
    private int total_posts;
    private String last_login;
    private int user_role_id;
    private int total_followers;
    private int total_following;
    private int is_active;
    private String created_at;
    private String updated_at;
    private String deleted_at;

    public UserMyProfileResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUser_tag() {
        return user_tag;
    }

    public void setUser_tag(String user_tag) {
        this.user_tag = user_tag;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getNotification_status() {
        return notification_status;
    }

    public void setNotification_status(int notification_status) {
        this.notification_status = notification_status;
    }

    public String getUser_bio() {
        return user_bio;
    }

    public void setUser_bio(String user_bio) {
        this.user_bio = user_bio;
    }

    public int getTotal_posts() {
        return total_posts;
    }

    public void setTotal_posts(int total_posts) {
        this.total_posts = total_posts;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public int getUser_role_id() {
        return user_role_id;
    }

    public void setUser_role_id(int user_role_id) {
        this.user_role_id = user_role_id;
    }

    public int getTotal_followers() {
        return total_followers;
    }

    public void setTotal_followers(int total_followers) {
        this.total_followers = total_followers;
    }

    public int getTotal_following() {
        return total_following;
    }

    public void setTotal_following(int total_following) {
        this.total_following = total_following;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public static Creator<UserMyProfileResponse> getCREATOR() {
        return CREATOR;
    }

    protected UserMyProfileResponse(Parcel in) {
        id = in.readInt();
        name = in.readString();
        email = in.readString();
        mobile = in.readString();
        user_tag = in.readString();
        user_image = in.readString();
        location = in.readString();
        notification_status = in.readInt();
        user_bio = in.readString();
        total_posts = in.readInt();
        last_login = in.readString();
        user_role_id = in.readInt();
        total_followers = in.readInt();
        total_following = in.readInt();
        is_active = in.readInt();
        created_at = in.readString();
        updated_at = in.readString();
        deleted_at = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(mobile);
        dest.writeString(user_tag);
        dest.writeString(user_image);
        dest.writeString(location);
        dest.writeInt(notification_status);
        dest.writeString(user_bio);
        dest.writeInt(total_posts);
        dest.writeString(last_login);
        dest.writeInt(user_role_id);
        dest.writeInt(total_followers);
        dest.writeInt(total_following);
        dest.writeInt(is_active);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(deleted_at);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserMyProfileResponse> CREATOR = new Creator<UserMyProfileResponse>() {
        @Override
        public UserMyProfileResponse createFromParcel(Parcel in) {
            return new UserMyProfileResponse(in);
        }

        @Override
        public UserMyProfileResponse[] newArray(int size) {
            return new UserMyProfileResponse[size];
        }
    };
}

package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class OtherUserDetailsRequest implements Parcelable {

    private String user_id;


    public OtherUserDetailsRequest() {
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public static Creator<OtherUserDetailsRequest> getCREATOR() {
        return CREATOR;
    }

    protected OtherUserDetailsRequest(Parcel in) {
        user_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OtherUserDetailsRequest> CREATOR = new Creator<OtherUserDetailsRequest>() {
        @Override
        public OtherUserDetailsRequest createFromParcel(Parcel in) {
            return new OtherUserDetailsRequest(in);
        }

        @Override
        public OtherUserDetailsRequest[] newArray(int size) {
            return new OtherUserDetailsRequest[size];
        }
    };
}

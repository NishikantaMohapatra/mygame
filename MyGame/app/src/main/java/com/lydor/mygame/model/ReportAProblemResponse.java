package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ReportAProblemResponse implements Parcelable {

    public String status;

    public static Creator<ReportAProblemResponse> getCREATOR() {
        return CREATOR;
    }

    public ReportAProblemResponse() {
    }

    protected ReportAProblemResponse(Parcel in) {
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReportAProblemResponse> CREATOR = new Creator<ReportAProblemResponse>() {
        @Override
        public ReportAProblemResponse createFromParcel(Parcel in) {
            return new ReportAProblemResponse(in);
        }

        @Override
        public ReportAProblemResponse[] newArray(int size) {
            return new ReportAProblemResponse[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

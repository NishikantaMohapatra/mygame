package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MyProfileMyChallengesListData implements Parcelable {

    private int id;
    private String title;
    private String description;
    private String date_start;
    private String date_end;
    private int user_id;
    private String video_id;
    private String created_at;
    private String updated_at;
    private String deleted_at;
    private String file_name;
    private String video_thumbnail;
    private String total_views;
    private String total_likes;

    public MyProfileMyChallengesListData() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }

    public String getTotal_views() {
        return total_views;
    }

    public void setTotal_views(String total_views) {
        this.total_views = total_views;
    }

    public String getTotal_likes() {
        return total_likes;
    }

    public void setTotal_likes(String total_likes) {
        this.total_likes = total_likes;
    }

    public static Creator<MyProfileMyChallengesListData> getCREATOR() {
        return CREATOR;
    }

    protected MyProfileMyChallengesListData(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        date_start = in.readString();
        date_end = in.readString();
        user_id = in.readInt();
        video_id = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        deleted_at = in.readString();
        file_name = in.readString();
        video_thumbnail = in.readString();
        total_views = in.readString();
        total_likes = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(date_start);
        dest.writeString(date_end);
        dest.writeInt(user_id);
        dest.writeString(video_id);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(deleted_at);
        dest.writeString(file_name);
        dest.writeString(video_thumbnail);
        dest.writeString(total_views);
        dest.writeString(total_likes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MyProfileMyChallengesListData> CREATOR = new Creator<MyProfileMyChallengesListData>() {
        @Override
        public MyProfileMyChallengesListData createFromParcel(Parcel in) {
            return new MyProfileMyChallengesListData(in);
        }

        @Override
        public MyProfileMyChallengesListData[] newArray(int size) {
            return new MyProfileMyChallengesListData[size];
        }
    };
}

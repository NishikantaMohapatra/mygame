package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FollowersResponse implements Parcelable {

    private String email;
    private String name;
    private int total_followers;
    private int total_following;
    private int user_id;
    private int follower_id;
    private String user_tag;
    private int ifollow;
    private String user_image;

    public FollowersResponse() {
    }

    protected FollowersResponse(Parcel in) {
        email = in.readString();
        name = in.readString();
        total_followers = in.readInt();
        total_following = in.readInt();
        user_id = in.readInt();
        follower_id = in.readInt();
        user_tag = in.readString();
        ifollow = in.readInt();
        user_image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(name);
        dest.writeInt(total_followers);
        dest.writeInt(total_following);
        dest.writeInt(user_id);
        dest.writeInt(follower_id);
        dest.writeString(user_tag);
        dest.writeInt(ifollow);
        dest.writeString(user_image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FollowersResponse> CREATOR = new Creator<FollowersResponse>() {
        @Override
        public FollowersResponse createFromParcel(Parcel in) {
            return new FollowersResponse(in);
        }

        @Override
        public FollowersResponse[] newArray(int size) {
            return new FollowersResponse[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal_followers() {
        return total_followers;
    }

    public void setTotal_followers(int total_followers) {
        this.total_followers = total_followers;
    }

    public int getTotal_following() {
        return total_following;
    }

    public void setTotal_following(int total_following) {
        this.total_following = total_following;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getFollower_id() {
        return follower_id;
    }

    public void setFollower_id(int follower_id) {
        this.follower_id = follower_id;
    }

    public String getUser_tag() {
        return user_tag;
    }

    public void setUser_tag(String user_tag) {
        this.user_tag = user_tag;
    }

    public int getIfollow() {
        return ifollow;
    }

    public void setIfollow(int ifollow) {
        this.ifollow = ifollow;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }
}

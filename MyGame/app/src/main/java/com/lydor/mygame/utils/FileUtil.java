package com.lydor.mygame.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Raju Satheesh on 11/17/2016.
 */
public class FileUtil {
    private static  final String TAG="FileUtil";

    public static final String ROOT="/Digichamps/";
    public static final String PROFILE_PICS=ROOT+"Digichamps ProfilePics/";
    public static final String CROP_PICS=ROOT+"CropPics/";
    public static final String PHOTO_PICS=ROOT+"PhotoPics/";

    public static final String DIGI_MEDIA="Digichamps Media/";
    public static final String CHAT_AUDIO="Digichamps Audios/";
    public static final String CHAT_IMAGES="Digichamps Images/";
    public static final String CHAT_VIDEOS="Digichamps Videos/";
    public static final String CHAT_SENT="Sent/";
    public static final String CHAT_RECIEVE="Recieved/";

    public static final String AUDIO_SENT_DIR= ROOT+DIGI_MEDIA+CHAT_AUDIO+CHAT_SENT;
    public static final String AUDIO_RECR_DIR=ROOT+DIGI_MEDIA+CHAT_AUDIO+CHAT_RECIEVE;

    public static final String VIDEO_SENT_DIR= ROOT+DIGI_MEDIA+CHAT_VIDEOS+CHAT_SENT;
    public static final String VIDEO_RECR_DIR=ROOT+DIGI_MEDIA+CHAT_VIDEOS+CHAT_RECIEVE;

    public static final String IMAGE_SENT_DIR=ROOT+DIGI_MEDIA+CHAT_IMAGES+CHAT_SENT;
    public static final String IMAGE_RECR_DIR= ROOT+DIGI_MEDIA+CHAT_IMAGES+CHAT_RECIEVE;

    public static final int MEDIA_TYPE_IMAGE=1;
    public static final int MEDIA_TYPE_VIDEO=3;
    public static final int MEDIA_TYPE_AUDIO=2;


    public static final String JPEG_SUFFIX=".jpeg";
    public static final String PNG_SUFFIX=".png";
    public static final String AUDIO_SUFFIX=".3gp";
    public static final String VIDEO_SUFFIX=".mp4";

    public static File getProfileTempFile(String name){
        File mediaStorageDir=new File(Environment.getExternalStorageDirectory()+PROFILE_PICS);
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                //Log.v(TAG, "Unable to create directory :" + mediaStorageDir.getAbsolutePath());
                return null;
            }
        }

        // Create a media file name

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + name+PNG_SUFFIX);
        return mediaFile;
    }
    public static File getOutputMediaFile(int type, String path){
        File mediaStorageDir=new File(Environment.getExternalStorageDirectory()+path);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                //Log.v(TAG,"Unable to create directory :"+mediaStorageDir.getAbsolutePath());
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "TIMG_"+ timeStamp +JPEG_SUFFIX);
        } else if(type == MEDIA_TYPE_AUDIO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "TAUD_"+ timeStamp +AUDIO_SUFFIX);
        }
        else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "TVID_"+ timeStamp +VIDEO_SUFFIX);
        }else {
            return null;
        }

        return mediaFile;
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }


    public static boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
    public static long checkVideoDurationValidation(Context context, Uri uri){
        //Log.d("CommonHandler", "Uri: " + uri);
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(context, uri);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long duration = Long.parseLong(time );

        retriever.release();

        return duration;
    }

  /*  public static Uri downloadImage(Context context,String url, final String mobileno)
    {

        log("downloadImage: " + url);
        boolean status = false;
        Bitmap  bitmap=null;
        File imageFile;
        FileOutputStream  out;
        File pictureDirFile;
        Uri imageUri=null;
        try {
         bitmap = Picasso.with(context).load(url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .get();

            if (bitmap != null) {
              pictureDirFile = new File(Environment.getExternalStorageDirectory() +PROFILE_PICS);
                if (!pictureDirFile.isDirectory()) {
                    pictureDirFile.mkdirs();
                }



              log("file nmae:" + mobileno + ".png");
              imageFile = new File(pictureDirFile.getAbsolutePath() +"/", mobileno + PNG_SUFFIX);
                if (imageFile.exists()) {
                    imageFile.delete();
                    imageFile = new File(pictureDirFile.getAbsolutePath() + "/", mobileno + PNG_SUFFIX);
                }

                out = new FileOutputStream(imageFile);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);

                out.flush();
                out.close();
                imageUri= FileProvider.getUriForFile( context, context.getResources().getString(R.string.file_provider), imageFile);
                status = true;
            } else {
               log("bmp null");
            }
        } catch (Exception e) {
            log(e.getMessage() + "");
        } finally {
                if(bitmap!=null)
                {
                    bitmap.recycle();
                    bitmap=null;
                }
            out=null;
            pictureDirFile=null;
            imageFile=null;
            return imageUri;
        }






    }
*/
  public static String getFileName(long time)
  {
      return "TIMG_"+time+JPEG_SUFFIX;
  }
  public static File saveImageToFolder(String filename, byte[] data)
  {
      File imageRootRecieved = new File(Environment.getExternalStorageDirectory()+
             ROOT ,IMAGE_RECR_DIR);

      if (!imageRootRecieved.isDirectory()) {
          imageRootRecieved.mkdirs();
      }



      try {
          File photo=new File(imageRootRecieved.getAbsolutePath()+"/"+ filename);
          FileOutputStream fos=new FileOutputStream(photo.getPath());

          fos.write(data);
          fos.close();

          return photo;
      }
      catch (java.io.IOException e) {
          //Log.v(TAG, "Exception in photoCallback" + e.getMessage());
          return null;
      }

  }
    public static File saveAudioToFolder(String filename, byte[] data)
    {
        File imageRootRecieved = new File(Environment.getExternalStorageDirectory()+ROOT ,
                AUDIO_RECR_DIR);

        if (!imageRootRecieved.isDirectory()) {
            imageRootRecieved.mkdirs();
        }



        try {
            File photo=new File(imageRootRecieved.getAbsolutePath()+"/"+ filename);
            FileOutputStream fos=new FileOutputStream(photo.getPath());

            fos.write(data);
            fos.close();

            return photo;
        }
        catch (java.io.IOException e) {
            //Log.v(TAG, "Exception in photoCallback" + e.getMessage());
            return null;
        }

    }
    public static File getProfileFile(String mobile)
    {
        return new File(Environment.getExternalStorageDirectory(),
                FileUtil.ROOT + FileUtil.PHOTO_PICS + mobile+".png");
    }
    public  static  boolean isImageExist(String mobileno)
    {
        boolean status=false;
        File f = new File(Environment.getExternalStorageDirectory()+PROFILE_PICS);
        File file=null;
        if (f.isDirectory()) {

           file = new File(f.getAbsolutePath() + "/", mobileno+PNG_SUFFIX);
            if (file.exists()) {
                if (file.length() > 0) {
                    status = true;
                }
            }
        }
        file=null;
        f=null;

        return  status;

    }


}

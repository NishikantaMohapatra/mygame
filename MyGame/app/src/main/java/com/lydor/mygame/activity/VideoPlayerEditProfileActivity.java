package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.lydor.mygame.R;


import java.io.File;
import java.util.Locale;

public class VideoPlayerEditProfileActivity extends AppCompatActivity {

    VideoView videoView;
    ImageView imageView;
    SeekBar seekbar;
    String strVideoUrl;
    Uri uri;
    boolean isPlay = false;
    Handler handler;

    private static long START_TIME_IN_MILLIS=0;
    private TextView mTextViewCountdown;
    private CountDownTimer mCountDownTimer,t;
    int counter;
    private boolean mTimmerRunning;

    private static long
            mTimeLeftInMills;
    private TextView retake,next;
    ImageView imvBack;
    int duration;
    String str_video;
    long timetime;
    long timeInMillisec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_preview);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        //getSupportActionBar().hide();
        init();
    }

    private void init() {

        videoView = findViewById(R.id.videoView);
        imageView = findViewById(R.id.imageView);
        seekbar = findViewById(R.id.seekBar);
        mTextViewCountdown = findViewById(R.id.tvTimer);
        retake = findViewById(R.id.retake);
        next = findViewById(R.id.next);
        handler = new Handler();

        imvBack = findViewById(R.id.imvBack);


        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        String fromWhere = getIntent().getStringExtra("from");

        if (fromWhere.equalsIgnoreCase("adapter")) {

            next.setVisibility(View.VISIBLE);
            retake.setVisibility(View.GONE);
        } else {
            next.setVisibility(View.GONE);
            retake.setVisibility(View.GONE);
        }

        /*DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) videoView.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;*/
        strVideoUrl = "https://androidwave.com/media/androidwave-video-exo-player.mp4";

        str_video = getIntent().getStringExtra("video");

      /*  if (fromWhere.equalsIgnoreCase("adapter")){


        }*/


        videoView.setFitsSystemWindows(true);
        //videoView.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        uri = Uri.parse(str_video);
        videoView.setVideoURI(uri);
        videoView.start();


        /*//update package name
        uri = Uri.parse(strVideoUrl);
        //videoView.setLayoutParams(params);
        videoView.setVideoURI(uri);
        videoView.start();*/


        //resumeTimer();

        if(videoView.isPlaying()){
            imageView.setImageResource(R.drawable.ic_pausesign);

        }
        startTimerTimer(1000000);
        isPlay=true;


        updateSeekBar();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isPlay){

                    videoView.pause();
                    isPlay = false;
                    imageView.setImageResource(R.drawable.ic_playsign);
                    onPauseTimer();
                    //startTimer();
                }else if(!isPlay){
                    //t.start();
                    videoView.start();
                    isPlay = true;
                    updateSeekBar();
                    resumeTimer();

                    imageView.setImageResource(R.drawable.ic_pausesign);
                }
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                duration = videoView.getDuration();
            }
        });


        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer vmp) {
                onPauseTimer();
            }
        });

        /*MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(this, Uri.fromFile(new File(strVideoUrl)));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        timeInMillisec = Long.parseLong(time);

        retriever.release();

        startTimerTimer(timeInMillisec);*/






    }

    private void onPauseTimer(){
        t.cancel();
    }

    private void resumeTimer(){

        startTimerTimer(timeInMillisec);

    }


    private void startTimerTimer(long mTimeLeftInMills){


        //updateCountDownStart();




        t = new CountDownTimer(mTimeLeftInMills,1000) {
            @Override
            public void onTick(long l) {
                timetime = l;
                counter++;
                timeInMillisec = l;
                String time = new Integer(counter).toString();

                long millis = counter;
                int seconds = (int) (millis / 60);
                int minutes = seconds / 60;
                seconds     = seconds % 60;

                mTextViewCountdown.setText(String.format("%02d:%02d", seconds,millis));
            }

            @Override
            public void onFinish() {
                //textView.setText("FINISH!!");
                //t.cancel();


                counter = 0;
            }
        }.start();




    }

    private void updateSeekBar() {

        handler.postDelayed(updateTimeTask,100);
        //startTimer();

    }

    public Runnable updateTimeTask = new Runnable() {
        @Override
        public void run() {
            seekbar.setProgress(videoView.getCurrentPosition());
            seekbar.setMax(videoView.getDuration());
            int testesffsddag = videoView.getDuration();
            int sdfasdf = videoView.getCurrentPosition();
            START_TIME_IN_MILLIS = videoView.getCurrentPosition();
            mTimeLeftInMills = START_TIME_IN_MILLIS;

            //updateCountDownStart();






            handler.postDelayed(this,100);

            //startTimer();

            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    handler.removeCallbacks(updateTimeTask);
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    handler.removeCallbacks(updateTimeTask);
                    videoView.seekTo(seekBar.getProgress());
                    imageView.setImageResource(R.drawable.ic_playsign);
                    updateSeekBar();
                    //pauseTimer();
                }
            });
        }
    };


    private void startTimer(){

        mCountDownTimer = new CountDownTimer(mTimeLeftInMills,1000) {
            @Override
            public void onTick(long l) {

                mTimeLeftInMills = l;
                //updateCountDownStart();
            }

            @Override
            public void onFinish() {
                mTimmerRunning = false;
            }
        }.start();

        mTimmerRunning = true;

    }

    private void pauseTimer(){

        mCountDownTimer.cancel();
        mTimmerRunning = false;


    }

    private void resetTimer(){

        mTimeLeftInMills = START_TIME_IN_MILLIS;
        //updateCountDownStart();
    }


    private void updateCountDownStart(){

        int minutes = (int) (mTimeLeftInMills / 1000) / 60;
        int seconds = (int) (mTimeLeftInMills / 1000) % 60;

        String timeLeftFormated = String.format(Locale.getDefault(),"%02d:%02d", minutes,seconds);

        //mTextViewCountdown.setText(timeLeftFormated);


    }




}
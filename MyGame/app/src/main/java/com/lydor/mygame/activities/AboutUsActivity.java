package com.lydor.mygame.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activity.BaseActivity;
import com.lydor.mygame.activity.EditProfileActivity;
import com.lydor.mygame.activity.LoginActivity;
import com.lydor.mygame.activity.ReportUsActivity;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.fragment.MyProfileMyChallengesListActivity;
import com.lydor.mygame.fragment.MyProfileMyFeedVideoList;
import com.lydor.mygame.fragment.MyProfileMyPerformanceVideo;
import com.lydor.mygame.fragmentss.MomentsFragment;
import com.lydor.mygame.model.UserMyProfileResponse;
import com.lydor.mygame.model.VideoLikeUnLikeRequest;
import com.lydor.mygame.model.VideoLikeUnlikeResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AboutUsActivity extends BaseActivity {


    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Button btnEditProfile;
    private ImageView imvBack;
    private TextView tvTotalPosts,tvTotalFollowers,tvTotalFollowing,tvName,tvBio,toolbarTitle;
    private ArrayList<UserMyProfileResponse> userMyProfileResponse;
    private CircleImageView imv;
    private SharedPref preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activites_about_us);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        btnEditProfile = findViewById(R.id.btnEditProfile);
        imvBack = findViewById(R.id.imvBack);
        tvName = findViewById(R.id.tvName);
        tvBio = findViewById(R.id.tvBio);
        tvTotalPosts = findViewById(R.id.tvTotalPosts);
        tvTotalFollowers = findViewById(R.id.tvTotalFollowers);
        tvTotalFollowing = findViewById(R.id.tvTotalFollowing);
        toolbarTitle = findViewById(R.id.toolbarTitle);
        imv = findViewById(R.id.imv);
        preferences = new SharedPref(this);


        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });







        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(com.lydor.mygame.activities.AboutUsActivity.this, EditProfileActivity.class);
                in.putExtra("obj",userMyProfileResponse);
                startActivity(in);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MyProfileMyFeedVideoList(), "MOMENTS");
        adapter.addFragment(new MyProfileMyPerformanceVideo(),"PERFORMANCE");
        adapter.addFragment(new MyProfileMyChallengesListActivity(), "CHALLENGES");
        viewPager.setAdapter(adapter);
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void getMyProfile(){


        if (AppUtil.isInternetConnected(AboutUsActivity.this)) {
            enableDialog("Please wait...");



            RestService apiService =
                    ApiClient.getClient(AboutUsActivity.this).create(RestService.class);

            Call<ArrayList<UserMyProfileResponse>> call = apiService.postGettingUserDetails();
            call.enqueue(new Callback<ArrayList<UserMyProfileResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<UserMyProfileResponse>> call, Response<ArrayList<UserMyProfileResponse>> response) {


                    disableDialog();



                    if(response.code()==200){
                        userMyProfileResponse = response.body();
                        if(userMyProfileResponse!=null){

                            String userName = null;
                            String userBio= null;
                            int totalPost = 0;
                            int totalFollowers = 0;
                            int totoalFollowing = 0;
                            String userImage = null;
                            String userTag = null;
                            int iFollow = 0;
                            for(int i=0;i<response.body().size();i++){
                                userName = response.body().get(i).getName();
                                userBio = response.body().get(i).getUser_bio();
                                totalPost = response.body().get(i).getTotal_posts();
                                totalFollowers = response.body().get(i).getTotal_followers();
                                totoalFollowing = response.body().get(i).getTotal_following();
                                userImage = response.body().get(i).getUser_image();
                                userTag = response.body().get(i).getUser_tag();


                            }

                            if(userTag!=null){
                                tvName.setText("@"+userTag);
                            }else{
                                tvName.setText("User doesn't have a unique user tag");
                            }

                            if(userBio!=null){
                                tvBio.setText(userBio);
                            }else{
                                tvBio.setText("No bio till yet saved");
                            }

                            tvTotalPosts.setText(totalPost+"");
                            tvTotalFollowers.setText(totoalFollowing+"");
                            tvTotalFollowing.setText(totalFollowers+"");
                            toolbarTitle.setText(userName);


                            if(userImage!=null) {
                                //holder.imv.setTag(liveStreamLists.get(position).getImage());
                                Picasso.with(AboutUsActivity.this).load("http://mygame-app.s3.ap-south-1.amazonaws.com/"+userImage)
                                        .into(new Target() {
                                            @Override
                                            public void onBitmapLoaded(Bitmap bitmap,
                                                                       Picasso.LoadedFrom from) {

                                                imv.setImageBitmap(bitmap);

                                            }

                                            @Override
                                            public void onBitmapFailed(Drawable errorDrawable) {
                                                imv.setImageDrawable(
                                                        getResources().
                                                                getDrawable(R.drawable.ic_user));

                                            }

                                            @Override
                                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                                            }
                                        });
                            }
                            else
                            {
                                imv.setImageDrawable(
                                        getResources().
                                                getDrawable(R.drawable.ic_user)
                                );

                            }

                        }
                    }else {


                        if (response != null && response.errorBody() != null) {
                            if(response.code()==401){
                                Toast.makeText(AboutUsActivity.this, "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                                Intent in = new Intent(AboutUsActivity.this, LoginActivity.class);
                                startActivity(in);
                                preferences.logout();
                                finish();
                            }else{
                                Toast.makeText(AboutUsActivity.this, Constants.SOME_THING_WRONG , Toast.LENGTH_SHORT).show();
                            }
                            //Toast.makeText(AboutUsActivity.this, response.message()+"", Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }


                }

                @Override
                public void onFailure(Call<ArrayList<UserMyProfileResponse>> call, Throwable t) {

                    disableDialog();

                    Toast.makeText(AboutUsActivity.this, Constants.SOME_THING_WRONG, Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            disableDialog();
            Toast.makeText(AboutUsActivity.this,
                    "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getMyProfile();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(AboutUsActivity.this, MainActivity.class);
        startActivity(in);
        finish();
    }
}

package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RegistrationResponse implements Parcelable {

    private String status;

    public RegistrationResponse() {
    }

    protected RegistrationResponse(Parcel in) {
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RegistrationResponse> CREATOR = new Creator<RegistrationResponse>() {
        @Override
        public RegistrationResponse createFromParcel(Parcel in) {
            return new RegistrationResponse(in);
        }

        @Override
        public RegistrationResponse[] newArray(int size) {
            return new RegistrationResponse[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.lydor.mygame.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activity.LoginActivity;
import com.lydor.mygame.adapter.ChallengesListingAdapter;
import com.lydor.mygame.adapter.FollowersListAdapter;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.FollowUnfollowRequest;
import com.lydor.mygame.model.FollowUnfollowResponse;
import com.lydor.mygame.model.FollowersResponse;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.MyChallengesList;
import com.lydor.mygame.model.MyChallengesResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;
import com.lydor.mygame.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FollowersFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FollowersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FollowersFragment extends Fragment implements FollowersListAdapter.EventListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RecyclerView rcvList;
    ArrayList<FollowersResponse> mList;
    FollowersListAdapter mAdapter;
    private ProgressDialog mProgressDialog;
    private SharedPref pref;
    private TextView errTv;
    private EditText etSearch;

    private OnFragmentInteractionListener mListener;

    public FollowersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FollowersFragment newInstance(String param1, String param2) {
        FollowersFragment fragment = new FollowersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_followers, container, false);
        errTv = view.findViewById(R.id.errTv);
        rcvList = view.findViewById(R.id.rcvList);
        mProgressDialog = new ProgressDialog(getActivity());
        pref = new SharedPref(getActivity());
        mList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rcvList.setLayoutManager(linearLayoutManager);
        mAdapter = new FollowersListAdapter(mList,getActivity(),this);
        rcvList.setAdapter(mAdapter);
        rcvList.addItemDecoration(new SimpleDividerItemDecoration(getActivity(),10,true));
        etSearch = view.findViewById(R.id.tvSearch);


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(etSearch.getText().length()>=3){
                    searchFollowers(etSearch.getText().toString());
                }else{
                    searchFollowers("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getFollowersList();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void postFollowUnfollow(String followersId,int status) {
        postFollowUnfollowRequest(followersId,status);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void getFollowersList(){
        if (AppUtil.isInternetConnected(getActivity())) {
            mProgressDialog= ProgressDialog.show(getActivity(),null, Constants.PLEASE_WAIT);



            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);

            Call<ArrayList<FollowersResponse>> call = apiService.getFollowerList(
                    );
            call.enqueue(new Callback<ArrayList<FollowersResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<FollowersResponse>> call, Response<ArrayList<FollowersResponse>> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        ArrayList<FollowersResponse> followersResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        if(followersResponse.size()>0){
                            mList.clear();
                            mList.addAll(followersResponse);
                            rcvList.scrollToPosition(0);
                            mAdapter.notifyDataSetChanged();

                            rcvList.setVisibility(View.VISIBLE);
                            errTv.setVisibility(View.GONE);
                            etSearch.setVisibility(View.VISIBLE);

                        }else{
                            rcvList.setVisibility(View.GONE);
                            errTv.setVisibility(View.VISIBLE);
                            errTv.setText("You don't have any followers");
                            mList.clear();
                            mAdapter.notifyDataSetChanged();
                            etSearch.setVisibility(View.GONE);

                            //Toast.makeText(getActivity(), "No users following you", Toast.LENGTH_SHORT).show();
                        }
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<ArrayList<FollowersResponse>> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            //Toast.makeText(getActivity().getApplicationContext(), "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }


    public void postFollowUnfollowRequest(String followId,int status){
        if (AppUtil.isInternetConnected(getActivity())) {
            //mProgressDialog= ProgressDialog.show(getActivity(),null, Constants.PLEASE_WAIT);

            FollowUnfollowRequest request = new FollowUnfollowRequest();
            request.setFollowing_id(followId);
            request.setFollow(status);

            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);

            Call<FollowUnfollowResponse> call = apiService.getFollowUnfollow(request
            );
            call.enqueue(new Callback<FollowUnfollowResponse>() {
                @Override
                public void onResponse(Call<FollowUnfollowResponse> call, Response<FollowUnfollowResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        FollowUnfollowResponse followUnfollowResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        getFollowersList();
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<FollowUnfollowResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(getActivity(), "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){

            getFollowersList();
        }
    }

    public void searchFollowers(String name){
        if (AppUtil.isInternetConnected(getActivity())) {




            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);

            Call<ArrayList<FollowersResponse>> call = apiService.searchFollowersList(name
            );
            call.enqueue(new Callback<ArrayList<FollowersResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<FollowersResponse>> call, Response<ArrayList<FollowersResponse>> response) {





                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        ArrayList<FollowersResponse> followersResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        if(followersResponse.size()>0){
                            mList.clear();
                            mList.addAll(followersResponse);
                            mAdapter.notifyDataSetChanged();
                            rcvList.setVisibility(View.VISIBLE);
                            errTv.setVisibility(View.GONE);
                        }else{
                            rcvList.setVisibility(View.GONE);
                            errTv.setVisibility(View.VISIBLE);
                            errTv.setText("You don't have any followers");
                            mList.clear();
                            mAdapter.notifyDataSetChanged();
                            //Toast.makeText(getActivity(), "No users following you", Toast.LENGTH_SHORT).show();
                        }
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<ArrayList<FollowersResponse>> call, Throwable t) {

                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {

            Toast.makeText(getActivity(), "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }
}


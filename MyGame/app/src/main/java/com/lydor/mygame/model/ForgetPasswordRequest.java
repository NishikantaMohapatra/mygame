package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ForgetPasswordRequest implements Parcelable {

    private String email;


    protected ForgetPasswordRequest(Parcel in) {
        email = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ForgetPasswordRequest> CREATOR = new Creator<ForgetPasswordRequest>() {
        @Override
        public ForgetPasswordRequest createFromParcel(Parcel in) {
            return new ForgetPasswordRequest(in);
        }

        @Override
        public ForgetPasswordRequest[] newArray(int size) {
            return new ForgetPasswordRequest[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ForgetPasswordRequest() {
    }
}

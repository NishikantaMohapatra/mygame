package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserMyFeedRequest implements Parcelable {

    private int user_id;

    public UserMyFeedRequest() {
    }

    protected UserMyFeedRequest(Parcel in) {
        user_id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(user_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserMyFeedRequest> CREATOR = new Creator<UserMyFeedRequest>() {
        @Override
        public UserMyFeedRequest createFromParcel(Parcel in) {
            return new UserMyFeedRequest(in);
        }

        @Override
        public UserMyFeedRequest[] newArray(int size) {
            return new UserMyFeedRequest[size];
        }
    };

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}

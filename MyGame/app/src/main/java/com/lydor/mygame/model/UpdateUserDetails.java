package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UpdateUserDetails implements Parcelable {

    private String status;

    public UpdateUserDetails() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static Creator<UpdateUserDetails> getCREATOR() {
        return CREATOR;
    }

    protected UpdateUserDetails(Parcel in) {
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UpdateUserDetails> CREATOR = new Creator<UpdateUserDetails>() {
        @Override
        public UpdateUserDetails createFromParcel(Parcel in) {
            return new UpdateUserDetails(in);
        }

        @Override
        public UpdateUserDetails[] newArray(int size) {
            return new UpdateUserDetails[size];
        }
    };
}

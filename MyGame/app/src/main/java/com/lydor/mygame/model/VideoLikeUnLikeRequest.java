package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class VideoLikeUnLikeRequest implements Parcelable {


    private String video_id;
    private int like;

    public VideoLikeUnLikeRequest() {
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public static Creator<VideoLikeUnLikeRequest> getCREATOR() {
        return CREATOR;
    }

    protected VideoLikeUnLikeRequest(Parcel in) {
        video_id = in.readString();
        like = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(video_id);
        dest.writeInt(like);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoLikeUnLikeRequest> CREATOR = new Creator<VideoLikeUnLikeRequest>() {
        @Override
        public VideoLikeUnLikeRequest createFromParcel(Parcel in) {
            return new VideoLikeUnLikeRequest(in);
        }

        @Override
        public VideoLikeUnLikeRequest[] newArray(int size) {
            return new VideoLikeUnLikeRequest[size];
        }
    };
}

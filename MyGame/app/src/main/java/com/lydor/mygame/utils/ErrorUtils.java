package com.lydor.mygame.utils;

import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.LoginErrorModel;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

    public static LoginErrorModel parseError(Response<?> response) {
        Converter<ResponseBody, LoginErrorModel> converter =
                ApiClient.retrofit()
                        .responseBodyConverter(LoginErrorModel.class, new Annotation[0]);

        LoginErrorModel error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new LoginErrorModel();
        }

        return error;
    }
}

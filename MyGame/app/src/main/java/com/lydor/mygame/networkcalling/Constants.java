package com.lydor.mygame.networkcalling;

/**
 * Created by user on 22-01-2018.
 */

public class Constants {


    public static final String IMAGE_BASE="http://40.114.87.117:8080/images/";
    public static final String IMAGE_EE=IMAGE_BASE+"EntryExit/Staff/";

    public static String PACKAGENAME = "com.sseduventures.superguard.access";
    public static final String IMAGE_FOLDER_PATH_STAFF="http://40.114.87.117:8080/images/Staff/";
    public static String IMAGE_PROFILE = "http://40.114.87.117:8080/images/ProfilePic/";
    public static final String IMAGE_VEHICLE="http://40.114.87.117:8080/images/Vehicle/";
    public static String IMAGE_CAB = "http://40.114.87.117:8080/images/Company/";
    public static final String IMAGE_FOLDER_PATH_ACTION_LOG="http://40.114.87.117:8080/images/Action/";
    public static final String IMAGE_FOLDER_PATH_LSU="http://40.114.87.117:8080/images/LocalServiceUser/";
    public static String IMAGE_FM = "http://40.114.87.117:8080/images/FamilyMember/";
    public static String IMAGE_HELP_DESK = "http://40.114.87.117:8080/images/HelpDesk/";

    public static String IMAGE_NOTICE = "http://40.114.87.117:8080/images/Notice/";

    public static String IMAGE_LSU = "http://40.114.87.117:8080/images/LocalServiceUser/";

    public static final String IMAGE_STAFF= IMAGE_BASE +"Staff/";
    public static final String IMAGE_FOLDER_PATH_HELP_DESK_COMMENT= "http://40.114.87.117:8080/images/HelpDesk/Comment/";

    public static String IMAGE_GIVEN_ITEMS = "http://40.114.87.117:8080/images/EntryExit/GivenItems/";


    public static final String ACTION_CAB="Cab";
    public static final String ACTION_DELIVERY="Delivery";
    public static final String ACTION_GUEST="Guest";

    public static final String ACTION_PARCEL_AT_GATE="Parcel At Gate";

    public static final String ACTION_KID="Kid";
    public static final String ACTION_STAFF="Staff";
    public static final String ACTION_VEHICLE="Vehicle";
    public static final String ACTION_SCHOOL_BUS="School Bus";

    public static final String ACTION_LSU="Local Service User";

    public static final String ACTION_ENTERED="ENTERED";
    public static final String ACTION_CANCELLED="CANCELLED";
    public static final String ACTION_INSIDE="INSIDE";
    public static final String ACTION_ALLOW="ALLOW";
    public static final String ACTION_DECLINED="DECLINED";
    public static final String ACTION_LEFT="LEFT";
    public static final String ACTION_EXPIRED="EXPIRED";

    public static final String ACTION_ACTIVE="ACTIVE";
    public static final String ACTION_DELETED="DELETED";
    public static final String ACTION_RECEIVED="RESIDENT RECEIVED THE PARCEL";
    public static final String ACTION_PARCEL_AT_GATE_TEXT="PARCEL AT THE GATE";

    public static final String ACTION_WAITING_APPROVAL="WAITING FOR APPROVAL";



    public static final String ACTION_PRE_APPROVED="PRE-APPROVAL";
    public static final String ACTION_POST_APPROVED="POST-APPROVAL";

    public static final String ACTION_APPROVED="APPROVED";

    public static final String ATTENDANCE_LSU="LOCAL SERVICE USER";

    public static final String ATTENDANCE_CAB="CAB";
    public static final String ATTENDANCE_DELIVERY="DELIVERY";
    public static final String ATTENDANCE_GUEST="GUEST";
    public static final String ATTENDANCE_KID="KID";
    public static final String ATTENDANCE_STAFF="STAFF";
    public static final String ATTENDANCE_SCHOOL_BUS="SCHOOL_BUS";
    public static final String ATTENDANCE_VEHICLE="VEHICLE";


    public static final String HELP_DESK_NEW="NEW";
    public static final String HELP_DESK_PROGRESS="IN-PROGRESS";
    public static final String HELP_DESK_RESOLVED="RESOLVED";
    public static final String HELP_DESK_REOPEN="RE-OPEN";
    public static final String HELP_DESK_RESOLVE="RESOLVE";


    public static final int NOTIFY_CODE_NOTICE=6001;
    public static final int NOTIFY_CODE_COMPLAINT_INPROGRESS=6002;
    public static final int NOTIFY_CODE_CAB=6003;
    public static final int NOTIFY_CODE_DELIVERY=6004;
    public static final int NOTIFY_CODE_GUEST=6005;
    public static final int NOTIFY_CODE_KID=6006;
    public static final int NOTIFY_CODE_ALERT=6007;
    public static final int NOTIFY_CODE_COMPLAINT=6008;



    public static final String KID_ENTRY_EXIT_ALLOW_EXIT="ALLOW EXIT";
    public static final String KID_PERMISSION_NEED_APPROVAL="Need Approval";
    public static final String KID_PERMISSION_NEED_ALWAYS="Allow Exit Without Permission";

    public static final String IMAGE_PATH_ME= IMAGE_BASE +"ManualEntry/";
    public static final String IMAGE_PATH_MEK= IMAGE_BASE +"ManualEntry/Kid/";
    public static final String IMAGE_PATH_MEV= IMAGE_BASE +"ManualEntry/Vehicle/";


}

package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SearchUserRequest implements Parcelable {

    private String name;

    public SearchUserRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Creator<SearchUserRequest> getCREATOR() {
        return CREATOR;
    }

    protected SearchUserRequest(Parcel in) {
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchUserRequest> CREATOR = new Creator<SearchUserRequest>() {
        @Override
        public SearchUserRequest createFromParcel(Parcel in) {
            return new SearchUserRequest(in);
        }

        @Override
        public SearchUserRequest[] newArray(int size) {
            return new SearchUserRequest[size];
        }
    };
}

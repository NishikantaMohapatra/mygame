package com.lydor.mygame.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;


import androidx.fragment.app.Fragment;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Container;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.TrackBox;
import com.googlecode.mp4parser.DataSource;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Mp4TrackImpl;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activity.CustomCameraActivity;
import com.lydor.mygame.activity.GalleryActivity;
import com.lydor.mygame.activity.VideoPreviewActivity;
import com.lydor.mygame.camera.AutoFitTextureView;
import com.lydor.mygame.camera.CameraVideoFragment;
import com.lydor.mygame.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import me.tankery.lib.circularseekbar.CircularSeekBar;

/*import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;*/


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CameraFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CameraFragment extends CameraVideoFragment {

    private static final String TAG = "CameraFragment";
    private static final String VIDEO_DIRECTORY_NAME = "AndroidWave";

    AutoFitTextureView mTextureView;

    ImageView mRecordVideo;

    VideoView mVideoView;

    ImageView mPlayVideo,galleryVideo;

    CircularSeekBar seekBar;

    ImageView switchCamera,imvBack;

    CountDownTimer t;
    int counter;
    //Unbinder unbinder;
    private String mOutputFilePath;
    int pStatus;
    private Handler handler = new Handler();
    TextView countTimer,labelHeading;
    String from,challengeId;
    SharedPref pref;
    int i = 0;



    public CameraFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */


    public static CameraFragment newInstance() {
        CameraFragment fragment = new CameraFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.test_video_ui, container, false);
        //unbinder = ButterKnife.bind(this, view);
        mTextureView = view.findViewById(R.id.mTextureView);
        mRecordVideo = view.findViewById(R.id.mRecordVideo);
        mVideoView = view.findViewById(R.id.mVideoView);
        mPlayVideo = view.findViewById(R.id.mPlayVideo);
        seekBar = view.findViewById(R.id.seekBar);
        switchCamera = view.findViewById(R.id.switchCamera);
        countTimer = view.findViewById(R.id.countTimer);
        labelHeading = view.findViewById(R.id.labelHeading);
        imvBack = view.findViewById(R.id.imvBack);
        pref = new SharedPref(getActivity());
        galleryVideo = view.findViewById(R.id.galleryVideo);


        seekBar.setProgress(0);   // Main Progress
        // Secondary Progress
        seekBar.setMax(2000);


        if(getActivity().getIntent().getStringExtra("From")!=null){
            from = getActivity().getIntent().getStringExtra("From");
            if(from.equalsIgnoreCase("challenges")){
                labelHeading.setText("CREATE CHALLENGE");
                challengeId = getActivity().getIntent().getStringExtra("id");
                galleryVideo.setVisibility(View.GONE);
            }else{
                labelHeading.setText("CREATE VIDEO");
                galleryVideo.setVisibility(View.VISIBLE);
            }
        }

        switchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeCamera();
            }
        });

        mRecordVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIsRecordingVideo) {
                    try {
                        stopRecordingVideo();
                        prepareViews();
                        t.cancel();

                        seekBar.setProgress(0);
                        mRecordVideo.setBackground(getResources().getDrawable(R.drawable.ellipse_4));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    startRecordingVideo();
                    mRecordVideo.setImageResource(R.drawable.rounded_rectangle_2_copy);
                    t.start();
                    pref.putBoolean(Constants.IS_RESUME,false);
                    //Receive out put file here
                    pStatus = 0;


                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            while (pStatus < 2000) {
                                pStatus += 1;

                                handler.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        // TODO Auto-generated method stub


                                        if(pref.getBoolean(Constants.IS_RESUME)){
                                            seekBar.setProgress(0);
                                        }else{
                                            seekBar.setProgress(pStatus);
                                        }

                                        //tv.setText(pStatus + "%");

                                    }
                                });
                                try {
                                    // Sleep for 200 milliseconds.
                                    // Just to display the progress slowly
                                    i = 8;
                                    Thread.sleep(i); //thread will take approx 1.5 seconds to finish
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).start();

                    mOutputFilePath = getCurrentFile().getAbsolutePath();
                }
            }
        });


        mPlayVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mVideoView.start();
                mPlayVideo.setVisibility(View.GONE);
            }
        });


        t = new CountDownTimer(18000,1000) {
            @Override
            public void onTick(long l) {
                counter++;


                String time = new Integer(counter).toString();

                long millis = counter;
                int seconds = (int) (millis / 60);
                int minutes = seconds / 60;
                seconds     = seconds % 60;

                countTimer.setText(String.format("%02d:%02d", seconds,millis));
            }

            @Override
            public void onFinish() {
                //textView.setText("FINISH!!");
                //t.cancel();


                try {
                    stopRecordingVideo();
                    prepareViews();
                    t.cancel();

                    seekBar.setProgress(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mRecordVideo.setBackground(getResources().getDrawable(R.drawable.ellipse_4));


                seekBar.setProgress(0);
                counter = 0;
            }
        };

        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });


        galleryVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(),
                        GalleryActivity.class);
                startActivity(in);
            }
        });


        return view;
    }

    @Override
    public int getTextureResource() {
        return R.id.mTextureView;
    }

    @Override
    protected void setUp(View view) {

    }

    /*@OnClick({R.id.mRecordVideo, R.id.mPlayVideo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mRecordVideo:
                *//**
                 * If media is not recoding then start recording else stop recording
                 *//*
                if (mIsRecordingVideo) {
                    try {
                        stopRecordingVideo();
                        prepareViews();

                        seekBar.setProgress(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    startRecordingVideo();
                    mRecordVideo.setImageResource(R.drawable.rounded_rectangle_2_copy);
                    //Receive out put file here
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            while (pStatus < 2000) {
                                pStatus += 1;

                                handler.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        // TODO Auto-generated method stub
                                        seekBar.setProgress(pStatus);
                                        //tv.setText(pStatus + "%");

                                    }
                                });
                                try {
                                    // Sleep for 200 milliseconds.
                                    // Just to display the progress slowly
                                    Thread.sleep(8); //thread will take approx 1.5 seconds to finish
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).start();

                    mOutputFilePath = getCurrentFile().getAbsolutePath();
                }
                break;
            case R.id.mPlayVideo:
                mVideoView.start();
                mPlayVideo.setVisibility(View.GONE);
                break;
        }
    }*/

    private void prepareViews() {
        if (mVideoView.getVisibility() == View.GONE) {
            mVideoView.setVisibility(View.GONE);
            mPlayVideo.setVisibility(View.GONE);
            mTextureView.setVisibility(View.GONE);
            seekBar.setProgress(0);
            Intent in = new Intent(getActivity(), VideoPreviewActivity.class);
            if(from.equalsIgnoreCase("challenges")){
                in.putExtra("video",mOutputFilePath);
                in.putExtra("from","custom");
                in.putExtra("id",challengeId);
                in.putExtra("fromActivity",from);
            }else{
                in.putExtra("video",mOutputFilePath);
                in.putExtra("from","custom");
                in.putExtra("fromActivity",from);
            }
            startActivity(in);
            getActivity().finish();

            /*try {
                setMediaForRecordVideo();
            } catch (IOException e) {
                e.printStackTrace();
            }*/
        }
    }

    private void setMediaForRecordVideo() throws IOException {
        mOutputFilePath = parseVideo(mOutputFilePath);
        // Set media controller
        mVideoView.setMediaController(new MediaController(getActivity()));
        mVideoView.requestFocus();
        mVideoView.setVideoPath(mOutputFilePath);
        mVideoView.seekTo(100);
        mVideoView.setOnCompletionListener(mp -> {
            // Reset player
            mVideoView.setVisibility(View.GONE);
            mTextureView.setVisibility(View.VISIBLE);
            mPlayVideo.setVisibility(View.GONE);
            mRecordVideo.setImageResource(R.drawable.ellipse_4);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //unbinder.unbind();
    }

    private String parseVideo(String mFilePath) throws IOException {
        DataSource channel = new FileDataSourceImpl(mFilePath);
        IsoFile isoFile = new IsoFile(channel);
        List<TrackBox> trackBoxes = isoFile.getMovieBox().getBoxes(TrackBox.class);
        boolean isError = false;
        for (TrackBox trackBox : trackBoxes) {
            TimeToSampleBox.Entry firstEntry = trackBox.getMediaBox().getMediaInformationBox().getSampleTableBox().getTimeToSampleBox().getEntries().get(0);
            // Detect if first sample is a problem and fix it in isoFile
            // This is a hack. The audio deltas are 1024 for my files, and video deltas about 3000
            // 10000 seems sufficient since for 30 fps the normal delta is about 3000
            if (firstEntry.getDelta() > 10000) {
                isError = true;
                firstEntry.setDelta(3000);
            }
        }
        File file = getOutputMediaFile();
        String filePath = file.getAbsolutePath();
        if (isError) {
            Movie movie = new Movie();
            for (TrackBox trackBox : trackBoxes) {
                movie.addTrack(new Mp4TrackImpl(channel.toString() + "[" + trackBox.getTrackHeaderBox().getTrackId() + "]", trackBox));
            }
            movie.setMatrix(isoFile.getMovieBox().getMovieHeaderBox().getMatrix());
            Container out = new DefaultMp4Builder().build(movie);

            //delete file first!
            FileChannel fc = new RandomAccessFile(filePath, "rw").getChannel();
            out.writeContainer(fc);
            fc.close();
            Log.d(TAG, "Finished correcting raw video");
            return filePath;
        }
        return mFilePath;
    }

    /**
     * Create directory and return file
     * returning video file
     */
    private File getOutputMediaFile() {
        // External sdcard file location
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                VIDEO_DIRECTORY_NAME);
        // Create storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + VIDEO_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "VID_" + timeStamp + ".mp4");
        return mediaFile;
    }


    @Override
    public void onResume() {
        super.onResume();

        //pStatus = 0;
        i=0;
        seekBar.setProgress(0);
        //seekBar.onHoverChanged(false);
        counter = 0;
        mIsRecordingVideo = false;
        mRecordVideo.setBackground(getResources().getDrawable(R.drawable.ellipse_4));
        countTimer.setText("00:00");
        pref.putBoolean(Constants.IS_RESUME,true);

        //mRecordVideo.setBackground(getResources().getDrawable(R.drawable.roun));
        //onResume();


    }


    @Override
    public void onPause() {
        super.onPause();
        t.cancel();
        mIsRecordingVideo = false;
        mRecordVideo.setBackground(getResources().getDrawable(R.drawable.ellipse_4));
    }
}
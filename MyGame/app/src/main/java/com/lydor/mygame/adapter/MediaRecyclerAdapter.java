package com.lydor.mygame.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.lydor.mygame.R;
import com.lydor.mygame.model.MyFeedList;

import java.util.ArrayList;

public class MediaRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private ArrayList<MyFeedList> mediaObjects;
  private RequestManager requestManager;
  private Context mContext;
  private PlayerViewHolder.CallBack callBack;



  public MediaRecyclerAdapter(ArrayList<MyFeedList> mediaObjects,
                              RequestManager requestManager, Context mContext, PlayerViewHolder.CallBack callBack) {
    this.mediaObjects = mediaObjects;
    this.requestManager = requestManager;
    this.mContext = mContext;
    this.callBack = callBack;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    return new PlayerViewHolder(
        LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.feed_video_list_item, viewGroup, false));
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
    ((PlayerViewHolder) viewHolder).onBind(mediaObjects.get(i), requestManager,mContext,callBack);
  }

  @Override
  public int getItemCount() {
    return mediaObjects.size();
  }
}

package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lydor.mygame.R;
import com.lydor.mygame.activities.MainActivity;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.LoginRequest;
import com.lydor.mygame.model.LoginResponseNew;
import com.lydor.mygame.model.ReportAProblemRequest;
import com.lydor.mygame.model.ReportAProblemResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportUsActivity extends BaseActivity {

    ImageView imvBack;
    EditText postProblem;
    TextView tvOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_us);

        imvBack = findViewById(R.id.imvBack);
        postProblem = findViewById(R.id.postProblem);
        tvOtp = findViewById(R.id.tvOtp);



        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(postProblem.getText().toString().isEmpty()){

                    Toast.makeText(ReportUsActivity.this, "Please enter your problem...", Toast.LENGTH_SHORT).show();
                }else{
                    postReportAProblem(postProblem.getText().toString());
                }

            }
        });


    }

    private void postReportAProblem(String searchText){
        if (AppUtil.isInternetConnected(ReportUsActivity.this)) {
            enableDialog("Please wait...");
            ReportAProblemRequest request = new ReportAProblemRequest();
            request.setLog(searchText);


            RestService apiService =
                    ApiClient.getClient(this).create(RestService.class);

            Call<ReportAProblemResponse> call = apiService.postReportAProblem(request);
            call.enqueue(new Callback<ReportAProblemResponse>() {
                @Override
                public void onResponse(Call<ReportAProblemResponse> call, Response<ReportAProblemResponse> response) {


                    disableDialog();



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(ReportUsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==201){
                        ReportAProblemResponse successresponse = response.body();
                        if(successresponse.getStatus().equalsIgnoreCase("ok")){
                      Toast.makeText(ReportUsActivity.this, "Report sent to us successfully. Will get back to you soon.. Thank you" , Toast.LENGTH_SHORT).show();
                            finish();


                        }else{
                            Toast.makeText(ReportUsActivity.this, "" + successresponse.getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    }


                }

                @Override
                public void onFailure(Call<ReportAProblemResponse> call, Throwable t) {

                    disableDialog();
                    showToast(t.toString());
                }
            });




        } else {
            disableDialog();
            Toast.makeText(ReportUsActivity.this, "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(ReportUsActivity.this, MainActivity.class);
        startActivity(in);
        finish();
    }
}

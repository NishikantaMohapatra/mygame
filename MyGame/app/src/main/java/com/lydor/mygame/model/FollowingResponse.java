package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FollowingResponse implements Parcelable {

    private String email;
    private String name;
    private int total_followers;
    private int total_following;
    private String user_tag;
    private String user_image;
    private int user_id;

    public FollowingResponse() {
    }

    protected FollowingResponse(Parcel in) {
        email = in.readString();
        name = in.readString();
        total_followers = in.readInt();
        total_following = in.readInt();
        user_tag = in.readString();
        user_image = in.readString();
        user_id = in.readInt();
    }

    public static final Creator<FollowingResponse> CREATOR = new Creator<FollowingResponse>() {
        @Override
        public FollowingResponse createFromParcel(Parcel in) {
            return new FollowingResponse(in);
        }

        @Override
        public FollowingResponse[] newArray(int size) {
            return new FollowingResponse[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal_followers() {
        return total_followers;
    }

    public void setTotal_followers(int total_followers) {
        this.total_followers = total_followers;
    }

    public int getTotal_following() {
        return total_following;
    }

    public void setTotal_following(int total_following) {
        this.total_following = total_following;
    }

    public String getUser_tag() {
        return user_tag;
    }

    public void setUser_tag(String user_tag) {
        this.user_tag = user_tag;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(email);
        parcel.writeString(name);
        parcel.writeInt(total_followers);
        parcel.writeInt(total_following);
        parcel.writeString(user_tag);
        parcel.writeString(user_image);
        parcel.writeInt(user_id);
    }
}

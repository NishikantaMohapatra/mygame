package com.lydor.mygame.networkcalling;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;

import android.util.Log;


import androidx.annotation.Nullable;

import com.lydor.mygame.model.RegistrationRequest;
import com.lydor.mygame.model.RegistrationResponse;
import com.lydor.mygame.model.RegistrationResponseNew;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by NISHIKANT on 11/27/2018.
 */

public class NetworkService extends IntentService
{
    private static final String TAG=NetworkService.class.getSimpleName();

    private int mResultCode= ResponseCodes.ERROR;
    private ResultReceiver mResultReceiver;
    public NetworkService()
    {
        super(TAG);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            mResultReceiver = intent.
                    getParcelableExtra(IntentHelper.EXTRA_PARCELABLE);
            final String action = intent.getAction();
            if (action != null && IntentHelper.ACTION_POST_REGISTRATION.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);

                handleActionLogin(bundle);
            }


        }
    }





    public static void startActionRegistration(Context context,
                                         ResultReceiver receiver,
                                         Bundle bundle)
    {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_POST_REGISTRATION);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }











    private void handleActionLogin(Bundle bundle) {

       /* try {

            RegistrationRequest registrationRequest = new RegistrationRequest();
            registrationRequest.setFirstname(bundle.getString("firstName"));
            registrationRequest.setLastname("test");
            registrationRequest.setEmail(bundle.getString("email"));
            registrationRequest.setPassword(bundle.getString("password"));

            RestService restService = RestServiceBuilder.get(this).
                    getRestService();
            Call<RegistrationResponseNew> scholarShipCall = restService.
                    postRegistration(registrationRequest

                    );

            Response<RegistrationResponseNew> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {

                        RegistrationResponseNew responseDto=response.body();

                        if(responseDto!=null) {

                            if(responseDto.getMesssage().equalsIgnoreCase("User was created."))
                            {
                                mResultCode = ResponseCodes.SUCCESS;
                                bundle.putString(IntentHelper.RESULT_DATA,
                                        responseDto.getMesssage());
                            }else{
                                mResultCode = ResponseCodes.FAILURE;
                                bundle.putString(IntentHelper.RESULT_DATA,
                                        responseDto.getMesssage());
                            }


                        }
                        else
                        {
                            mResultCode = ResponseCodes.FAILURE;
                            bundle.putString(IntentHelper.RESULT_DATA,
                                   responseDto.getMesssage());
                        }
                    } else
                        mResultCode = ResponseCodes.FAILURE;

                } else
                    mResultCode = ResponseCodes.FAILURE;


            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e)
        {
            Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);*/
    }









}

package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class LoginErrorModel implements Parcelable {

    String error;
    String message;
    String fields;

    public LoginErrorModel() {
    }


    protected LoginErrorModel(Parcel in) {
        error = in.readString();
        message = in.readString();
        fields = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(error);
        dest.writeString(message);
        dest.writeString(fields);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginErrorModel> CREATOR = new Creator<LoginErrorModel>() {
        @Override
        public LoginErrorModel createFromParcel(Parcel in) {
            return new LoginErrorModel(in);
        }

        @Override
        public LoginErrorModel[] newArray(int size) {
            return new LoginErrorModel[size];
        }
    };

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }
}

package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class VideoLikeUnlikeResponse implements Parcelable {

    private String status;
    private String message;

    public VideoLikeUnlikeResponse() {
    }

    protected VideoLikeUnlikeResponse(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoLikeUnlikeResponse> CREATOR = new Creator<VideoLikeUnlikeResponse>() {
        @Override
        public VideoLikeUnlikeResponse createFromParcel(Parcel in) {
            return new VideoLikeUnlikeResponse(in);
        }

        @Override
        public VideoLikeUnlikeResponse[] newArray(int size) {
            return new VideoLikeUnlikeResponse[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

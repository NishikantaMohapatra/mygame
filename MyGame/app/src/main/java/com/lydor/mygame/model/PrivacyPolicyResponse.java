package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PrivacyPolicyResponse implements Parcelable {

    private String message;
    private String PolicyText;
    private String UploadedDateTime;
    private String Version;
    private String About_details;

    public PrivacyPolicyResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPolicyText() {
        return PolicyText;
    }

    public void setPolicyText(String policyText) {
        PolicyText = policyText;
    }

    public String getUploadedDateTime() {
        return UploadedDateTime;
    }

    public void setUploadedDateTime(String uploadedDateTime) {
        UploadedDateTime = uploadedDateTime;
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getAbout_details() {
        return About_details;
    }

    public void setAbout_details(String about_details) {
        About_details = about_details;
    }

    public static Creator<PrivacyPolicyResponse> getCREATOR() {
        return CREATOR;
    }

    protected PrivacyPolicyResponse(Parcel in) {
        message = in.readString();
        PolicyText = in.readString();
        UploadedDateTime = in.readString();
        Version = in.readString();
        About_details = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeString(PolicyText);
        dest.writeString(UploadedDateTime);
        dest.writeString(Version);
        dest.writeString(About_details);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PrivacyPolicyResponse> CREATOR = new Creator<PrivacyPolicyResponse>() {
        @Override
        public PrivacyPolicyResponse createFromParcel(Parcel in) {
            return new PrivacyPolicyResponse(in);
        }

        @Override
        public PrivacyPolicyResponse[] newArray(int size) {
            return new PrivacyPolicyResponse[size];
        }
    };
}

package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activities.MainActivity;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.AboutUsResponse;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.MyChallengesResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUsActivity extends BaseActivity {



    private ImageView imvBack;
    private TextView tvAboutUs;
    private ProgressDialog mProgressDialog;


    SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);


        imvBack = findViewById(R.id.imvBack);
        tvAboutUs = findViewById(R.id.tvAboutUs);

        mProgressDialog = new ProgressDialog(this);
        pref = new SharedPref(this);


        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getAboutUs();


    }


    public void getAboutUs(){
        if (AppUtil.isInternetConnected(this)) {
            mProgressDialog= ProgressDialog.show(this,null, Constants.PLEASE_WAIT);



            RestService apiService =
                    ApiClient.getClient(this).create(RestService.class);

            Call<AboutUsResponse> call = apiService.getAboutUsDetails(
                    );
            call.enqueue(new Callback<AboutUsResponse>() {
                @Override
                public void onResponse(Call<AboutUsResponse> call, Response<AboutUsResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(AboutUsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        AboutUsResponse aboutUsResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        if(aboutUsResponse.getMessage().equalsIgnoreCase("Success")){
                            tvAboutUs.setText(aboutUsResponse.getAbout_details());
                        }

                    }else if(response.code()==401){
                        Toast.makeText(AboutUsActivity.this, "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(AboutUsActivity.this, LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        finish();
                    }else{
                        Toast.makeText(AboutUsActivity.this, Constants.SOME_THING_WRONG, Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<AboutUsResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(AboutUsActivity.this, Constants.SOME_THING_WRONG, Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(AboutUsActivity.this, "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(AboutUsActivity.this, MainActivity.class);
        startActivity(in);
        finish();
    }
}

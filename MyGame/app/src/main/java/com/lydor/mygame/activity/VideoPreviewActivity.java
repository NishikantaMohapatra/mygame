package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.fragment.CameraViewNewActivity;

import java.util.Locale;

public class VideoPreviewActivity extends AppCompatActivity {

    VideoView videoView;
    ImageView imageView;
    SeekBar seekbar;
    String strVideoUrl;
    Uri uri;
    boolean isPlay = false;
    Handler handler;

    private static long START_TIME_IN_MILLIS=0;
    private TextView mTextViewCountdown;
    private CountDownTimer mCountDownTimer,t;
    private boolean mTimmerRunning;

    private static long mTimeLeftInMills;
    private TextView retake,next;
    private String str_video;
    String challengeId,fromActivity;
    int counter;
    int duration;
    long timetime;
    long timeInMillisec;
    ImageView imvBack;
    Button btnMoments,btnPerformance;
    SharedPref pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_preview);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        //getSupportActionBar().hide();
        init();
    }

    private void init() {

        imvBack = findViewById(R.id.imvBack);
        videoView = findViewById(R.id.videoView);
        imageView = findViewById(R.id.imageView);
        seekbar = findViewById(R.id.seekBar);
        mTextViewCountdown = findViewById(R.id.tvTimer);
        retake = findViewById(R.id.retake);
        next = findViewById(R.id.next);
        handler = new Handler();

        pref = new SharedPref(this);

        btnMoments = findViewById(R.id.btnMoments);
        btnPerformance = findViewById(R.id.btnPerformance);

        btnMoments.setBackground(getResources().getDrawable(R.drawable.half_button_bg));
        btnPerformance.setBackground(getResources().getDrawable(R.drawable.half_right_bg_btn));

        btnPerformance.setText("PERFORMANCE");
        btnMoments.setText("MOMENTS");
        pref.putString("momentsOrPerformance","fromMoments");


        String fromWhere = getIntent().getStringExtra("from");

        if(fromWhere.equalsIgnoreCase("adapter")){

            next.setVisibility(View.VISIBLE);
            retake.setVisibility(View.GONE);
        }else{
            next.setVisibility(View.VISIBLE);
            retake.setVisibility(View.VISIBLE);
        }


        if(getIntent().getStringExtra("fromActivity")!=null){
            fromActivity = getIntent().getStringExtra("fromActivity");
            if(getIntent().getStringExtra("fromActivity").equalsIgnoreCase("challenges")){
                btnMoments.setVisibility(View.GONE);
                btnPerformance.setVisibility(View.GONE);
                challengeId = getIntent().getStringExtra("id");
            }else if(getIntent().getStringExtra("fromActivity").equalsIgnoreCase("adapter")){
                btnMoments.setVisibility(View.VISIBLE);
                btnPerformance.setVisibility(View.VISIBLE);
            }else{
                btnMoments.setVisibility(View.VISIBLE);
                btnPerformance.setVisibility(View.VISIBLE);
            }
        }

        /*DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) videoView.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;*/


        strVideoUrl = "https://androidwave.com/media/androidwave-video-exo-player.mp4";

        if(getIntent().getStringExtra("video")!=null){
            str_video = getIntent().getStringExtra("video");
            videoView.setFitsSystemWindows(true);

            //videoView.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            videoView.setVideoPath(str_video);
            videoView.start();
        }




        if(videoView.isPlaying()){

            imageView.setImageResource(R.drawable.ic_pausesign);

        }

        startTimerTimer(1000000);
        isPlay=true;


        updateSeekBar();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isPlay){

                    videoView.pause();
                    isPlay = false;
                    imageView.setImageResource(R.drawable.ic_playsign);
                    onPauseTimer();
                }else if(!isPlay){

                    videoView.start();
                    isPlay = true;
                    updateSeekBar();
                    resumeTimer();
                    imageView.setImageResource(R.drawable.ic_pausesign);
                }
            }
        });

        //updateCountDownStart();

        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(VideoPreviewActivity.this,CustomCameraActivity.class);
                in.putExtra("From",fromActivity);
                in.putExtra("id",challengeId);
                startActivity(in);
                finish();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(VideoPreviewActivity.this,PostTypeActivity.class);
                if(fromActivity.equalsIgnoreCase("challenges")){
                    in.putExtra("videoPath",str_video);
                    in.putExtra("id",challengeId);
                    in.putExtra("fromActivity",fromActivity);
                }else{
                    in.putExtra("videoPath",str_video);
                    in.putExtra("fromActivity",fromActivity);
                    in.putExtra("mOrp",pref.getString("momentsOrPerformance"));
                    Log.v("mOrP",pref.getString("momentsOrPerformance"));
                }
                startActivity(in);
                finish();

            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer vmp) {
                onPauseTimer();
                mTextViewCountdown.setText("00:00");
                counter = 0;
                isPlay = false;
                imageView.setImageResource(R.drawable.ic_playsign);
            }
        });

        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        btnMoments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnMoments.getBackground().getConstantState() == getResources().getDrawable( R.drawable.half_btn_bg_left_white).getConstantState())
                {

                    btnMoments.setBackground(getResources().getDrawable(R.drawable.half_button_bg));
                    btnPerformance.setBackground(getResources().getDrawable(R.drawable.half_right_bg_btn));
                    pref.putString("momentsOrPerformance","fromMoments");




                    //Toast.makeText(_con, "Image is ivPic", Toast.LENGTH_LONG).show();
                    // new RegisterAsyntaskNew().execute();
                }
                else
                {

                    /*viewAll.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_left_white));
                    viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_right_yellow));
                    getMyFeedFollowers();*/
                    /*Toast.makeText(_con, "Image isn't ivPic", Toast.LENGTH_LONG).show();*/
                    // new RegisterAsyntask().execute();

                }


            }
        });


        btnPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnPerformance.getBackground().getConstantState() == getResources().getDrawable( R.drawable.half_btn_bg_right_yellow).getConstantState())
                {
                    /*viewAll.setBackground(getResources().getDrawable(R.drawable.half_button_bg));
                    viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_right_bg_btn));*/
                    //Toast.makeText(_con, "Image is ivPic", Toast.LENGTH_LONG).show();
                    // new RegisterAsyntaskNew().execute();
                }
                else
                {

                    pref.putString("momentsOrPerformance","fromPerformance");
                    btnMoments.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_left_white));
                    btnPerformance.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_right_yellow));

                }
            }
        });

    }

    private void updateSeekBar() {

        handler.postDelayed(updateTimeTask,100);
        //startTimer();

    }

    public Runnable updateTimeTask = new Runnable() {
        @Override
        public void run() {
            seekbar.setProgress(videoView.getCurrentPosition());
            seekbar.setMax(videoView.getDuration());
            int testesffsddag = videoView.getDuration();
            int sdfasdf = videoView.getCurrentPosition();
            START_TIME_IN_MILLIS = videoView.getCurrentPosition();
            mTimeLeftInMills = START_TIME_IN_MILLIS;

            updateCountDownStart();






            handler.postDelayed(this,100);

            //startTimer();

            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    handler.removeCallbacks(updateTimeTask);
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    handler.removeCallbacks(updateTimeTask);
                    videoView.seekTo(seekBar.getProgress());
                    imageView.setImageResource(R.drawable.ic_playsign);
                    updateSeekBar();
                    //pauseTimer();
                }
            });
        }
    };


    private void startTimer(){

        mCountDownTimer = new CountDownTimer(mTimeLeftInMills,1000) {
            @Override
            public void onTick(long l) {

                mTimeLeftInMills = l;
                updateCountDownStart();
            }

            @Override
            public void onFinish() {
                mTimmerRunning = false;
            }
        }.start();

        mTimmerRunning = true;

    }

    private void pauseTimer(){

        mCountDownTimer.cancel();
        mTimmerRunning = false;


    }

    private void resetTimer(){

        mTimeLeftInMills = START_TIME_IN_MILLIS;
        updateCountDownStart();
    }


    private void updateCountDownStart(){

        int minutes = (int) (mTimeLeftInMills / 1000) / 60;
        int seconds = (int) (mTimeLeftInMills / 1000) % 60;

        String timeLeftFormated = String.format(Locale.getDefault(),"%02d:%02d", minutes,seconds);

        //mTextViewCountdown.setText(timeLeftFormated);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(VideoPreviewActivity.this, CameraViewNewActivity.class);
        in.putExtra("From",fromActivity);
        in.putExtra("fromActivity",fromActivity);
        if(getIntent().getStringExtra("fromActivity").equalsIgnoreCase("challenges")){
            in.putExtra("id",getIntent().getStringExtra("id"));
        }
        startActivity(in);
        finish();
    }


    private void onPauseTimer(){
        t.cancel();
    }

    private void resumeTimer(){

        startTimerTimer(timeInMillisec);

    }


    private void startTimerTimer(long mTimeLeftInMills){


        //updateCountDownStart();




        t = new CountDownTimer(mTimeLeftInMills,1000) {
            @Override
            public void onTick(long l) {
                timetime = l;
                counter++;
                timeInMillisec = l;
                String time = new Integer(counter).toString();

                long millis = counter;
                int seconds = (int) (millis / 60);
                int minutes = seconds / 60;
                seconds     = seconds % 60;

                mTextViewCountdown.setText(String.format("%02d:%02d", seconds,millis));
            }

            @Override
            public void onFinish() {
                //textView.setText("FINISH!!");
                //t.cancel();


                counter = 0;
                mTextViewCountdown.setText("00:00");
            }
        }.start();




    }


}


package com.lydor.mygame.callingnetwork;

import android.content.Context;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.utils.Constants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private Context mContext;
    private static SharedPref pref;
    private static String token;

    public static final String BASE_URL = "http://ec2-13-233-152-60.ap-south-1.compute.amazonaws.com/api/";


    public static Retrofit retrofit = null;


    static OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request newRequest  = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
            return chain.proceed(newRequest);
        }
    }).build();


    public static Retrofit getClient(Context context) {
        pref = new SharedPref(context);
        token = pref.getString(Constants.USER_TOKEN);
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit retrofit() {
        return retrofit;
    }
}

package com.lydor.mygame.fragmentss;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Movie;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.AnyRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activities.MainActivity;
import com.lydor.mygame.activities.VideoViewActivity;
import com.lydor.mygame.activity.BaseActivity;
import com.lydor.mygame.activity.CustomCameraActivity;
import com.lydor.mygame.activity.LoginActivity;

import com.lydor.mygame.adapter.MediaRecyclerAdapter;
import com.lydor.mygame.adapter.PlayerViewHolder;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.fragment.CameraFragment;
import com.lydor.mygame.fragment.CameraViewNewActivity;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.LoginRequest;
import com.lydor.mygame.model.LoginResponseNew;
import com.lydor.mygame.model.MyFeedList;
import com.lydor.mygame.model.MyFeedResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.ui.ExoPlayerRecyclerView;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.LinearLayout.VERTICAL;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MoviesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MoviesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MoviesFragment extends Fragment implements PlayerViewHolder.CallBack {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String baseVideoUrl = "https://mygame-app.s3.ap-south-1.amazonaws.com/videos/";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    private OnFragmentInteractionListener mListener;

    ImageView imv1;
    SharedPref preferences;
    private ProgressDialog mProgressDialog;
    ExoPlayerRecyclerView mRecyclerView;
    Button viewAll,viewFollowers;
    TextView tvPostVideo;

    private ArrayList<MyFeedList> mediaObjectList = new ArrayList<>();
    private MediaRecyclerAdapter mAdapter;
    private boolean firstTime = true;
    TextView errTv;
    private boolean fromFollowers = false;

    int currentPage,nextPage,previousPage,lastPage,currentPageMyFollowers,lastPageMyFollowers;
    private static final int REQUEST_PERMISSIONS = 100;

    public static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    public static final int REQUEST_CAMERA_CODE = 100;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;











    public MoviesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MoviesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MoviesFragment newInstance(String param1, String param2) {
        MoviesFragment fragment = new MoviesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movies, container, false);

        imv1 = view.findViewById(R.id.imv);
        tvPostVideo = view.findViewById(R.id.tvPostVideo);
        errTv = view.findViewById(R.id.errTv);
        //activity = new MainActivity();
        preferences = new SharedPref(getActivity());
        mProgressDialog = new ProgressDialog(getActivity());


        viewAll = view.findViewById(R.id.viewCamera);
        viewFollowers = view.findViewById(R.id.viewCamerads);

        viewAll.setBackground(getResources().getDrawable(R.drawable.half_button_bg));
        viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_right_bg_btn));

        viewFollowers.setText("FOLLOWING");
        viewAll.setText("VIEW ALL");







        mRecyclerView = view.findViewById(R.id.exoPlayerRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
//        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        imv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    //checkPermission(getActivity());
                Intent in = new Intent(getActivity(), CameraViewNewActivity.class);
                in.putExtra("From","post");
                startActivity(in);

            }
        });

        tvPostVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    //checkPermission(getActivity());
                //checkPermission();
                Intent in = new Intent(getActivity(), CameraViewNewActivity.class);
                in.putExtra("From","post");
                startActivity(in);




            }
        });


        mRecyclerView.setMediaObjects(mediaObjectList);
        mAdapter = new MediaRecyclerAdapter(mediaObjectList, initGlide(),getActivity(),this);

        //Set Adapter
        mRecyclerView.setAdapter(mAdapter);
        preferences.putBoolean("isScrolling",true);





        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                /*switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        //Toast.makeText(, "", Toast.LENGTH_SHORT).show();
                        Log.v("scrollEffect","The RecyclerView is not scrolling");

                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        //System.out.println("Scrolling now");
                        Log.v("scrollEffect","Scrolling now");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        //System.out.println("Scroll Settling");
                        Log.v("scrollEffect","Scroll Settling");
                        break;

                }*/

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    //Toast.makeText(getActivity(), "Last", Toast.LENGTH_LONG).show();
                    preferences.putBoolean("isScrolling",false);
                    if(fromFollowers){
                        if(currentPageMyFollowers!=lastPageMyFollowers){
                            getMyFeedFollowers(currentPageMyFollowers+1);
                        }else{
                            Toast.makeText(getActivity(), "No More Videos", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        if(currentPage!=lastPage){
                            Log.v("currentPage:",currentPage+"");
                            getMyFeed(currentPage+1);
                        }else{
                            Toast.makeText(getActivity(), "No More Videos", Toast.LENGTH_SHORT).show();
                        }

                    }

                }else if(newState==RecyclerView.SCROLL_STATE_IDLE){
                    preferences.putBoolean("isScrolling",false);
                }else if(newState == RecyclerView.SCROLL_STATE_DRAGGING || newState == RecyclerView.SCROLL_STATE_SETTLING){

                    preferences.putBoolean("isScrolling",true);
                }
            }
        });

         mediaObjectList.clear();

        getMyFeed(1);


        if (firstTime) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mRecyclerView.playVideo(true);
                }
            });
            firstTime = false;
        }

        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewAll.getBackground().getConstantState() == getResources().getDrawable( R.drawable.half_btn_bg_left_white).getConstantState())
                {

                    if(preferences.getBoolean("isScrolling")){

                    }else{
                        if(mRecyclerView!=null){
                            mRecyclerView.onPausePlayer();
                        }
                        viewAll.setBackground(getResources().getDrawable(R.drawable.half_button_bg));
                        viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_right_bg_btn));
                        mediaObjectList.clear();
                        //mAdapter.notifyDataSetChanged();
                        fromFollowers = false;
                        viewAll.setEnabled(false);
                        viewAll.setClickable(false);
                        viewFollowers.setClickable(false);
                        viewFollowers.setEnabled(false);
                        getMyFeed(1);
                    }





                    //Toast.makeText(_con, "Image is ivPic", Toast.LENGTH_LONG).show();
                    // new RegisterAsyntaskNew().execute();
                }
                else
                {

                    /*viewAll.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_left_white));
                    viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_right_yellow));
                    getMyFeedFollowers();*/
                    /*Toast.makeText(_con, "Image isn't ivPic", Toast.LENGTH_LONG).show();*/
                    // new RegisterAsyntask().execute();

                }


            }
        });


        viewFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewFollowers.getBackground().getConstantState() == getResources().getDrawable( R.drawable.half_btn_bg_right_yellow).getConstantState())
                {
                    /*viewAll.setBackground(getResources().getDrawable(R.drawable.half_button_bg));
                    viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_right_bg_btn));*/
                    //Toast.makeText(_con, "Image is ivPic", Toast.LENGTH_LONG).show();
                    // new RegisterAsyntaskNew().execute();
                }
                else
                {
                    if(preferences.getBoolean("isScrolling")){

                    } else{
                        if(mRecyclerView!=null){
                            mRecyclerView.onPausePlayer();
                        }

                        viewAll.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_left_white));
                        viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_right_yellow));
                        /*Toast.makeText(_con, "Image isn't ivPic", Toast.LENGTH_LONG).show();*/
                        // new RegisterAsyntask().execute();
                        mediaObjectList.clear();
                        //mAdapter.notifyDataSetChanged();
                        fromFollowers = true;
                        viewAll.setEnabled(false);
                        viewAll.setClickable(false);
                        viewFollowers.setClickable(false);
                        viewFollowers.setEnabled(false);
                        getMyFeedFollowers(1);
                    }



                }
            }
        });

        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private RequestManager initGlide() {
        RequestOptions options = new RequestOptions();

        return Glide.with(this)
                .setDefaultRequestOptions(options);
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCallMethod() {
        getMyFeedNew(1);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void getMyFeedNew(int pageNum){
        if (AppUtil.isInternetConnected(getActivity())) {
            //mProgressDialog= ProgressDialog.show(getActivity(),null,Constants.PLEASE_WAIT);
            viewAll.setBackground(getContext().getResources().getDrawable(R.drawable.half_button_bg));
            viewFollowers.setBackground(getContext().getResources().getDrawable(R.drawable.half_right_bg_btn));


            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);
            Log.v("pageNum:",pageNum+"");
            Call<MyFeedResponse> call = apiService.getMyFeedList(
                    pageNum);

            call.enqueue(new Callback<MyFeedResponse>() {
                @Override
                public void onResponse(Call<MyFeedResponse> call, Response<MyFeedResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }


                    Log.v("MyFeedResponse: ",response.body()+"");
                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        MyFeedResponse successresponse = response.body();
                        currentPage = 0;
                        lastPage = 0;
                        currentPage = successresponse.getCurrent_page();
                        lastPage = successresponse.getLast_page();

                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        if(successresponse.getData().size()>0){

                            errTv.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                            mediaObjectList.clear();
                            mediaObjectList.addAll(successresponse.getData());
                            mAdapter.notifyDataSetChanged();


                        }else{
                            errTv.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            errTv.setText("No videos posted");
                            mediaObjectList.clear();
                            mAdapter.notifyDataSetChanged();
                        }
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        preferences.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<MyFeedResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
                Toast.makeText(getContext(),Constants.CHECK_YOUR_INTERNET_CONNECTION,Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void getMyFeed(int pageNum){
        if (AppUtil.isInternetConnected(getActivity())) {
            //mProgressDialog= ProgressDialog.show(getActivity(),null,Constants.PLEASE_WAIT);
            viewAll.setBackground(getContext().getResources().getDrawable(R.drawable.half_button_bg));
            viewFollowers.setBackground(getContext().getResources().getDrawable(R.drawable.half_right_bg_btn));


            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);
            Log.v("pageNum:",pageNum+"");
            Call<MyFeedResponse> call = apiService.getMyFeedList(
                    pageNum);

            call.enqueue(new Callback<MyFeedResponse>() {
                @Override
                public void onResponse(Call<MyFeedResponse> call, Response<MyFeedResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }


                    Log.v("MyFeedResponse: ",response.body()+"");
                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        MyFeedResponse successresponse = response.body();
                        currentPage = 0;
                        lastPage = 0;
                        currentPage = successresponse.getCurrent_page();
                        lastPage = successresponse.getLast_page();

                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        if(successresponse.getData().size()>0){

                            errTv.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                            //mediaObjectList.clear();
                            mediaObjectList.addAll(successresponse.getData());
                            mAdapter.notifyDataSetChanged();


                        }else{
                            errTv.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            errTv.setText("No videos posted");
                            mediaObjectList.clear();
                            mAdapter.notifyDataSetChanged();
                        }

                        viewAll.setEnabled(true);
                        viewAll.setClickable(true);
                        viewFollowers.setClickable(true);
                        viewFollowers.setEnabled(true);
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        preferences.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<MyFeedResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
                Toast.makeText(getContext(),Constants.CHECK_YOUR_INTERNET_CONNECTION,Toast.LENGTH_SHORT).show();
            }

        }
    }



    @Override
    public void onDestroy() {
        if (mRecyclerView != null) {
            mRecyclerView.releasePlayer();
        }
        super.onDestroy();
    }



    public void getMyFeedFollowers(int pageNum){
        if (AppUtil.isInternetConnected(getActivity())) {
            //mProgressDialog= ProgressDialog.show(getActivity(),null,Constants.PLEASE_WAIT);



            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);

            Call<MyFeedResponse> call = apiService.getMyFeedListFollowing(
                    pageNum);
            call.enqueue(new Callback<MyFeedResponse>() {
                @Override
                public void onResponse(Call<MyFeedResponse> call, Response<MyFeedResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        MyFeedResponse successresponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();
                        currentPageMyFollowers = successresponse.getCurrent_page();
                        lastPageMyFollowers = successresponse.getLast_page();

                        if(successresponse.getData().size()>0){
                            errTv.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                            //mediaObjectList.clear();
                            mediaObjectList.addAll(successresponse.getData());
                            mAdapter.notifyDataSetChanged();


                        }else{
                            errTv.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            errTv.setText("No videos posted by your friends");
                            mediaObjectList.clear();
                            mAdapter.notifyDataSetChanged();
                        }

                        viewAll.setEnabled(true);
                        viewAll.setClickable(true);
                        viewFollowers.setClickable(true);
                        viewFollowers.setEnabled(true);
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        preferences.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<MyFeedResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(getActivity(), "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.v("VISIBLE:","YES");
            mediaObjectList.clear();
            getMyFeed(1);
            /*viewAll.setBackground(getContext().getResources().getDrawable(R.drawable.half_button_bg));
            viewFollowers.setBackground(getContext().getResources().getDrawable(R.drawable.half_right_bg_btn));*/

        } else {
            if(mRecyclerView!=null){
                mRecyclerView.onPausePlayer();
            }
            Log.v("VISIBLE:","GONE");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //getMyFeedPause(1);
        /*mRecyclerView = new ExoPlayerRecyclerView(getActivity(), null);
        mediaObjectList.clear();*/
        //mAdapter.notifyDataSetChanged();
        if(mRecyclerView!=null){

            mRecyclerView.onPausePlayer();
        }
    }


    public void onCallMethod(Context mContext){
        //getMyFeed(1);
    }


    public void testMethod(){
        //mRecyclerView.onPausePlayer();
        Toast.makeText(getActivity(), "Called", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        mRecyclerView.playVideo(true);
       /* mediaObjectList.clear();
        getMyFeed(1);*/
    }


    public void makeVideoPlayOff(){
        if(mRecyclerView!=null){
            mRecyclerView.controlVideoAudio();
        }

    }

    public void makeVideoPlayOn(){
        if(mRecyclerView!=null){
            mRecyclerView.makeVideoAudioOn();
        }

    }

    /*@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public  boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.
                    WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.
                            READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.
                            CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.
                            RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                        context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                            //Toast.makeText(context, "Checked", Toast.LENGTH_SHORT).show();
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                Intent in = new Intent(getActivity(), CustomCameraActivity.class);
                in.putExtra("From","post");
                startActivity(in);
                return true;
            }
        } else {
            //Do Nothing
            return true;
        }
    }*/


    protected void checkPermission(){
        if(ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.CAMERA)
                + ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.READ_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            // Do something, when permissions not granted
            if(ActivityCompat.shouldShowRequestPermissionRationale(
                    getActivity(),Manifest.permission.CAMERA)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    getActivity(),Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    getActivity(),Manifest.permission.RECORD_AUDIO) ){
                // If we should give explanation of requested permissions

                // Show an alert dialog here with request explanation
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Camera, Read Contacts, Write External and Audio Record" +
                        " Storage permissions are required to do the task.");
                builder.setTitle("Please grant those permissions");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermissions(
                                new String[]{
                                        Manifest.permission.CAMERA,
                                        Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        Manifest.permission.RECORD_AUDIO
                                },
                                MY_PERMISSIONS_REQUEST_CODE
                        );
                    }
                });
                builder.setNeutralButton("Cancel",null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }else{
                // Directly request for required permissions, without explanation
                requestPermissions(
                        new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.RECORD_AUDIO
                        },
                        MY_PERMISSIONS_REQUEST_CODE
                );
            }
        }else {
            // Do something, when permissions are already granted
            Intent in = new Intent(getActivity(), CustomCameraActivity.class);
            in.putExtra("From","post");
            startActivity(in);
            //Toast.makeText(getActivity(),"Permissions already granted",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CODE:{
                // When request is cancelled, the results array are empty
                if(
                        (grantResults.length >0) &&
                                (grantResults[0]
                                        + grantResults[1]
                                        + grantResults[2]
                                        + grantResults[3]
                                        == PackageManager.PERMISSION_GRANTED
                                )
                ){
                    // Permissions are granted
                    Intent in = new Intent(getActivity(), CustomCameraActivity.class);
                    in.putExtra("From","post");
                    startActivity(in);
                    Toast.makeText(getActivity(),"Permissions granted.",Toast.LENGTH_SHORT).show();
                }else {
                    // Permissions are denied
                    Toast.makeText(getActivity(),"Permissions denied.",Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }



}

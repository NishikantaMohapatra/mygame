package com.lydor.mygame.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activities.AboutUsActivity;
import com.lydor.mygame.activity.UserDetailsActivity;
import com.lydor.mygame.model.FollowersResponse;
import com.lydor.mygame.model.FollowingResponse;
import com.lydor.mygame.utils.Constants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FollowingListAdapter extends RecyclerView.Adapter<FollowingListAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private Context context;
    private List<FollowingResponse> testLists;

    EventListener eventListener;
    SharedPref pref;


    public interface EventListener{
        void postFollowUnfollow(String followersId,int status);
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public FollowingListAdapter(List<FollowingResponse> testLists, Context context, EventListener eventListener) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.testLists = testLists;
        this.eventListener = eventListener;
    }


    // inflates the row layout from xml when needed
    @Override
    public FollowingListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.followers_list_item,
                parent, false);
        FollowingListAdapter.ViewHolder viewHolder = new FollowingListAdapter.ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the view and textview in each row
    @Override
    public void onBindViewHolder(FollowingListAdapter.ViewHolder holder, final int position) {
        LinearLayout lnrLayContainer = holder.lnrLayContainer;
        final CircleImageView imageView = holder.imageView;
        TextView tvName = holder.tvName;
        TextView tvUserName = holder.tvUserName;
        Button btnFollowUnFollow = holder.btnFollowUnFollow;
        LinearLayout lnrLay = lnrLayContainer;
        View view = holder.viewId;
        pref = new SharedPref(context);
        final FollowingResponse followingResponse = testLists.get(position);


        view.setVisibility(View.GONE);

        tvName.setText(testLists.get(position).getName());
        if(testLists.get(position).getUser_tag()!=null){
            tvUserName.setText(testLists.get(position).getUser_tag());
        }else{
            tvUserName.setText("User Name");
        }


        if(testLists.get(position).getUser_image()!=null) {
            //holder.imv.setTag(liveStreamLists.get(position).getImage());
            Picasso.with(context).load("http://mygame-app.s3.ap-south-1.amazonaws.com/"+testLists.get(position).getUser_image())
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap,
                                                   Picasso.LoadedFrom from) {

                            imageView.setImageBitmap(bitmap);

                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            imageView.setImageDrawable(
                                    context.getResources().
                                            getDrawable(R.drawable.ic_user));

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        }
        else
        {
            imageView.setImageDrawable(
                    context.getResources().
                            getDrawable(R.drawable.ic_user)
            );

        }

        if(position==testLists.size()-1){


            view.setVisibility(View.GONE);
        }

        lnrLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(testLists.get(position).getUser_id() == pref.getInt(Constants.USER_ID)){

                    Intent in = new Intent(context, AboutUsActivity.class);
                    //in.putExtra("userId",String.valueOf(mediaObject.getUser_id()));
                    context.startActivity(in);
                }else{

                    Intent in = new Intent(context, UserDetailsActivity.class);
                    in.putExtra("userId",String.valueOf(testLists.get(position).getUser_id()));
                    context.startActivity(in);
                }
            }
        });


            btnFollowUnFollow.setBackground(context.getResources().getDrawable(R.drawable.yellow_bg_line));
            btnFollowUnFollow.setTextColor(context.getResources().getColor(R.color.yellowAppColoer));
            btnFollowUnFollow.setText("UNFOLLOW");


/*Intent in = new Intent(context, ChallengesDetailsActivity.class);
                in.putExtra("challengeID",testLists.get(position).getId());
                in.putExtra("videoUrl",testLists.get(position).getFile_name());
                in.putExtra("obj",myChallengesList);
                context.startActivity(in);*/
        btnFollowUnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    eventListener.postFollowUnfollow(String.valueOf(testLists.get(position).getUser_id()),0);


            }
        });


    }


    // total number of rows
    @Override
    public int getItemCount() {
        return testLists.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public LinearLayout lnrLayContainer;
        public CircleImageView imageView;
        public TextView tvName,tvUserName;
        public View viewId;
        public Button btnFollowUnFollow;

        public ViewHolder(View itemView) {
            super(itemView);
            lnrLayContainer = itemView.findViewById(R.id.lnrLayContainer);
            imageView = itemView.findViewById(R.id.imv);
            tvName = itemView.findViewById(R.id.tvName);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            viewId = itemView.findViewById(R.id.belowView);
            btnFollowUnFollow = itemView.findViewById(R.id.btnFollowUnFollow);

        }
    }

    // allows clicks events to be caught

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);


    }

    public Bitmap createVideoThumbNail(String path){
        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
    }




}

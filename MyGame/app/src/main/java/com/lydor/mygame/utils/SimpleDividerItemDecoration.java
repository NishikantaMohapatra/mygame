package com.lydor.mygame.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;


import com.lydor.mygame.R;

public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
    private Drawable mDivider;
    int inset;
    Context mContext;
    boolean isHorizontalLayout;
    int space;
    Rect outRect = new Rect();




    public SimpleDividerItemDecoration(Context context,int space,boolean isHorizontalLayout) {
        mDivider = context.getResources().getDrawable(R.drawable.line_divider);
        this.mContext = context;
        this.space = space;
        this.isHorizontalLayout = isHorizontalLayout;

    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {



        //inset = mContext.getResources().getDimensionPixelSize(R.dimen.your_margin_value);
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount-1; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();
            int left = child.getLeft() + 40;
            int right = child.getRight() - 40;

            //mDivider.setBounds(left, top, right, bottom);
            outRect.bottom=bottom;
            outRect.right=right;
            outRect.left=left;
            outRect.top=top;
            mDivider.setBounds(outRect);
            mDivider.draw(c);
        }
    }


}

package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.tabs.TabLayout;
import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activities.AboutUsActivity;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.fragment.MyProfileMyChallengesListActivity;
import com.lydor.mygame.fragment.MyProfileMyFeedVideoList;
import com.lydor.mygame.fragment.UserProfileAcceptedChallenges;
import com.lydor.mygame.fragment.UserProfileMyFeedVideo;
import com.lydor.mygame.fragment.UserProfileMyPerformance;
import com.lydor.mygame.model.FollowUnfollowRequest;
import com.lydor.mygame.model.FollowUnfollowResponse;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.OtherUserDetailsRequest;
import com.lydor.mygame.model.OtherUsersResponse;
import com.lydor.mygame.model.UserMyProfileResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDetailsActivity extends BaseActivity {


    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Button btnEditProfile;
    private ImageView imvBack;
    private TextView tvTotalPosts,tvTotalFollowers,tvTotalFollowing,tvName,tvBio,toolbarTitle;
    private ArrayList<OtherUsersResponse> userMyProfileResponse;
    private CircleImageView imv;
    private ProgressDialog mProgressDialog;
    private SharedPref pref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);


        viewPager = (ViewPager) findViewById(R.id.viewpager);


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        btnEditProfile = findViewById(R.id.btnEditProfile);
        imvBack = findViewById(R.id.imvBack);
        tvName = findViewById(R.id.tvName);
        tvBio = findViewById(R.id.tvBio);
        tvTotalPosts = findViewById(R.id.tvTotalPosts);
        tvTotalFollowers = findViewById(R.id.tvTotalFollowers);
        tvTotalFollowing = findViewById(R.id.tvTotalFollowing);
        toolbarTitle = findViewById(R.id.toolbarTitle);
        imv = findViewById(R.id.imv);


        pref = new SharedPref(this);


        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if(getIntent().getStringExtra("userId")!=null){
            String userId = getIntent().getStringExtra("userId");
            getUserProfile(getIntent().getStringExtra("userId"));
        }




        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btnEditProfile.getText().toString().equalsIgnoreCase("FOLLOW")){
                    postFollowUnfollowRequest(getIntent().getStringExtra("userId"),1);
                }else{
                    postFollowUnfollowRequest(getIntent().getStringExtra("userId"),0);
                }
            }
        });
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new UserProfileMyFeedVideo(), "MOMENTS");
        adapter.addFragment(new UserProfileMyPerformance(),"PERFORMANCE");
        adapter.addFragment(new UserProfileAcceptedChallenges(), "CHALLENGES");
        viewPager.setAdapter(adapter);
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void getUserProfile(String userId){


        if (AppUtil.isInternetConnected(UserDetailsActivity.this)) {
            enableDialog("Please wait...");

            OtherUserDetailsRequest request = new OtherUserDetailsRequest();
            request.setUser_id(userId);

            RestService apiService =
                    ApiClient.getClient(UserDetailsActivity.this).create(RestService.class);

            Call<ArrayList<OtherUsersResponse>> call = apiService.postGetOtherUserDetails(request);
            call.enqueue(new Callback<ArrayList<OtherUsersResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<OtherUsersResponse>> call, Response<ArrayList<OtherUsersResponse>> response) {


                    disableDialog();



                    if(response.code()==200){
                        userMyProfileResponse = response.body();
                        String userName = null;
                        String userBio= null;
                        int totalPost = 0;
                        int totalFollowers = 0;
                        int totoalFollowing = 0;
                        String userImage = null;
                        String userTag = null;
                        int iFollow = 0;
                        for(int i=0;i<response.body().size();i++){
                            userName = response.body().get(i).getName();
                            userBio = response.body().get(i).getUser_bio();
                            totalPost = response.body().get(i).getTotal_posts();
                            totalFollowers = response.body().get(i).getTotal_followers();
                            totoalFollowing = response.body().get(i).getTotal_following();
                            userImage = response.body().get(i).getUser_image();
                            iFollow = response.body().get(i).getiFollow();
                            userTag = response.body().get(i).getUser_tag();

                        }

                       // Toast.makeText(UserDetailsActivity.this, ""+userName, Toast.LENGTH_SHORT).show();

                        if(iFollow==0){
                            btnEditProfile.setText("FOLLOW");
                        }else{
                            btnEditProfile.setText("UNFOLLOW");
                        }
                        if(userMyProfileResponse!=null){


                            if(userTag!=null){
                                tvName.setText("@"+userTag);
                            }else{
                                tvName.setText("User doesn't have a unique user tag");
                            }

                            if(userBio!=null){
                                tvBio.setText(userBio);
                            }else{
                                tvBio.setText("No bio till yet saved");
                            }

                            tvTotalPosts.setText(totalPost+"");
                            tvTotalFollowers.setText(totoalFollowing+"");
                            tvTotalFollowing.setText(totalFollowers+"");
                            toolbarTitle.setText(userName);


                            if(userImage!=null) {
                                //holder.imv.setTag(liveStreamLists.get(position).getImage());
                                Picasso.with(UserDetailsActivity.this).load("http://mygame-app.s3.ap-south-1.amazonaws.com/"+userImage)
                                        .into(new Target() {
                                            @Override
                                            public void onBitmapLoaded(Bitmap bitmap,
                                                                       Picasso.LoadedFrom from) {

                                                imv.setImageBitmap(bitmap);

                                            }

                                            @Override
                                            public void onBitmapFailed(Drawable errorDrawable) {
                                                imv.setImageDrawable(
                                                        getResources().
                                                                getDrawable(R.drawable.ic_user));

                                            }

                                            @Override
                                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                                            }
                                        });
                            }
                            else
                            {
                                imv.setImageDrawable(
                                        getResources().
                                                getDrawable(R.drawable.ic_user)
                                );

                            }

                            setupViewPager(viewPager);

                        }
                    }else {


                        if (response != null && response.errorBody() != null) {
                            //LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(UserDetailsActivity.this, response.message()+"", Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }


                }

                @Override
                public void onFailure(Call<ArrayList<OtherUsersResponse>> call, Throwable t) {

                    disableDialog();

                    Toast.makeText(UserDetailsActivity.this, ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            disableDialog();
            Toast.makeText(UserDetailsActivity.this,
                    "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }

    }




    public void postFollowUnfollowRequest(String followId,int status){
        if (AppUtil.isInternetConnected(UserDetailsActivity.this)) {
            mProgressDialog= ProgressDialog.show(UserDetailsActivity.this,null, Constants.PLEASE_WAIT);

            FollowUnfollowRequest request = new FollowUnfollowRequest();
            request.setFollowing_id(followId);
            request.setFollow(status);

            RestService apiService =
                    ApiClient.getClient(UserDetailsActivity.this).create(RestService.class);

            Call<FollowUnfollowResponse> call = apiService.getFollowUnfollow(request
            );
            call.enqueue(new Callback<FollowUnfollowResponse>() {
                @Override
                public void onResponse(Call<FollowUnfollowResponse> call, Response<FollowUnfollowResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(UserDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        FollowUnfollowResponse followUnfollowResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();
                        //addFrnds.setVisibility(View.GONE);

                        if(btnEditProfile.getText().toString().equalsIgnoreCase("FOLLOW")){
                            btnEditProfile.setText("UNFOLLOW");
                        }else{
                            btnEditProfile.setText("FOLLOW");
                        }

                    }else if(response.code()==401){
                        Toast.makeText(UserDetailsActivity.this, "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(UserDetailsActivity.this, LoginActivity.class);
                        UserDetailsActivity.this.startActivity(in);
                        pref.logout();
                        finish();
                    }


                }

                @Override
                public void onFailure(Call<FollowUnfollowResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(UserDetailsActivity.this, ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(UserDetailsActivity.this, "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }


    public String sendData() {
        return getIntent().getStringExtra("userId");
    }
}

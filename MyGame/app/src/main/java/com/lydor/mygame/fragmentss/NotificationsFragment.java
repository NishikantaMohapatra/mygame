package com.lydor.mygame.fragmentss;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activity.ChallengesDetailsActivity;
import com.lydor.mygame.activity.LoginActivity;
import com.lydor.mygame.adapter.ChallengesListingAdapter;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.MyChallengesList;
import com.lydor.mygame.model.MyChallengesResponse;
import com.lydor.mygame.model.MyFeedResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NotificationsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NotificationsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotificationsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RecyclerView rcv;
    ArrayList<MyChallengesList> mList;
    ChallengesListingAdapter mAdapter;
    private ProgressDialog mProgressDialog;
    private OnFragmentInteractionListener mListener;
    SharedPref pref;
    Button viewAll,viewFollowers;
    TextView errTv;

    public NotificationsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotificationsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationsFragment newInstance(String param1, String param2) {
        NotificationsFragment fragment = new NotificationsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_notifications, container, false);


       rcv = view.findViewById(R.id.challengesListingRcv);
        viewAll = view.findViewById(R.id.viewCamera);
        viewFollowers = view.findViewById(R.id.viewCamerads);
        errTv = view.findViewById(R.id.errTv);

        viewAll.setBackground(getResources().getDrawable(R.drawable.half_button_bg));
        viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_right_bg_btn));


        viewFollowers.setText("PREVIOUS");
        viewAll.setText("ACTIVE");
        mProgressDialog = new ProgressDialog(getActivity());
        pref = new SharedPref(getActivity());
        mList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rcv.setLayoutManager(linearLayoutManager);
        mAdapter = new ChallengesListingAdapter(mList,getActivity());
        rcv.setAdapter(mAdapter);
        pref.putBoolean("isActive",true);
        getChallengesListing();

        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewAll.getBackground().getConstantState() == getResources().getDrawable( R.drawable.half_btn_bg_left_white).getConstantState())
                {

                    viewAll.setBackground(getResources().getDrawable(R.drawable.half_button_bg));
                    viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_right_bg_btn));
                    getChallengesListing();
                    pref.putBoolean("isActive",true);



                    //Toast.makeText(_con, "Image is ivPic", Toast.LENGTH_LONG).show();
                    // new RegisterAsyntaskNew().execute();
                }
                else
                {

                    /*viewAll.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_left_white));
                    viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_right_yellow));
                    getMyFeedFollowers();*/
                    /*Toast.makeText(_con, "Image isn't ivPic", Toast.LENGTH_LONG).show();*/
                    // new RegisterAsyntask().execute();

                }


            }
        });


        viewFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewFollowers.getBackground().getConstantState() == getResources().getDrawable( R.drawable.half_btn_bg_right_yellow).getConstantState())
                {
                    /*viewAll.setBackground(getResources().getDrawable(R.drawable.half_button_bg));
                    viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_right_bg_btn));*/
                    //Toast.makeText(_con, "Image is ivPic", Toast.LENGTH_LONG).show();
                    // new RegisterAsyntaskNew().execute();
                }
                else
                {


                    viewAll.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_left_white));
                    viewFollowers.setBackground(getResources().getDrawable(R.drawable.half_btn_bg_right_yellow));
                    /*Toast.makeText(_con, "Image isn't ivPic", Toast.LENGTH_LONG).show();*/
                    // new RegisterAsyntask().execute();

                    getPastChallenges();
                    pref.putBoolean("isActive",false);
                }
            }
        });


       return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void getChallengesListing(){
        if (AppUtil.isInternetConnected(getActivity())) {
            //mProgressDialog= ProgressDialog.show(getActivity(),null, Constants.PLEASE_WAIT);



            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);

            Call<MyChallengesResponse> call = apiService.getChallengesListing(
                    1);
            call.enqueue(new Callback<MyChallengesResponse>() {
                @Override
                public void onResponse(Call<MyChallengesResponse> call, Response<MyChallengesResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        MyChallengesResponse myChallengesResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        if(myChallengesResponse.getData().size()>0){
                            errTv.setVisibility(View.GONE);
                            rcv.setVisibility(View.VISIBLE);
                            mList.clear();
                            mList.addAll(myChallengesResponse.getData());
                            mAdapter.notifyDataSetChanged();


                        }else{
                            errTv.setVisibility(View.VISIBLE);
                            rcv.setVisibility(View.GONE);
                            errTv.setText("No active challenges");
                            mList.clear();
                            mAdapter.notifyDataSetChanged();

                        }
                    }else if(response.code()==401){
                        //Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<MyChallengesResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(getActivity(), "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }

    public void getPastChallenges(){
        if (AppUtil.isInternetConnected(getActivity())) {
            //mProgressDialog= ProgressDialog.show(getActivity(),null, Constants.PLEASE_WAIT);



            RestService apiService =
                    ApiClient.getClient(getActivity()).create(RestService.class);

            Call<MyChallengesResponse> call = apiService.getPastChallengeListing(
                    1);
            call.enqueue(new Callback<MyChallengesResponse>() {
                @Override
                public void onResponse(Call<MyChallengesResponse> call, Response<MyChallengesResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        MyChallengesResponse myChallengesResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        if(myChallengesResponse.getData().size()>0){

                            errTv.setVisibility(View.GONE);
                            rcv.setVisibility(View.VISIBLE);
                            mList.clear();
                            mList.addAll(myChallengesResponse.getData());
                            mAdapter.notifyDataSetChanged();


                        }else{
                            errTv.setVisibility(View.VISIBLE);
                            rcv.setVisibility(View.GONE);
                            errTv.setText("No previous challenges");
                            mList.clear();
                            mAdapter.notifyDataSetChanged();
                        }
                    }else if(response.code()==401){
                        Toast.makeText(getActivity(), "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        getActivity().finish();
                    }


                }

                @Override
                public void onFailure(Call<MyChallengesResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(getActivity(), "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }
}

package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.lydor.mygame.R;
import com.lydor.mygame.activities.Activity_galleryview;
import com.lydor.mygame.activities.MyCameraSurfaceView;
import com.lydor.mygame.activities.ShowCamera;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import me.tankery.lib.circularseekbar.CircularSeekBar;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.CAMERA;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class CustomCameraActivity extends Activity {

    private Camera myCamera;
    private MyCameraSurfaceView myCameraSurfaceView;
    private MediaRecorder mediaRecorder;
    TextView videoBottomLabel;

    Button myButton;
    SurfaceHolder surfaceHolder;
    boolean recording;
    String videoPath;
    CircularSeekBar seekBar;
    ImageView switchCamera;
    private boolean cameraFront = false;
    private CircleImageView galleryVideo;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int REQUEST_PERMISSIONS = 100;

    CountDownTimer t;
    int counter;
    TextView countTimer;
    int pStatus;
    private Handler handler = new Handler();
    String from,challengeId;
    ImageView imvBack;
    TextView labelHeading;
    boolean videoIsCanceld = false;



    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        recording = false;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_custom_camera);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);





        labelHeading = findViewById(R.id.labelHeading);
        videoBottomLabel = findViewById(R.id.videoBottomLabel);

        if(getIntent().getStringExtra("From")!=null){
           from = getIntent().getStringExtra("From");
            if(from.equalsIgnoreCase("challenges")){
                labelHeading.setText("CREATE CHALLENGE");
                challengeId = getIntent().getStringExtra("id");
            }else{
                labelHeading.setText("CREATE VIDEO");
            }
        }



        //getSupportActionBar().hide();


        //fn_checkpermission();



        //Get Camera for preview
        /*if(checkPermission()){
            myCamera = getCameraInstance();
        }else{
            requestPermission();
            myCamera = getCameraInstance();
        }*/

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {Manifest.permission.CAMERA}, 1);
            }
        }*/




        myCamera = getCameraInstance();
        myCameraSurfaceView = new MyCameraSurfaceView(this, myCamera);
        FrameLayout myCameraPreview = (FrameLayout) findViewById(R.id.videoview);
        mediaRecorder = new MediaRecorder();


        /*Point displayDim = getDisplayWH();
        Point layoutPreviewDim = calcCamPrevDimensions(displayDim,
                myCameraSurfaceView.getOptimalPreviewSize(myCameraSurfaceView.mSupportedPreviewSizes,
                        displayDim.x, displayDim.y));
        if (layoutPreviewDim != null) {
            RelativeLayout.LayoutParams layoutPreviewParams =
                    (RelativeLayout.LayoutParams) myCameraPreview.getLayoutParams();
            layoutPreviewParams.width = layoutPreviewDim.x;
            layoutPreviewParams.height = layoutPreviewDim.y;
            layoutPreviewParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            myCameraPreview.setLayoutParams(layoutPreviewParams);
        }*/

        myCameraPreview.addView(myCameraSurfaceView);
        if (myCamera == null) {
            Toast.makeText(CustomCameraActivity.this,
                    "Fail to get Camera",
                    Toast.LENGTH_LONG).show();
        }


        myCamera.stopSmoothZoom();

        myButton = (Button) findViewById(R.id.mybutton);
        seekBar = findViewById(R.id.seekBar);
        switchCamera = findViewById(R.id.switchCamera);
        galleryVideo = findViewById(R.id.galleryVideo);
        countTimer = findViewById(R.id.countTimer);
        myButton.setOnClickListener(myButtonOnClickListener);
        imvBack = findViewById(R.id.imvBack);


        seekBar.setProgress(0);   // Main Progress
        // Secondary Progress
        seekBar.setMax(2000); // Maximum Progress


        switchCamera.setVisibility(View.VISIBLE);

        switchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int camerasNumber = Camera.getNumberOfCameras();
                if (camerasNumber > 1) {
                    //release the old camera instance
                    //switch camera, from the front and the back and vice versa

                    releaseCamera();
                    chooseCamera();
                } else {

                }

                myCamera.startPreview();

            }
        });

        galleryVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(CustomCameraActivity.this,
                        GalleryActivity.class);
                startActivity(in);
            }
        });


        //seekBar.setMax(18);

        t = new CountDownTimer(18000,1000) {
            @Override
            public void onTick(long l) {
                counter++;


                String time = new Integer(counter).toString();

                long millis = counter;
                int seconds = (int) (millis / 60);
                int minutes = seconds / 60;
                seconds     = seconds % 60;

                countTimer.setText(String.format("%02d:%02d", seconds,millis));
            }

            @Override
            public void onFinish() {
                //textView.setText("FINISH!!");
                //t.cancel();
                try{

                    mediaRecorder.stop();
                }
                catch (Exception e){

                }
                  // stop the recording
                releaseMediaRecorder(); // release the MediaRecorder object
                //myButton.setText("REC");
                myButton.setBackground(getResources().getDrawable(R.drawable.ellipse_4));
                recording = false;

                if(videoIsCanceld){}else{
                    Intent in = new Intent(CustomCameraActivity.this, VideoPreviewActivity.class);
                    if(from.equalsIgnoreCase("challenges")){
                        in.putExtra("video",videoPath);
                        in.putExtra("from","custom");
                        in.putExtra("id",challengeId);
                        in.putExtra("fromActivity",from);
                    }else{
                        in.putExtra("video",videoPath);
                        in.putExtra("from","custom");
                        in.putExtra("fromActivity",from);
                    }
                    startActivity(in);
                    finish();
                }

                seekBar.setProgress(0);
                counter = 0;
            }
        };

        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoIsCanceld = true;
                onDestroy();

                finish();
            }
        });



    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    private Point getDisplayWH() {

        Display display = this.getWindowManager().getDefaultDisplay();
        Point displayWH = new Point();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(displayWH);
            return displayWH;
        }
        displayWH.set(display.getWidth(), display.getHeight());
        return displayWH;
    }


    public void onSurfaceChangesCamera(){
        videoIsCanceld = true;
        onDestroy();


    }


    Button.OnClickListener myButtonOnClickListener
            = new Button.OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (recording) {
                // stop recording and release camera
                try{

                    mediaRecorder.stop();
                }
                catch (Exception e){

                } // stop the recording
                releaseMediaRecorder(); // release the MediaRecorder object
                //myButton.setText("REC");
                myButton.setBackground(getResources().getDrawable(R.drawable.ellipse_4));
                recording = false;
                videoBottomLabel.setText("Video");
                Intent in = new Intent(CustomCameraActivity.this, VideoPreviewActivity.class);
                if(from.equalsIgnoreCase("challenges")){
                    in.putExtra("video",videoPath);
                    in.putExtra("from","custom");
                    in.putExtra("id",challengeId);
                    in.putExtra("fromActivity",from);
                }else{
                    in.putExtra("video",videoPath);
                    in.putExtra("from","custom");
                    in.putExtra("fromActivity",from);
                }


                startActivity(in);
                finish();
                seekBar.setProgress(0);
                counter = 0;
                t.cancel();
                switchCamera.setVisibility(View.VISIBLE);


                //Exit after saved
                //finish();
            } else {

                //Release Camera before MediaRecorder start
                switchCamera.setVisibility(View.GONE);
                releaseCamera();
                //myCameraSurfaceView.surfaceDestroyed(surfaceHolder);
                if (!prepareMediaRecorder()) {
                    Toast.makeText(CustomCameraActivity.this,
                            "Fail in prepareMediaRecorder()!\n - Ended -",
                            Toast.LENGTH_LONG).show();
                    //finish();
                }


                    //seekBar.setProgress(18);

                try{
                    mediaRecorder.start();
                }catch (Exception e){
                    Toast.makeText(CustomCameraActivity.this, ""+e.toString(), Toast.LENGTH_SHORT).show();
                }

                recording = true;
                videoBottomLabel.setText("Tap to stop recording");
                t.start();
                //myButton.setText("STOP");
                myButton.setBackground(getResources().getDrawable(R.drawable.rounded_rectangle_2_copy));
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        while (pStatus < 2000) {
                            pStatus += 1;

                            handler.post(new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    seekBar.setProgress(pStatus);
                                    //tv.setText(pStatus + "%");

                                }
                            });
                            try {
                                // Sleep for 200 milliseconds.
                                // Just to display the progress slowly
                                Thread.sleep(8); //thread will take approx 1.5 seconds to finish
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();
            }
        }
    };

    private Camera getCameraInstance() {
        // TODO Auto-generated method stub
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    private boolean prepareMediaRecorder() {
        myCamera = getCameraInstance();

        mediaRecorder = new MediaRecorder();
        if(cameraFront){
            myCamera.release();
            myCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            myCamera.setDisplayOrientation(90);
            mediaRecorder.setOrientationHint(270);
            
        }else{
            //myCamera.release();
            myCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
            myCamera.setDisplayOrientation(90);
            mediaRecorder.setOrientationHint(90);
        }



        mediaRecorder.setPreviewDisplay(myCameraSurfaceView.getHolder().getSurface());

        myCamera.unlock();





            mediaRecorder.setCamera(myCamera);






        String filename;
        String path;

        path= Environment.getExternalStorageDirectory().getAbsolutePath().toString();

        Date date=new Date();
        filename="/rec"+date.toString().replace(" ", "_").replace(":", "_")+".mp4";

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);



        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
        //mediaRecorder.setVideoEncodingBitRate(690000 );
        if(cameraFront){
            String deviceMan = android.os.Build.MANUFACTURER;
            if(deviceMan.equalsIgnoreCase("OnePlus")){
                //mediaRecorder.setVideoSize(320, 200);
                mediaRecorder.setVideoFrameRate(16);
            }else{

            }

        }

        /*mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);// MPEG_4_SP
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);*/
        mediaRecorder.setOutputFile(path+filename);
        //myCamera.setDisplayOrientation(90);
        videoPath = path+filename;

        //mediaRecorder.setOutputFile("/sdcard/myvideo.mp4");

        /*mediaRecorder.setMaxDuration(180000); // Set max duration 60 sec.
        mediaRecorder.setMaxFileSize(5000000); // Set max file size 5M*/

        //myCamera.setDisplayOrientation(90);
        mediaRecorder.setPreviewDisplay(myCameraSurfaceView.getHolder().getSurface());

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;

    }



    @Override
    protected void onResume() {
        super.onResume();
        videoIsCanceld = false;
        if(checkPermission()){


        if(myCamera == null) {
            if(checkPermission()){


            myCamera = Camera.open();
            myCamera.setDisplayOrientation(90);
            myCameraSurfaceView.refreshCamera(myCamera);
            Log.d("nu", "null");

            }else{
                requestPermission();
            }
        }else {
            Log.d("nu","no null");
        }

        }else{
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoIsCanceld = true;
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            if(recording){
                //myCamera.lock();
            }
                  // lock camera for later use
        }
    }

    private void releaseCamera() {
        //mediaRecorder.setCamera(myCamera);
        if (myCamera != null) {
            //myCamera.stopPreview();
            myCamera.release();        // release the camera for other applications
            myCamera = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

            try{
                mediaRecorder.stop();
            }catch (Exception e){
                //Toast.makeText(CustomCameraActivity.this,e.toString(),Toast.LENGTH_SHORT).show();
            }
             // stop the recording
            releaseMediaRecorder();


        //myCameraSurfaceView.surfaceDestroyed(surfaceHolder);
        if(myCamera!=null){
            myCamera.lock();
            myCamera.stopPreview();
            myCamera.setPreviewCallback(null);
            myCamera.release();
            myCamera = null;
        }

        if(myCameraSurfaceView != null){
            myCameraSurfaceView.destroyDrawingCache();
            myCameraSurfaceView.mCamera = null;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        videoIsCanceld = true;
        onDestroy();

        finish();
    }


    public void chooseCamera() {
        //if the camera preview is the front
        if (cameraFront) {
            int cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview

                myCamera = Camera.open(cameraId);
                myCamera.setDisplayOrientation(90);
                myCameraSurfaceView.refreshCamera(myCamera);
            }
        } else {
            int cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview
                myCamera = Camera.open(cameraId);
                myCamera.setDisplayOrientation(90);
                myCameraSurfaceView.refreshCamera(myCamera);
            }
        }



    }

    private int findFrontFacingCamera() {

        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;

    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;

            }

        }
        return cameraId;
    }

    private void fn_checkpermission(){
        /*RUN TIME PERMISSIONS*/


    }


    private Point calcCamPrevDimensions(Point disDim, Camera.Size camDim) {

        Point displayDim = disDim;
        Camera.Size cameraDim = camDim;

        double widthRatio = (double) displayDim.x / cameraDim.width;
        double heightRatio = (double) displayDim.y / cameraDim.height;

        // use ">" to zoom preview full screen
        if (widthRatio < heightRatio) {
            Point calcDimensions = new Point();
            calcDimensions.x = displayDim.x;
            calcDimensions.y = (displayDim.x * cameraDim.height) / cameraDim.width;
            return calcDimensions;
        }
        // use "<" to zoom preview full screen
        if (widthRatio > heightRatio) {
            Point calcDimensions = new Point();
            calcDimensions.x = (displayDim.y * cameraDim.width) / cameraDim.height;
            calcDimensions.y = displayDim.y;
            return calcDimensions;
        }
        return null;
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "You have denied the permission...", Toast.LENGTH_SHORT).show();
            onDestroy();
            return false;
        }
        return true;
    }


    private void requestPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
                    myCamera = Camera.open();
                    myCamera.setDisplayOrientation(90);
                    myCameraSurfaceView.refreshCamera(myCamera);
                    // main logic
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(CustomCameraActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }



}




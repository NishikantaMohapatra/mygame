package com.lydor.mygame.utils;

public class Constants {

    public static final String URL_LOGIN = "auth/login";
    public static final String URL_REGISTERATION = "auth/signup";
    public static final String URL_GET_MYFEED = "video/feeds";
    public static final String URL_GET_MYFEED_FOLLOWERES = "video/following";
    public static final String URL_POST_VIDEO_FEED = "feeds";
    public static final String URL_MYCHALLENGES_LISTING = "challenges";
    public static final String URL_MYACTIVE_CHALLENGES = "challenges/active";
    public static final String URL_MYPAST_CHALLENGES = "challenges/past";
    public static final String URL_ACCEPTED_CHALLENGES_LIST = "challenge";
    public static final String URL_VIDEO_LIKE_UNLIKE = "video/like";
    public static final String URL_GET_USERS = "user";
    public static final String URL_GET_FOLLOWERS_LIST = "user/followers";
    public static final String URL_GET_FOLLOWING_LIST = "user/following";
    public static final String URL_MYPROFILE_MYVIDEO = "user/videos";
    public static final String URL_MYPROFILE_MYCHALLENGES = "user/challenges/accepted";
    public static final String URL_UPDATE_MYPROFILE = "user/profile";
    public static final String URL_FOLLOW_UNFOLLOW = "user/follow";
    public static final String URL_REPORT_A_PROBLEM = "issues/log ";
    public static final String URL_SEARCH_USER = "user/search";
    public static final String URL_ABOUT_US = "aboutus";
    public static final String URL_PRIVACY_POLICY = "privacypolicy";
    public static final String URL_FORGET_PASSWORD = "auth/resetpassword";
    public static final String URL_GET_USER_PROFILE_MOMENTS ="video/moments";
    public static final String URL_GET_USER_PROFILE_PERFORMANCE = "video/performance";




    public static final String NO_INTERNET = "Please check your Internet connection!!!";
    public static final String ENTER_YOUR_NAME = "Please enter your name";
    public static final String ENTER_YOUR_EMAIL = "Please enter email";
    public static final String ENTER_VALID_EMAIL = "Please enter a valid email";
    public static final String ENTER_MOBILE_NUMBER = "Please enter your Mobile number";
    public static final String ENTER_10_DIG_MOBILE_NUMBER = "Please enter a 10 digit Mobile number";
    public static final String ENTER_A_PASSWORD = "Please enter a password";
    public static final String CONFIRM_PASSWORD = "Please confirm your password by re-entering the password";
    public static final String CHECK_YOUR_INTERNET_CONNECTION = "PLease check your internet connection!!!";
    public static final String BOTH_PASS_MATCHES = "Password doesn't matches. Make sure both password is same";
    public static final String PLEASE_WAIT = "Please wait...";

    public static final String LOGIN_STATUS = "login_status";
    public static final String USER_TOKEN = "user_token";
    public static final String USER_NAME = "user_name";
    public static final String USER_ID = "user_id";

    public static final String FROM_MENU = "from_menu";
    public static final String SOME_THING_WRONG = "Something went wrong!!!";

    public static final String IS_RESUME = "video_player_resumed";

}

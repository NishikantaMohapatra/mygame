package com.lydor.mygame.networkcalling;


import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by HP on 8/19/2017.
 */

public class RestServiceBuilder
{

    private Context mContext;

    //52.172.183.131
    public static final String BASE_URL ="http://apoxeo.net/mygame/api/";
    //",192.168.1.119http://43.239.200.180";


    private static RestServiceBuilder restServiceBuilder;
    private RestService restService;
   // private IBugService bugService;
    //read  timeout in seconds
    private static final int READ_TIMEOUT = 70;

    //CONS

    private RestServiceBuilder(Context mContext) {
        this.mContext = mContext;
    }


    //STATIC

    public static RestServiceBuilder get(Context mContext)
    {

        if (restServiceBuilder == null) {
            restServiceBuilder = new RestServiceBuilder(mContext);


        }
        return restServiceBuilder;

    }


    public RestService getRestService() {
        if(restService !=null)
        {

            return restService;
        }
        else {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            /*final String aa = Credentials.basic(RegPrefManager.getInstance(mContext).getUsername(),
                    RegPrefManager.getInstance(mContext).getPassword());*/


            //Create a new Interceptor.
            Interceptor headerAuthorizationInterceptor = new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    okhttp3.Request request = chain.request();
                    //Headers headers = request.headers().newBuilder().add("Authorization", aa).build();
                    request = request.newBuilder().build();
                    return chain.proceed(request);
                }
            };



            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)

                    .addInterceptor(headerAuthorizationInterceptor)
                    .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                    .connectTimeout(70, TimeUnit.SECONDS)

                    .build();

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
//                    .registerTypeAdapter(Date.class, new JsonDateDeserializer())
//                    .registerTypeAdapter(Date.class, new JsonDateSerializer())
                    .create();

            Retrofit retrofit = new Retrofit.Builder()

                    .baseUrl("http://apoxeo.net/mygame/api/")

                    .client(client)

                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addConverterFactory(
                            GsonConverterFactory.create(
                                    new GsonBuilder()
                                            .serializeNulls()
                                            .excludeFieldsWithModifiers(
                                                    Modifier.FINAL,
                                                    Modifier.TRANSIENT,
                                                    Modifier.STATIC)
                                            .create()))
                    .build();

            restService = retrofit.create(RestService.class);
            return restService;
        }
    }

}

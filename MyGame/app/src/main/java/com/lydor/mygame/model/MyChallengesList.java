package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MyChallengesList implements Parcelable {

    private int id;
    private String title;
    private String description;
    private int user_id;
    private String video_id;
    private String created_at;
    private String updated_at;
    private String deleted_at;
    private String file_name;
    private String video_thumbnail;
    private int total_views;
    private int total_likes;

    public MyChallengesList() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }

    public int getTotal_views() {
        return total_views;
    }

    public void setTotal_views(int total_views) {
        this.total_views = total_views;
    }

    public int getTotal_likes() {
        return total_likes;
    }

    public void setTotal_likes(int total_likes) {
        this.total_likes = total_likes;
    }

    public static Creator<MyChallengesList> getCREATOR() {
        return CREATOR;
    }

    protected MyChallengesList(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        user_id = in.readInt();
        video_id = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        deleted_at = in.readString();
        file_name = in.readString();
        video_thumbnail = in.readString();
        total_views = in.readInt();
        total_likes = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeInt(user_id);
        dest.writeString(video_id);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(deleted_at);
        dest.writeString(file_name);
        dest.writeString(video_thumbnail);
        dest.writeInt(total_views);
        dest.writeInt(total_likes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MyChallengesList> CREATOR = new Creator<MyChallengesList>() {
        @Override
        public MyChallengesList createFromParcel(Parcel in) {
            return new MyChallengesList(in);
        }

        @Override
        public MyChallengesList[] newArray(int size) {
            return new MyChallengesList[size];
        }
    };
}

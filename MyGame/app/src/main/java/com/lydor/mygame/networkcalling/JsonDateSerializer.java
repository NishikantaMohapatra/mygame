package com.lydor.mygame.networkcalling;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lydor.mygame.utils.DateTimeUtil;


import java.lang.reflect.Type;
import java.util.Date;

public class JsonDateSerializer
    implements JsonSerializer<Date> {

    public static final String TAG=JsonDateSerializer.class.getSimpleName();
    @Override
    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context)
    {

        return src == null ? null : new JsonPrimitive(DateTimeUtil.get(src.getTime()));
    }


}
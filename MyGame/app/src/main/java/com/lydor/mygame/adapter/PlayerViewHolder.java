package com.lydor.mygame.adapter;


import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.FileUtils;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activities.AboutUsActivity;
import com.lydor.mygame.activity.LoginActivity;
import com.lydor.mygame.activity.PostTypeActivity;
import com.lydor.mygame.activity.UserDetailsActivity;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.fragmentss.MoviesFragment;
import com.lydor.mygame.model.FollowUnfollowRequest;
import com.lydor.mygame.model.FollowUnfollowResponse;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.MyFeedList;
import com.lydor.mygame.model.PostVideoMyFeedResponse;
import com.lydor.mygame.model.VideoLikeUnLikeRequest;
import com.lydor.mygame.model.VideoLikeUnlikeResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wseemann.media.FFmpegMediaMetadataRetriever;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created on : May 24, 2019
 * Author     : AndroidWave
 */
public class PlayerViewHolder extends RecyclerView.ViewHolder {

  /**
   * below view have public modifier because
   * we have access PlayerViewHolder inside the ExoPlayerRecyclerView
   */
  public LinearLayout userProfileLinearLay;
  public ImageView imv,addFrnds;
  public FrameLayout mediaContainer;
  public ImageView mediaCoverImage, volumeControl,ivMediaCoverImageIfImage;
  public ProgressBar progressBar;
  public RequestManager requestManager;
  private TextView title, userHandle,tvLikes,shareVideo;
  private View parent;
  private Context mContext;
  private MyFeedList mediaObjNew = new MyFeedList();
  private SharedPref pref;
  ProgressDialog mProgressDialog;
  ArrayList<MyFeedList> mList;
  CallBack callBack;


  public interface CallBack{
    void onCallMethod();


  }

  public PlayerViewHolder(@NonNull View itemView) {
    super(itemView);
    parent = itemView;
    mediaContainer = itemView.findViewById(R.id.mediaContainer);
    mediaCoverImage = itemView.findViewById(R.id.ivMediaCoverImage);
    title = itemView.findViewById(R.id.tvTitle);
    userHandle = itemView.findViewById(R.id.tvUserHandle);
    progressBar = itemView.findViewById(R.id.progressBar);
    volumeControl = itemView.findViewById(R.id.ivVolumeControl);
    tvLikes = itemView.findViewById(R.id.tvLikes);
    userProfileLinearLay = itemView.findViewById(R.id.userProfileLinearLay);
    imv = itemView.findViewById(R.id.imv);
    addFrnds = itemView.findViewById(R.id.addFrnds);
    shareVideo = itemView.findViewById(R.id.shareVideo);
    ivMediaCoverImageIfImage = itemView.findViewById(R.id.ivMediaCoverImageIfImage);
  }

  void onBind(final MyFeedList mediaObject, RequestManager requestManager, final Context context, CallBack callBack) {
    this.requestManager = requestManager;
    parent.setTag(this);
    title.setText(mediaObject.getPosted_by());
    this.mContext = context;
    this.mediaObjNew = mediaObject;
    pref = new SharedPref(mContext);
    this.callBack = callBack;




    if(mediaObject.getPosted_user_location()!=null){
      userHandle.setText(mediaObject.getPosted_user_location());
    }else{
      userHandle.setText("Bengaluru");
    }



   /* if(mediaObject.getVideo_thumbnail()!=null){
      this.requestManager
              .load("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+mediaObject.getVideo_thumbnail())
              .into(mediaCoverImage);
    }else{
      this.requestManager
              .load("https://ixtiro.in/test/placeholder.png")
              .into(mediaCoverImage)
              ;
    }*/


    if(mediaObject.getVideo_thumbnail()!=null) {
      //holder.imv.setTag(liveStreamLists.get(position).getImage());
      mediaCoverImage.setVisibility(View.GONE);
      ivMediaCoverImageIfImage.setVisibility(View.VISIBLE);
      Picasso.with(mContext).load("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+mediaObject.getVideo_thumbnail())
              .into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap,
                                           Picasso.LoadedFrom from) {
                  ivMediaCoverImageIfImage.setImageBitmap(bitmap);
                  //mediaCoverImage.setImageBitmap(bitmap);

                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                  ivMediaCoverImageIfImage.setImageDrawable(
                          mContext.getResources().
                                  getDrawable(R.drawable.videothumbnail));

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
              });
    }
    else
    {
      mediaCoverImage.setVisibility(View.VISIBLE);
      ivMediaCoverImageIfImage.setVisibility(View.GONE);
      mediaCoverImage.setImageDrawable(
              mContext.getResources().
                      getDrawable(R.drawable.videothumbnail)
      );

    }

    tvLikes.setText(mediaObject.getTotal_likes()+"");

    if(mediaObject.getLiked()==0){

      tvLikes.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_heart, 0, 0, 0);
    }else{
      tvLikes.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_likefilled, 0, 0, 0);

    }


    tvLikes.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        tvLikes.setEnabled(false);
        tvLikes.setClickable(false);
        postChallengesAcceptedVideo();

      }
    });


    if(mediaObject.getUser_id() == pref.getInt(Constants.USER_ID)){
      addFrnds.setVisibility(View.GONE);
    }else{
      if(mediaObject.getFollowing()==0){
        addFrnds.setVisibility(View.VISIBLE);
      }else{
        addFrnds.setVisibility(View.GONE);
      }
    }


    shareVideo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        //shareVideoWhatsApp();
        //shareVideoToAll();
        //shareVideoWhatsApp();
        shareVid();
      }
    });


    addFrnds.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        postFollowUnfollowRequest(String.valueOf(mediaObject.getUser_id()),1);
      }
    });

    userProfileLinearLay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(mediaObject.getUser_id() == pref.getInt(Constants.USER_ID)){

          Intent in = new Intent(mContext, AboutUsActivity.class);
          //in.putExtra("userId",String.valueOf(mediaObject.getUser_id()));
          mContext.startActivity(in);
        }else{

          Intent in = new Intent(mContext, UserDetailsActivity.class);
          in.putExtra("userId",String.valueOf(mediaObject.getUser_id()));
          mContext.startActivity(in);
        }


      }
    });


    if(mediaObject.getPosted_user_image()!=null) {
      //holder.imv.setTag(liveStreamLists.get(position).getImage());
      Picasso.with(mContext).load("http://mygame-app.s3.ap-south-1.amazonaws.com/"+mediaObject.getPosted_user_image())
              .into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap,
                                           Picasso.LoadedFrom from) {

                  imv.setImageBitmap(bitmap);

                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                  imv.setImageDrawable(
                          mContext.getResources().
                                  getDrawable(R.drawable.ic_user));

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
              });
    }
    else
    {
      imv.setImageDrawable(
              mContext.getResources().
                      getDrawable(R.drawable.ic_user)
      );

    }

  }





  private void postChallengesAcceptedVideo(){
    if (AppUtil.isInternetConnected(mContext)) {
      //mContext.enableDialog("Please wait...");
     // mProgressDialog= ProgressDialog.show(mContext,null, Constants.PLEASE_WAIT);
      tvLikes.setEnabled(false);
        tvLikes.setClickable(false);

      VideoLikeUnLikeRequest request = new VideoLikeUnLikeRequest();
      request.setVideo_id(mediaObjNew.getId());
      if(mediaObjNew.getLiked()==0){
        request.setLike(1);
        mediaObjNew.setLiked(1);
      }else if(mediaObjNew.getLiked()==1){
        request.setLike(0);
        mediaObjNew.setLiked(0);
      }



      RestService apiService =
              ApiClient.getClient(mContext).create(RestService.class);

      Call<VideoLikeUnlikeResponse> call = apiService.postVideoLikeUnlike(request);
      Log.v("call",call.toString());
      call.enqueue(new Callback<VideoLikeUnlikeResponse>() {
        @Override
        public void onResponse(Call<VideoLikeUnlikeResponse> call, Response<VideoLikeUnlikeResponse> response) {


         // disableDialog();



          if(response.code()==201){

            /*if(mProgressDialog!=null)
            {
              mProgressDialog.dismiss();
              mProgressDialog=null;
            }*/


            VideoLikeUnlikeResponse videoLikeUnlikeResponse = response.body();
            if(videoLikeUnlikeResponse.getStatus().equalsIgnoreCase("ok")){
              //Toast.makeText(mContext, videoLikeUnlikeResponse.getMessage(), Toast.LENGTH_SHORT).show();

              if(videoLikeUnlikeResponse.getMessage().equalsIgnoreCase("Video Liked")){

                tvLikes.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_likefilled, 0, 0, 0);
                int videoCount = Integer.valueOf(tvLikes.getText().toString());
                videoCount = videoCount+1;
                tvLikes.setText(videoCount+"");

              }else{
                tvLikes.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_heart, 0, 0, 0);
                int videoCount = Integer.valueOf(tvLikes.getText().toString());
                if(videoCount==0){

                }else{
                  videoCount = videoCount-1;
                }

                tvLikes.setText(videoCount+"");
                //tvLikes.setEnabled(true);

              }


              tvLikes.setEnabled(true);
              tvLikes.setClickable(true);
            }else{
              /*if(mProgressDialog!=null)
              {
                mProgressDialog.dismiss();
                mProgressDialog=null;
              }*/
              tvLikes.setEnabled(true);
             // Toast.makeText(mContext, "" + videoLikeUnlikeResponse.getStatus(), Toast.LENGTH_SHORT).show();
            }
          }else {
            /*if(mProgressDialog!=null)
            {
              mProgressDialog.dismiss();
              mProgressDialog=null;
            }*/

            tvLikes.setEnabled(true);

            if (response != null && response.errorBody() != null) {
              //LoginErrorModel error = ErrorUtils.parseError(response);
              // … and use it to show error information

              // … or just log the issue like we’re doing :)
              //Log.d("error message", error.getMessage());
              Toast.makeText(mContext, response.message()+"response message", Toast.LENGTH_SHORT).show();
              return;
            }

          }


        }

        @Override
        public void onFailure(Call<VideoLikeUnlikeResponse> call, Throwable t) {

          //disableDialog();
          tvLikes.setEnabled(true);
         /* if(mProgressDialog!=null)
          {
            mProgressDialog.dismiss();
            mProgressDialog=null;
          }*/
          Toast.makeText(mContext, "Failure"+t.toString(), Toast.LENGTH_SHORT).show();
        }
      });




    } else {
      //disableDialog();
      tvLikes.setEnabled(true);
      /*if(mProgressDialog!=null)
      {
        mProgressDialog.dismiss();
        mProgressDialog=null;
      }*/
      Toast.makeText(mContext,
              "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
    }
  }


  public void postFollowUnfollowRequest(final String followId, int status){
    if (AppUtil.isInternetConnected(mContext)) {
      //mProgressDialog= ProgressDialog.show(mContext,null, Constants.PLEASE_WAIT);

      FollowUnfollowRequest request = new FollowUnfollowRequest();
      request.setFollowing_id(followId);
      request.setFollow(status);

      RestService apiService =
              ApiClient.getClient(mContext).create(RestService.class);

      Call<FollowUnfollowResponse> call = apiService.getFollowUnfollow(request
      );
      call.enqueue(new Callback<FollowUnfollowResponse>() {
        @Override
        public void onResponse(Call<FollowUnfollowResponse> call, Response<FollowUnfollowResponse> response) {


          if(mProgressDialog!=null)
          {
            mProgressDialog.dismiss();
            mProgressDialog=null;
          }



          if(response.code()==403){


            if (response != null && response.errorBody() != null) {
              LoginErrorModel error = ErrorUtils.parseError(response);
              // … and use it to show error information

              // … or just log the issue like we’re doing :)
              //Log.d("error message", error.getMessage());
              Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
              return;
            }

          }else if(response.code()==200){
            FollowUnfollowResponse followUnfollowResponse = response.body();
            //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();
            addFrnds.setVisibility(View.GONE);
            /*final MoviesFragment pf = new MoviesFragment();
            pf.onCallMethod(mContext);*/

            callBack.onCallMethod();

          }else if(response.code()==401){
            Toast.makeText(mContext, "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
            Intent in = new Intent(mContext, LoginActivity.class);
            mContext.startActivity(in);
            pref.logout();
          }


        }

        @Override
        public void onFailure(Call<FollowUnfollowResponse> call, Throwable t) {

          if(mProgressDialog!=null)
          {
            mProgressDialog.dismiss();
            mProgressDialog=null;
          }
          Toast.makeText(mContext, ""+t.toString(), Toast.LENGTH_SHORT).show();
        }
      });




    } else {
      if(mProgressDialog!=null)
      {
        mProgressDialog.dismiss();
        mProgressDialog=null;
      }
      Toast.makeText(mContext, "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
    }
  }



  public void shareVideoWhatsApp() {


    /*File file = new File("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+mediaObjNew.getFile_name());
    Uri uri = Uri.fromFile(file);
    Intent share = new Intent(Intent.ACTION_SEND);
    share.setType("video/*");
    share.putExtra(Intent.EXTRA_STREAM, Uri.parse("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+mediaObjNew.getFile_name()));
    mContext.startActivity(Intent.createChooser(share, "Share Video"));*/



    Intent share = new Intent(Intent.ACTION_SEND);

    // If you want to share a png image only, you can do:
    // setType("image/png"); OR for jpeg: setType("image/jpeg");
    share.setType("video/*");

    // Make sure you put example png image named myImage.png in your
    // directory


    File imageFileToShare = new File("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+mediaObjNew.getFile_name());

    Uri uri = Uri.fromFile(imageFileToShare);
    share.putExtra(Intent.EXTRA_STREAM, "http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+mediaObjNew.getFile_name());

    mContext.startActivity(Intent.createChooser(share, "Share Video"));
  }

public void shareVideoToAll(){
  String path = "http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+mediaObjNew.getFile_name(); //should be local path of downloaded video
  Intent share = new Intent(android.content.Intent.ACTION_SEND);
  share.setType("text/plain");
  share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

  // Add data to the intent, the receiving app will decide
  // what to do with it.
  share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
  share.putExtra(Intent.EXTRA_TEXT, path);

  mContext.startActivity(Intent.createChooser(share, "Share link!"));
}


public void shareVid(){
  String path = "http://mygame-app.s3.ap-south-1.amazonaws.com/videos/" + mediaObjNew.getFile_name(); //should be local path of downloaded video
  Intent share = new Intent(android.content.Intent.ACTION_SEND);
  share.setType("text/plain");
  share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

  // Add data to the intent, the receiving app will decide
  // what to do with it.
  share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
  share.putExtra(Intent.EXTRA_TEXT, path);

  mContext.startActivity(Intent.createChooser(share, "Share link!"));


  }


}


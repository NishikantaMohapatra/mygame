package com.lydor.mygame.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lydor.mygame.R;

public class TestTestTestActivity extends AppCompatActivity {


    public int counter;
    Button button;
    TextView textView;
    CountDownTimer t;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_test_test);button= (Button) findViewById(R.id.button);
        textView= (TextView) findViewById(R.id.textView);
       /* button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                new CountDownTimer(18000, 1000){
                    public void onTick(long millisUntilFinished){
                        //textView.setText(String.valueOf(counter));
                        counter++;


                        String time = new Integer(counter).toString();

                        long millis = counter;
                        int seconds = (int) (millis / 60);
                        int minutes = seconds / 60;
                        seconds     = seconds % 60;

                        textView.setText(String.format("%d:%02d:%02d", minutes, seconds,millis));
                    }
                    public  void onFinish(){

                        textView.setText("FINISH!!");
                    }
                }.start();
            }
        });*/
        t = new CountDownTimer(18000,1000) {
            @Override
            public void onTick(long l) {
                counter++;


                String time = new Integer(counter).toString();

                long millis = counter;
                int seconds = (int) (millis / 60);
                int minutes = seconds / 60;
                seconds     = seconds % 60;

                textView.setText(String.format("%02d:%02d", seconds,millis));
            }

            @Override
            public void onFinish() {
                textView.setText("FINISH!!");
                //t.cancel();
                counter = 0;
            }
        };

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(counter>0){
                    t.cancel();
                    textView.setText("FINISH!!");
                    counter = 0;
                }else{
                    t.start();
                }


            }
        });
    }
}

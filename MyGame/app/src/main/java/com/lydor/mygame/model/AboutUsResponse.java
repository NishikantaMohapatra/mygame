package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AboutUsResponse implements Parcelable {

    private String message;
    private String About_details;


    public AboutUsResponse() {
    }

    protected AboutUsResponse(Parcel in) {
        message = in.readString();
        About_details = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeString(About_details);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AboutUsResponse> CREATOR = new Creator<AboutUsResponse>() {
        @Override
        public AboutUsResponse createFromParcel(Parcel in) {
            return new AboutUsResponse(in);
        }

        @Override
        public AboutUsResponse[] newArray(int size) {
            return new AboutUsResponse[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAbout_details() {
        return About_details;
    }

    public void setAbout_details(String about_details) {
        About_details = about_details;
    }
}

package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.lydor.mygame.R;

import java.util.Locale;

public class VideoPlayerActivity extends AppCompatActivity {

    VideoView videoView;
    ImageView imageView;
    SeekBar seekbar;
    String strVideoUrl;
    Uri uri;
    boolean isPlay = false;
    Handler handler;

    private static long START_TIME_IN_MILLIS=0;
    private TextView mTextViewCountdown;
    private CountDownTimer mCountDownTimer;
    private boolean mTimmerRunning;

    private static long mTimeLeftInMills;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_player);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        //getSupportActionBar().hide();
        init();
    }

    private void init() {

        videoView = findViewById(R.id.videoView);
        imageView = findViewById(R.id.imageView);
        seekbar = findViewById(R.id.seekBar);
        mTextViewCountdown = findViewById(R.id.tvTimer);
        handler = new Handler();
        /*DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) videoView.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;*/
        strVideoUrl = "https://androidwave.com/media/androidwave-video-exo-player.mp4";
        //update package name
        uri = Uri.parse(strVideoUrl);
        //videoView.setLayoutParams(params);
        videoView.setVideoURI(uri);
        videoView.start();


        startTimer();
        if(videoView.isPlaying()){
            imageView.setImageResource(R.drawable.ic_pausesign);

        }
        isPlay=true;


        updateSeekBar();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isPlay){

                    videoView.pause();
                    isPlay = false;
                    imageView.setImageResource(R.drawable.ic_playsign);
                    startTimer();
                }else if(!isPlay){

                    videoView.start();
                    isPlay = true;
                    updateSeekBar();

                    imageView.setImageResource(R.drawable.ic_pausesign);
                }
            }
        });

        updateCountDownStart();

    }

    private void updateSeekBar() {

        handler.postDelayed(updateTimeTask,100);
        startTimer();

    }

    public Runnable updateTimeTask = new Runnable() {
        @Override
        public void run() {
            seekbar.setProgress(videoView.getCurrentPosition());
            seekbar.setMax(videoView.getDuration());
            int testesffsddag = videoView.getDuration();
            int sdfasdf = videoView.getCurrentPosition();
            START_TIME_IN_MILLIS = videoView.getCurrentPosition();
            mTimeLeftInMills = START_TIME_IN_MILLIS;

            updateCountDownStart();






            handler.postDelayed(this,100);

            startTimer();

            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    handler.removeCallbacks(updateTimeTask);
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    handler.removeCallbacks(updateTimeTask);
                    videoView.seekTo(seekBar.getProgress());
                    imageView.setImageResource(R.drawable.ic_playsign);
                    updateSeekBar();
                    pauseTimer();
                }
            });
        }
    };


    private void startTimer(){

        mCountDownTimer = new CountDownTimer(mTimeLeftInMills,1000) {
            @Override
            public void onTick(long l) {

                mTimeLeftInMills = l;
                updateCountDownStart();
            }

            @Override
            public void onFinish() {
                mTimmerRunning = false;
            }
        }.start();

        mTimmerRunning = true;

    }

    private void pauseTimer(){

        mCountDownTimer.cancel();
        mTimmerRunning = false;


    }

    private void resetTimer(){

        mTimeLeftInMills = START_TIME_IN_MILLIS;
        updateCountDownStart();
    }


    private void updateCountDownStart(){

        int minutes = (int) (mTimeLeftInMills / 1000) / 60;
        int seconds = (int) (mTimeLeftInMills / 1000) % 60;

        String timeLeftFormated = String.format(Locale.getDefault(),"%02d:%02d", minutes,seconds);

        mTextViewCountdown.setText(timeLeftFormated);


    }
}

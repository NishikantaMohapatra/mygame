package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class LoginResponseNew implements Parcelable {

        private String status;
        private String token;
        private String expires_in;
        private String notification_status;
        private String name;
        private String mobile;
        private String message;
        private int id;

    public LoginResponseNew() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getNotification_status() {
        return notification_status;
    }

    public void setNotification_status(String notification_status) {
        this.notification_status = notification_status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static Creator<LoginResponseNew> getCREATOR() {
        return CREATOR;
    }

    protected LoginResponseNew(Parcel in) {
        status = in.readString();
        token = in.readString();
        expires_in = in.readString();
        notification_status = in.readString();
        name = in.readString();
        mobile = in.readString();
        message = in.readString();
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(token);
        dest.writeString(expires_in);
        dest.writeString(notification_status);
        dest.writeString(name);
        dest.writeString(mobile);
        dest.writeString(message);
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginResponseNew> CREATOR = new Creator<LoginResponseNew>() {
        @Override
        public LoginResponseNew createFromParcel(Parcel in) {
            return new LoginResponseNew(in);
        }

        @Override
        public LoginResponseNew[] newArray(int size) {
            return new LoginResponseNew[size];
        }
    };
}

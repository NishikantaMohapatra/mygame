package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class LoginRequest implements Parcelable {


    private String email;
    private String password;
    private String device_id;


    public LoginRequest() {
    }

    protected LoginRequest(Parcel in) {
        email = in.readString();
        password = in.readString();
        device_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(device_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginRequest> CREATOR = new Creator<LoginRequest>() {
        @Override
        public LoginRequest createFromParcel(Parcel in) {
            return new LoginRequest(in);
        }

        @Override
        public LoginRequest[] newArray(int size) {
            return new LoginRequest[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
}

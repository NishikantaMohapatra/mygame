package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lydor.mygame.R;
import com.lydor.mygame.activities.MainActivity;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.ForgetPasswordRequest;
import com.lydor.mygame.model.ForgetPasswordResponse;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.LoginRequest;
import com.lydor.mygame.model.LoginResponseNew;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends BaseActivity {


    ImageView imvBack;
    TextView tvOtp;
    EditText etEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        imvBack = findViewById(R.id.imvBack);
        tvOtp = findViewById(R.id.tvOtp);
        etEmail= findViewById(R.id.etEmail);


        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (etEmail.getText().toString().isEmpty()) {
                    Toast.makeText(ForgetPasswordActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();
                } else if (!etEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(ForgetPasswordActivity.this, "Please enter a valid email", Toast.LENGTH_SHORT).show();
                }else{
                    postForgetPassword(etEmail.getText().toString());
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void postForgetPassword(String email){
        if (AppUtil.isInternetConnected(ForgetPasswordActivity.this)) {
            enableDialog("Please wait...");
            ForgetPasswordRequest request = new ForgetPasswordRequest();
            request.setEmail(email);


            RestService apiService =
                    ApiClient.getClient(this).create(RestService.class);

            Call<ForgetPasswordResponse> call = apiService.postForgetPassword(request);
            call.enqueue(new Callback<ForgetPasswordResponse>() {
                @Override
                public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {


                    disableDialog();



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);




                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(ForgetPasswordActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        ForgetPasswordResponse successresponse = response.body();


                        if (successresponse.getMessage().equalsIgnoreCase("We have e-mailed your password reset link!")){

                            showToast(successresponse.getMessage());
                            finish();
                        }else{
                            showToast(successresponse.getMessage());
                        }

                    }else if(response.code()==404){

                        showToast("We can't find a user with this e-mail address.");
                    }



                }

                @Override
                public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {

                    disableDialog();
                    showToast(t.toString());
                }
            });




        } else {
            disableDialog();
            Toast.makeText(ForgetPasswordActivity.this, "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }

}

package com.lydor.mygame.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lydor.mygame.R;
import com.lydor.mygame.activity.ChallengesDetailsActivity;
import com.lydor.mygame.activity.VideoPlayerEditProfileActivity;
import com.lydor.mygame.activity.VideoPreviewActiviyNew;
import com.lydor.mygame.model.ChallengesDetailsAcceptedList;
import com.lydor.mygame.model.ChallengesDetailsResponse;
import com.lydor.mygame.models.Model_Video;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

public class ChallengesDetailsAdapter extends RecyclerView.Adapter<ChallengesDetailsAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private Context context;
    private List<ChallengesDetailsAcceptedList> testLists;
    private Context mcontext;
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public ChallengesDetailsAdapter(List<ChallengesDetailsAcceptedList> testLists, Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.testLists = testLists;
    }


    // inflates the row layout from xml when needed
    @Override
    public ChallengesDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_videos,
                parent, false);
        ChallengesDetailsAdapter.ViewHolder viewHolder = new ChallengesDetailsAdapter.ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the view and textview in each row
    @Override
    public void onBindViewHolder(ChallengesDetailsAdapter.ViewHolder holder, final int position) {
        RelativeLayout container = holder.container;
        final ImageView imgView = holder.imageView;
        final ImageView iv_image_new = holder.iv_image_new;

/*
        if(testLists.get(position).getVideo_thumbnail()!=null) {
            //holder.imv.setTag(liveStreamLists.get(position).getImage());
            Picasso.with(context).load("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/" + testLists.get(position).getVideo_thumbnail())
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap,
                                                   Picasso.LoadedFrom from) {
                            iv_image_new.setVisibility(View.VISIBLE);
                            imgView.setVisibility(View.GONE);
                            iv_image_new.setImageBitmap(bitmap);

                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            iv_image_new.setVisibility(View.GONE);
                            imgView.setVisibility(View.VISIBLE);
                            imgView.setImageDrawable(
                                    context.getResources().
                                            getDrawable(R.drawable.photo));

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        }
        else
        {
            iv_image_new.setVisibility(View.GONE);
            imgView.setVisibility(View.VISIBLE);
            imgView.setImageDrawable(
                    context.getResources().
                            getDrawable(R.drawable.photo)
            );

        }*/


        if(testLists.get(position).getVideo_thumbnail()!=null){
            iv_image_new.setVisibility(View.VISIBLE);
            imgView.setVisibility(View.GONE);
            Glide.with(context).load("http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+testLists.get(position).getVideo_thumbnail())
                    .skipMemoryCache(false)
                    .placeholder(R.drawable.photo)
                    .into(iv_image_new);
        }else{
            iv_image_new.setVisibility(View.GONE);
            imgView.setVisibility(View.VISIBLE);
            imgView.setBackground(context.getResources().getDrawable(R.drawable.photo));
        }




        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ((ChallengesDetailsActivity) context).stopVideoPlayBack();
                Intent in = new Intent(context, VideoPreviewActiviyNew.class);
                in.putExtra("video","http://mygame-app.s3.ap-south-1.amazonaws.com/videos/"+testLists.get(position).getFile_name());
                in.putExtra("obj",testLists.get(position));
                context.startActivity(in);


            }
        });



    }


    // total number of rows
    @Override
    public int getItemCount() {
        return testLists.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public RelativeLayout container;
        public ImageView imageView,iv_image_new;

        public ViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.rl_select);
            imageView = itemView.findViewById(R.id.iv_image);
            iv_image_new = itemView.findViewById(R.id.iv_image_new);
        }
    }

    // allows clicks events to be caught

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);


    }

    public Bitmap createVideoThumbNail(String path){
        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
    }
}

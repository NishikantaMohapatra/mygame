package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.RegistrationRequest;
import com.lydor.mygame.model.RegistrationResponse;
import com.lydor.mygame.model.RegistrationResponseNew;
import com.lydor.mygame.networkcalling.IntentHelper;
import com.lydor.mygame.networkcalling.NetworkService;
import com.lydor.mygame.networkcalling.ResponseCodes;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.reciever.ServiceReceiver;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.Constants;
import com.lydor.mygame.utils.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends BaseActivity implements ServiceReceiver.Receiver {


    ImageView imvBack;
    EditText etPassword,etName,etEmail,etReEnterPassword,etMobile;
    TextView tvReg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        createReciever();
        imvBack = findViewById(R.id.imvBack);
        etPassword = findViewById(R.id.etPassword);
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etReEnterPassword = findViewById(R.id.etReEntPassword);
        etMobile = findViewById(R.id.etMobile);

        tvReg = findViewById(R.id.tvReg);
        Typeface type = Typeface.createFromAsset(getAssets(),"font/titilliumregular.otf");
        etPassword.setTypeface(type);
        etPassword.setTransformationMethod(new PasswordTransformationMethod());

        String fromActivity = getIntent().getStringExtra("from");

        if(fromActivity.equalsIgnoreCase("fbLogin")){

            etEmail.setText(getIntent().getStringExtra("userEmail"));
            etName.setText(getIntent().getStringExtra("userName"));
        }


        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*SharedPref pref = new SharedPref(RegistrationActivity.this);
                pref*/
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


                if (etName.getText().toString().isEmpty()) {

                    Toast.makeText(RegistrationActivity.this, Constants.ENTER_YOUR_NAME, Toast.LENGTH_SHORT).show();

                } else if (etEmail.getText().toString().isEmpty()) {
                    Toast.makeText(RegistrationActivity.this, Constants.ENTER_YOUR_EMAIL, Toast.LENGTH_SHORT).show();
                } else if (!etEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(RegistrationActivity.this, Constants.ENTER_VALID_EMAIL, Toast.LENGTH_SHORT).show();
                }else if(etMobile.getText().toString().isEmpty()){
                    showToast(Constants.ENTER_MOBILE_NUMBER);
                } else if(etMobile.getText().toString().length()<10){
                    showToast(Constants.ENTER_10_DIG_MOBILE_NUMBER);
                }else if (etPassword.getText().toString().isEmpty()) {
                    Toast.makeText(RegistrationActivity.this, Constants.ENTER_A_PASSWORD, Toast.LENGTH_SHORT).show();
                } else if (etReEnterPassword.getText().toString().isEmpty()) {
                    Toast.makeText(RegistrationActivity.this, Constants.CONFIRM_PASSWORD, Toast.LENGTH_SHORT).show();
                } else if (!etPassword.getText().toString().matches(etReEnterPassword.getText().toString())) {
                    Toast.makeText(RegistrationActivity.this, Constants.BOTH_PASS_MATCHES, Toast.LENGTH_SHORT).show();
                } else {
                if (AppUtil.isInternetConnected(RegistrationActivity.this)) {
                    enableDialog(Constants.PLEASE_WAIT);
                    RegistrationRequest registrationRequest = new RegistrationRequest();
                    registrationRequest.setName(etName.getText().toString().trim());
                    registrationRequest.setEmail(etEmail.getText().toString().trim());
                    registrationRequest.setMobile(etMobile.getText().toString().trim());
                    registrationRequest.setPassword(etPassword.getText().toString().trim());
                    registrationRequest.setPassword_confirm(etReEnterPassword.getText().toString());

                    RestService apiService =
                            ApiClient.getClient(RegistrationActivity.this).create(RestService.class);
                    SharedPref pref = new SharedPref(RegistrationActivity.this);
                    pref.logout();
                    Call<RegistrationResponse> call = apiService.postRegistration(registrationRequest);
                    call.enqueue(new Callback<RegistrationResponse>() {
                        @Override
                        public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {


                            disableDialog();


                            //Toast.makeText(RegistrationActivity.this, "" + movies.getMessage(), Toast.LENGTH_SHORT).show();

                            if(response.code()==201){
                                RegistrationResponse movies = response.body();
                                if (movies.getStatus() != null) {
                                    if (movies.getStatus().equalsIgnoreCase("ok")) {

                                        Toast.makeText(RegistrationActivity.this, "You have successfully registered. Please login", Toast.LENGTH_SHORT).show();
                                        Intent in = new Intent(RegistrationActivity.this,LoginActivity.class);
                                        startActivity(in);
                                        finish();
                                    }
                                }
                            } else if(response.code()==403){
                                if (response != null && response.errorBody() != null) {
                                    LoginErrorModel error = ErrorUtils.parseError(response);
                                     //… and use it to show error information

                                     //… or just log the issue like we’re doing :)

                                    Log.d("error message", error.getFields());
                                    Toast.makeText(RegistrationActivity.this, error.getFields(), Toast.LENGTH_SHORT).show();
                                    return;

                                }
                            }else{
                                if (response != null && response.errorBody() != null) {
                                    Toast.makeText(RegistrationActivity.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            }


                            //Log.d(TAG, "Number of movies received: " + movies.size());
                        }

                        @Override
                        public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                            // Log error here since request failed
                            //Log.e(TAG, t.toString());
                            disableDialog();
                            showToast(t.toString());
                        }
                    });


                   /* Bundle bun = new Bundle();
                    bun.putString("firstName",etName.getText().toString().trim());
                    bun.putString("lastName","test");
                    bun.putString("email",etEmail.getText().toString().trim());
                    bun.putString("password",etPassword.getText().toString().trim());

                    NetworkService.startActionRegistration(RegistrationActivity.this,mServiceReceiver,bun);*/

                } else {
                    disableDialog();
                    showToast(Constants.NO_INTERNET);
                }


            }
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch(resultCode){
            case ResponseCodes.SUCCESS:


                    String code = resultData.getString(IntentHelper.RESULT_DATA);

                    if(code.equalsIgnoreCase("User was created.")){
                        showToast("Successfully registered");
                    }else {

                        showToast(code);
                    }





                break;

            case ResponseCodes.FAILURE:
                showToast("Something went wrong");
                break;

            case ResponseCodes.UNEXPECTED_ERROR:
                showToast("Something went wrong");
                break;

            case ResponseCodes.EXCEPTION:
                showToast("Something went wrong");
                break;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }
}

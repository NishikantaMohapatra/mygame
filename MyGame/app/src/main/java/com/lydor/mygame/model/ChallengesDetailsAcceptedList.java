package com.lydor.mygame.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ChallengesDetailsAcceptedList implements Parcelable {


    private String id;
    private String title;
    private String description;
    private String file_name;
    private String video_thumbnail;
    private int duration;
    private int user_id;
    private int challenge_id;
    private int total_views;
    private int total_likes;
    private String created_at;
    private String updated_at;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(int challenge_id) {
        this.challenge_id = challenge_id;
    }

    public int getTotal_views() {
        return total_views;
    }

    public void setTotal_views(int total_views) {
        this.total_views = total_views;
    }

    public int getTotal_likes() {
        return total_likes;
    }

    public void setTotal_likes(int total_likes) {
        this.total_likes = total_likes;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public static Creator<ChallengesDetailsAcceptedList> getCREATOR() {
        return CREATOR;
    }

    public ChallengesDetailsAcceptedList() {
    }

    protected ChallengesDetailsAcceptedList(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        file_name = in.readString();
        video_thumbnail = in.readString();
        duration = in.readInt();
        user_id = in.readInt();
        challenge_id = in.readInt();
        total_views = in.readInt();
        total_likes = in.readInt();
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(file_name);
        dest.writeString(video_thumbnail);
        dest.writeInt(duration);
        dest.writeInt(user_id);
        dest.writeInt(challenge_id);
        dest.writeInt(total_views);
        dest.writeInt(total_likes);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChallengesDetailsAcceptedList> CREATOR = new Creator<ChallengesDetailsAcceptedList>() {
        @Override
        public ChallengesDetailsAcceptedList createFromParcel(Parcel in) {
            return new ChallengesDetailsAcceptedList(in);
        }

        @Override
        public ChallengesDetailsAcceptedList[] newArray(int size) {
            return new ChallengesDetailsAcceptedList[size];
        }
    };
}

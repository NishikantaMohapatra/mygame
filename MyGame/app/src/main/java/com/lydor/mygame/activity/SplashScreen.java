package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.activities.MainActivity;
import com.lydor.mygame.utils.Constants;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashScreen extends AppCompatActivity {



    private static final int REQUEST_PERMISSIONS = 100;
    SharedPref preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_splash_screen);
            setSupportActionBar(null);
        printHashKey(SplashScreen.this);

        preferences = new SharedPref(getApplicationContext());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*MyPrefs myPrefs = new MyPrefs(getApplicationContext());
                if(myPrefs.getBool(MyConsts.LOGGED_IN)){*/
                   // myPrefs.putBool(MyConsts.FROMFACEBOOK,false);

                if(preferences.getBoolean(Constants.LOGIN_STATUS)){
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }else{
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }


               /* } else {
                    startActivity(new Intent(getApplicationContext(), UserType.class));
                    myPrefs.putBool(MyConsts.FROMFACEBOOK,false);
                }*/
            }
        }, 3000);

    }


    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                //Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            //Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            //Log.e(TAG, "printHashKey()", e);
        }
    }


}

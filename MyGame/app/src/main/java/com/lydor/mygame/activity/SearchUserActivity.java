package com.lydor.mygame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lydor.mygame.Preferences.SharedPref;
import com.lydor.mygame.R;
import com.lydor.mygame.adapter.FollowersListAdapter;
import com.lydor.mygame.adapter.SearchUserAdapter;
import com.lydor.mygame.callingnetwork.ApiClient;
import com.lydor.mygame.model.FollowUnfollowRequest;
import com.lydor.mygame.model.FollowUnfollowResponse;
import com.lydor.mygame.model.FollowersResponse;
import com.lydor.mygame.model.LoginErrorModel;
import com.lydor.mygame.model.SearchUserRequest;
import com.lydor.mygame.model.SearchUserResponse;
import com.lydor.mygame.networkcalling.RestService;
import com.lydor.mygame.utils.AppUtil;
import com.lydor.mygame.utils.ErrorUtils;
import com.lydor.mygame.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUserActivity extends BaseActivity {




    EditText tvSearch;
    ImageView imvBack;
    RecyclerView rcvList;
    SharedPref pref;
    ArrayList<SearchUserResponse> mList;
    SearchUserAdapter mAdapter;
    private ProgressDialog mProgressDialog;
    TextView errTv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);

        tvSearch = findViewById(R.id.tvSearch);
        imvBack = findViewById(R.id.imvBack);
        rcvList = findViewById(R.id.rcvList);
        errTv = findViewById(R.id.errTv);
        pref = new SharedPref(this);
        mProgressDialog = new ProgressDialog(this);
        //pref = new SharedPref(this);
        mList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcvList.setLayoutManager(linearLayoutManager);
        mAdapter = new SearchUserAdapter(mList,this);
        rcvList.addItemDecoration(new SimpleDividerItemDecoration(SearchUserActivity.this,10,true));
        rcvList.setAdapter(mAdapter);

        searchUser("");

        tvSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {



                    searchUser(tvSearch.getText().toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });




    }

    public void searchUser(String name){
        if (AppUtil.isInternetConnected(this)) {

            //enableDialog("Please wait...");
            SearchUserRequest request = new SearchUserRequest();
            request.setName(name);



            RestService apiService =
                    ApiClient.getClient(this).create(RestService.class);

            Call<ArrayList<SearchUserResponse>> call = apiService.postSearchUser(request
            );
            call.enqueue(new Callback<ArrayList<SearchUserResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<SearchUserResponse>> call, Response<ArrayList<SearchUserResponse>> response) {




                    disableDialog();
                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(SearchUserActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        ArrayList<SearchUserResponse> followersResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        if(followersResponse.size()>0){
                            mList.clear();
                            mList.addAll(followersResponse);
                            mAdapter.notifyDataSetChanged();
                            rcvList.setVisibility(View.VISIBLE);
                            errTv.setVisibility(View.GONE);
                        }else{
                            rcvList.setVisibility(View.GONE);
                            errTv.setVisibility(View.VISIBLE);
                            errTv.setText("We dont find any users for this name");
                            mList.clear();
                            mAdapter.notifyDataSetChanged();
                            //Toast.makeText(getActivity(), "No users following you", Toast.LENGTH_SHORT).show();
                        }
                    }else if(response.code()==401){
                        Toast.makeText(SearchUserActivity.this, "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(SearchUserActivity.this, LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        finish();
                    }


                }

                @Override
                public void onFailure(Call<ArrayList<SearchUserResponse>> call, Throwable t) {

                    disableDialog();
                    Toast.makeText(SearchUserActivity.this, ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {

            Toast.makeText(SearchUserActivity.this, "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }


    public void postFollowUnfollowRequest(String followId,int status){
        if (AppUtil.isInternetConnected(SearchUserActivity.this)) {
            //mProgressDialog= ProgressDialog.show(getActivity(),null, Constants.PLEASE_WAIT);

            FollowUnfollowRequest request = new FollowUnfollowRequest();
            request.setFollowing_id(followId);
            request.setFollow(status);

            RestService apiService =
                    ApiClient.getClient(SearchUserActivity.this).create(RestService.class);

            Call<FollowUnfollowResponse> call = apiService.getFollowUnfollow(request
            );
            call.enqueue(new Callback<FollowUnfollowResponse>() {
                @Override
                public void onResponse(Call<FollowUnfollowResponse> call, Response<FollowUnfollowResponse> response) {


                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }



                    if(response.code()==403){


                        if (response != null && response.errorBody() != null) {
                            LoginErrorModel error = ErrorUtils.parseError(response);
                            // … and use it to show error information

                            // … or just log the issue like we’re doing :)
                            //Log.d("error message", error.getMessage());
                            Toast.makeText(SearchUserActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }else if(response.code()==200){
                        FollowUnfollowResponse followUnfollowResponse = response.body();
                        //Toast.makeText(getActivity(), ""+successresponse.getData().size(), Toast.LENGTH_SHORT).show();

                        searchUser(tvSearch.getText().toString());
                    }else if(response.code()==401){
                        Toast.makeText(SearchUserActivity.this, "Token is expired.Auto Logged out from the app.Please login again...", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(SearchUserActivity.this, LoginActivity.class);
                        startActivity(in);
                        pref.logout();
                        finish();
                    }


                }

                @Override
                public void onFailure(Call<FollowUnfollowResponse> call, Throwable t) {

                    if(mProgressDialog!=null)
                    {
                        mProgressDialog.dismiss();
                        mProgressDialog=null;
                    }
                    Toast.makeText(SearchUserActivity.this, ""+t.toString(), Toast.LENGTH_SHORT).show();
                }
            });




        } else {
            if(mProgressDialog!=null)
            {
                mProgressDialog.dismiss();
                mProgressDialog=null;
            }
            Toast.makeText(SearchUserActivity.this, "Please check your Internet connection!!!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
